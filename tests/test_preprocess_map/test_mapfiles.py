from __future__ import print_function
from __future__ import division
# STD lib
import os
import glob

# PULSE modules
from support.utils import load_default_config, get_filehash
from anchoring.main import anchor_searching, index_generation
# test
import pytest


tmpdir = os.path.dirname(__file__)
# remove previous output
files = glob.glob(tmpdir+'/output/preprocess/chrY/*.*')
for f in files:
    os.remove(f)
    
files = glob.glob(tmpdir+'/output/features/chrY/*.*')
for f in files:
    os.remove(f)

# init config
@pytest.fixture
def test_config(configfile= tmpdir+'/test_config_map.yml', smaple_id='chrY'):
    config = load_default_config(configfile, smaple_id)

    return config


# generate blast anchors
def test_anchoring(test_config):

    print(test_config)
    anchor_searching(test_config)
    # comapre output

    assert get_filehash(test_config['BLAST_OUTPUT']) == get_filehash(tmpdir+'/ref/blastx_from_as_events.out')



# generate indexs
def test_indexing(test_config):
    index_generation(test_config)


    # compare OUTPUTS
    assert get_filehash(test_config['CDNA']) == get_filehash(tmpdir+'/ref/cdna_transcripts.fasta')
    assert get_filehash(test_config['PSEQ']) == get_filehash(tmpdir+'/ref/p_seq_isoforms.fas')
    assert get_filehash(test_config['ANCHOR_EVENT_INIDEX_MAP']) == get_filehash(tmpdir+'/ref/uniprot_exon_indices_map.out')

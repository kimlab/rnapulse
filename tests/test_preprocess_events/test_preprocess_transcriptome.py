from __future__ import print_function
from __future__ import division
# STD lib
import os
import glob


# PULSE modules
from support.utils import timeitandlog, check_ouputs, load_config
# Filtering transcriptome
from preprocess_transcriptome.reference_genome import load_pickled_reference_genome
from preprocess_transcriptome.transcriptome import read_gtf
from preprocess_transcriptome.main import group_transcripts, fetch_splicing_events

# test
import pytest



tmpdir = os.path.dirname(__file__)
# remove previous output
files = glob.glob(tmpdir+'/output/chrY/*')
for f in files:
    os.remove(f)

# test timing decorator
@timeitandlog
def test_decorator():
    print('test')
    return


# init config
@pytest.fixture
def test_config(configfile= tmpdir+'/test_config_preprocess.yml', smaple_id='chrY'):
    config = load_config(configfile, smaple_id)
    print(config)
    return config


@pytest.fixture
def test_load_genome(test_config):
    ref_genome = load_pickled_reference_genome(test_config['GENOME_PICKLE'])


    # reference values for GRCh38
    total_chr = 2779
    total_bases = 3107576521
    count_bases = 0
    for _ in sorted(ref_genome.keys()):
        count_bases += len(ref_genome[_])

    assert total_bases == count_bases
    assert total_chr == len(ref_genome)

    return ref_genome

@pytest.fixture
def test_read_gtf(test_config):

    sample_transcriptome = read_gtf(test_config['TRANSCRIPT_FILE'])
    assert list(sample_transcriptome.chromosomes.keys())[0] == 'chrY'
    assert len(sample_transcriptome.genes) == 594
    assert len(sample_transcriptome.transcripts) == 929

    return sample_transcriptome


@pytest.fixture
def test_grouped(test_read_gtf, test_load_genome):
    grouped_transcripts = group_transcripts(test_read_gtf, test_load_genome)
    assert len(grouped_transcripts) == 867
    return grouped_transcripts


def test_ouputfiles(test_config, test_grouped):

    fetch_splicing_events(test_config, test_grouped)
    # comapre hashfiles
    # do not work because the order of the data is diff, I should fix this
    # assert get_filehash(test_config['AS_LOC']) == get_filehash(tmpdir+'/chrY/as_location.out')
    # assert get_filehash(test_config['EVENTS_FASTA']) == get_filehash(tmpdir+'/chrY/events.fa')
    # assert get_filehash(test_config['COMPLETE_TRANSCRIPTS']) == get_filehash(tmpdir+'/chrY/complete_transcripts.fasta')
    # assert get_filehash(test_config['EXTENDED']) == get_filehash(tmpdir+'/chrY/extended.out')
    # out exists
    check_ouputs(test_config, 'Preprocess')
    # Comapre size
    assert os.path.getsize(test_config['AS_LOC']) == os.path.getsize(tmpdir+'/ref/as_location.out')
    assert os.path.getsize(test_config['EVENTS_FASTA']) == os.path.getsize(tmpdir+'/ref/events.fa')
    assert os.path.getsize(test_config['COMPLETE_TRANSCRIPTS']) == os.path.getsize(tmpdir+'/ref/complete_transcripts.fasta')
    assert os.path.getsize(test_config['EXTENDED']) == os.path.getsize(tmpdir+'/ref/extended.out')

from __future__ import print_function
from __future__ import division
# STD lib
import os
import glob


# PULSE modules
from rnapulse.support.utils import load_default_config, get_filehash, is_non_zero_file
from rnapulse.features.main import generate_features_matrix
from rnapulse.scoring.main import run_PULSE

# test
import pytest


tmpdir = os.path.dirname(__file__)
# remove previous output
files = glob.glob(tmpdir+'/output/scoring/chrY/*.*')

for f in files:
    os.remove(f)


# init config
@pytest.fixture
def test_config(configfile= tmpdir+'/test_config_scoring.yml', sample_id='chrY'):

    config = load_default_config(configfile, sample_id)

    return config



def test_merge_features(test_config):

    generate_features_matrix(test_config)
    # compare OUTPUTS
    assert get_filehash(test_config['NAMES_ML']) == get_filehash(tmpdir+'/ref/names.txt')
    assert get_filehash(test_config['FEATURES_ML']) == get_filehash(tmpdir+'/ref/features_headers.txt')

def test_scoring(test_config):

    run_PULSE(test_config)
    # compare OUTPUTS
    # scores has small differents between runs
    # a bit lame as a test. need some improvement.
    assert is_non_zero_file(test_config['PULSE_OUTPUT'])

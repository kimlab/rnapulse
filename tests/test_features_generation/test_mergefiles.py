from __future__ import print_function
from __future__ import division
# STD lib
import os
import glob
import shutil


# PULSE modules
from rnapulse.support.utils import  naive_filter, load_default_config, get_filehash
from rnapulse.features.main import feature_extraction


# test
import pytest


tmpdir = os.path.dirname(__file__)
# remove previous output
files = glob.glob(tmpdir+'/output/features/chrY/*.*')

for f in files:
     os.remove(f)


# init config
@pytest.fixture
def test_config(configfile= tmpdir+'/test_config_merge.yml', sample_id='chrY'):

    config = load_default_config(configfile, sample_id)

    return config

# copy inputfile to be preprocessed by smart filter
def test_copy():

    shutil.copy(tmpdir+'/ref/uniprot_exon_indices_map.out',
                tmpdir+'/output/preprocess/chrY/uniprot_exon_indices_map.out')

    shutil.copy(tmpdir+'/ref/cdna_transcripts.fasta',
                tmpdir+'/output/preprocess/chrY/cdna_transcripts.fasta')

    shutil.copy(tmpdir+'/ref/as_location.out',
                tmpdir+'/output/preprocess/chrY/as_location.out')

    shutil.copy(tmpdir+'/ref/p_seq_isoforms.fas',
                tmpdir+'/output/preprocess/chrY/p_seq_isoforms.fas')




def test_naive_filter(test_config):

    naive_filter(test_config)
    # compare OUTPUTS
    assert get_filehash(test_config['ANCHOR_EVENT_INIDEX_MAP']) == get_filehash(tmpdir+'/ref/uniprot_exon_indices_map_after_filter.out')
    assert get_filehash(test_config['CDNA']) == get_filehash(tmpdir+'/ref/cdna_transcripts.fasta')


def test_merging(test_config):

    feature_extraction(test_config)
    # chekcoutput
    for item in test_config:
        if '_FEATURES' in item or 'CONSERVATION_' in item:
            filename =  os.path.basename(test_config[item])
            # hack to avoid two collisions, shlould revisit the naming of the config file
            if not 'sql3' in filename and not 'inp' in filename:
                assert get_filehash(test_config[item]) == get_filehash(tmpdir+'/ref/'+filename)

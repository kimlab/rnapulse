from __future__ import print_function
from __future__ import division

import sys
import logging

from .find_splicing_events import exonskipping

logger = logging.getLogger(__name__)


def group_transcripts(sample_transcriptome, ref_genome):
    """Group transcripts by TSS and Gene id. Add Nucleotide sequence using genomic coordinates.

    Parameters
    ----------
    sample_transcriptome : Transcriptome object

    ref_genome : dict
        Reference genome sequences


    Returns
    -------

    dict

    """
    # Group by Gene & TSS, then Extract ExonSkiping AS events
    # print(sample_transcriptome.transcripts)
    groupby_tssgen_transcripts = dict()
    #
    # for w in sorted(sample_transcriptome.chromosomes, key=sample_transcriptome.chromosomes.get, reverse=True):
    #print ('{} : {}'.format(w, sample_transcriptome.chromosomes[w]))

    # print(sample_transcriptome.chromosomes)
    for trans_by_gene in sample_transcriptome.genes.values():
        for trans in trans_by_gene:

            t = sample_transcriptome.transcripts[trans]

            if t.tss_id not in groupby_tssgen_transcripts:
                # Skip transcripts without seq information
                # I am using a lite version of the Genome without PATCHES or other extra files
                if t.chromosome in ref_genome:

                    t.fill_seq(ref_genome)

                    groupby_tssgen_transcripts[t.tss_id] = [t]

            else:
                # Skip transcripts without seq information
                if t.chromosome in ref_genome:

                    t.fill_seq(ref_genome)

                    groupby_tssgen_transcripts[t.tss_id].append(t)

    return groupby_tssgen_transcripts


def fetch_splicing_events(config, groupby_tssgen_transcripts):

    # open files to save AS events and info
    AS_LOCATION_OUTPUT = open(config['AS_LOC'], 'w')
    logger.debug("OUTPUT: Splicing events chrom coords: %s ",
                AS_LOCATION_OUTPUT)

    COMPLETE_OUTPUT = open(config['COMPLETE_TRANSCRIPTS'], 'w')
    logger.debug("OUTPUT: Fasta file of the whole transcript: %s ",
                COMPLETE_OUTPUT)

    AS_EVENTS_OUTPUT = open(config['EVENTS_FASTA'], 'w')
    logger.debug("OUTPUT: Fasta file only event E1+C+E2: %s ", AS_EVENTS_OUTPUT)

    EXTENDED_OUTPUT = open(config['EXTENDED'], 'w')
    logger.debug("OUTPUT: Extended info, COV, FPKM, etc : %s ", EXTENDED_OUTPUT)

    logger.debug("MSG: # Groups %i", len(groupby_tssgen_transcripts))

    for group_id, list_transcripts in groupby_tssgen_transcripts.items():
        # skipping
        if len(list_transcripts) > 1:

            exonskipping(list_transcripts,
                         AS_LOCATION_OUTPUT,
                         COMPLETE_OUTPUT,
                         AS_EVENTS_OUTPUT,
                         EXTENDED_OUTPUT)

    AS_LOCATION_OUTPUT.close()
    COMPLETE_OUTPUT.close()
    AS_EVENTS_OUTPUT.close()
    EXTENDED_OUTPUT.close()

    return


def main(argv):
    '''Debug '''

    import json
    import getopt

    try:
        opts, args = getopt.getopt(argv, "hc:r:p:o:", [
                                   "cell_line=", "ref_genome=", "pulse_path=", "output_path="])
        configjson = open(opts).read()
        cfg = json.loads(configjson)
    except getopt.GetoptError:
        print('preprocess/main.py -c <cell_line> -r <ref_genome> -p <pulse_path> -o <output_path>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(
                'preprocess/main.py -c <cell_line> -r <ref_genome> -p <pulse_path> -o <output_path>')
            sys.exit()
        elif opt in ("-c", "--cell_line"):
            # config['SAMPLE_ID'] = arg
            pass
        # elif opt in ("-r", "--ref_genome"):
        #     ref_genome = arg
        # elif opt in ("-p", "--pulse_path"):
        #     pulse_path = arg
        # elif opt in ("-o", "--output_path"):
        #     output_path = arg

    print("LOADING PICKLED REFERENCE GENOME (happens only once for all cell lines)...")
    # ref_genome = load_pickled_reference_genome(config['PULSE_PATH']+config['GENOME_PICKLE'])
    print("PICKLED REFERENCE GENOME LOADED!")
    # preprocess_cell_line(cell_line, ref_genome, pulse_path, output_path)
    # preprocess_transcriptome(config)
    # select_transcripts(config, ref_genome)
    # anchor_searching(config)
    # index_generation(config)
    pass


if __name__ == "__main__":
    main(sys.argv[1:])

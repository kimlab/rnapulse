from __future__ import print_function
from __future__ import division

import logging

logger = logging.getLogger(__name__)
# PULSE modules


def exonskipping(transcripts, as_location, complete, events_location, extended_info):
    '''Find Skipping Exon events from gtf file.

    Simple approach: Comparing Transcripts that belongs to same gene and
    same TSS.

    Parameters
    ----------
        :param transcripts (list): List of transcript objects.
        :param as_location (open file)
        :param complete (open file)
        :events_location (open file)

    Return
    ------
        Write result on output files.
    '''
    # Keep track events
    events = []

    # Complete compare, all Transcript against all
    for transcript_i in transcripts:
        # Get Exons in transcript by pairs and compare against all pairs
        for exon_pairs_i in transcript_i.iterpairs():
            for transcript_j in transcripts:
                # Skip compare the same Transcript
                if transcript_i.id != transcript_j.id:
                    c1 = None
                    c2 = None
                    exon_pair = False
                    # Get Exons in transcript by pairs
                    for exon_pairs_j in transcript_j.iterpairs():
                        # Compare Exon Coordinates id = Chr-Start:End
                        # if they are equal, no AS event
                        if exon_pairs_i[0].id == exon_pairs_j[0].id and exon_pairs_i[1].id != exon_pairs_j[1].id:

                            # If they are different check if is there a AS
                            # ej1  -  ej2
                            # ei1 ei2 ei3
                            # if ej1 == ei1 check if eij2 == ei3

                            # try to get next exon, if there is not pass
                            ei3 = transcript_i.get_exon(
                                exon_pairs_i[1].exon_number + 1)
                            if ei3 != None:
                                # Potential  Event
                                # Save it
                                if ei3.id == exon_pairs_j[1].id:
                                    # Event id, coord of the ei1 ei2 ei3 INCLUSION
                                    ei1 = exon_pairs_i[0]
                                    ei2 = exon_pairs_i[1]
                                    exclusion_exon = exon_pairs_i[1]
                                    event_id = ei1.id + ei2.id + ei3.id

                                    if event_id not in events:
                                        events.append(event_id)

                                        # Save
                                        # HEADER> I/E : Transcript id : exons involved coords : C1 len = E len = C2 len
                                        print('>I:{}:{}:{}={}={}'.format(transcript_i.id, exclusion_exon.id, ei1.length, ei2.length, ei3.length),
                                              file=events_location)
                                        print('{}{}{}'.format(
                                            ei1.nseq, ei2.nseq, ei3.nseq), file=events_location)

                                        print('>E:{}:{}:{}={}={}'.format(transcript_j.id, exclusion_exon.id, ei1.length, ei2.length, ei3.length),
                                              file=events_location)
                                        print('{}{}'.format(
                                            ei1.nseq, ei3.nseq), file=events_location)
                                        # extended
                                        print('I:{}:{}:{}={}={}\t{}\t{}\t{}\t{}'.format(transcript_i.id,
                                                                                        exclusion_exon.id,
                                                                                        ei1.length,
                                                                                        ei2.length,
                                                                                        ei3.length,
                                                                                        transcript_i.fpkm,
                                                                                        transcript_i.cov,
                                                                                        transcript_i.tpm,
                                                                                        transcript_i.ref_gene_name),
                                              file=extended_info)

                                        print('E:{}:{}:{}={}={}\t{}\t{}\t{}\t{}'.format(transcript_j.id,
                                                                                        exclusion_exon.id,
                                                                                        ei1.length,
                                                                                        ei2.length,
                                                                                        ei3.length,
                                                                                        transcript_j.fpkm,
                                                                                        transcript_j.cov,
                                                                                        transcript_j.tpm,
                                                                                        transcript_j.ref_gene_name),
                                              file=extended_info)

                                        # Print to AS_location
                                        # HEADER> Transcript id : exons involved coords
                                        for t in [transcript_i, transcript_j]:
                                            print('{}\t{}\t{}\t{}:{}:{}={}={}\tC1\t{}'.format(t.chromosome.strip(),
                                                                                              ei1.start, ei1.end, t.id,
                                                                                              exclusion_exon.id, ei1.length, ei2.length, ei3.length,
                                                                                              t.strand), file=as_location)
                                            print('{}\t{}\t{}\t{}:{}:{}={}={}\tA\t{}'.format(t.chromosome.strip(),
                                                                                             ei2.start, ei2.end, t.id,
                                                                                             exclusion_exon.id, ei1.length, ei2.length, ei3.length,
                                                                                             t.strand), file=as_location)
                                            print('{}\t{}\t{}\t{}:{}:{}={}={}\tC2\t{}'.format(t.chromosome.strip(),
                                                                                              ei3.start, ei3.end, t.id,
                                                                                              exclusion_exon.id, ei1.length, ei2.length, ei3.length,
                                                                                              t.strand), file=as_location)

                                            # Print to complete cDNA
                                            # ID = Transcript ID (TCONS_#######)
                                            # Exon Including & Skipping
                                            print('>{}'.format(t.id),
                                                  file=complete)
                                            print(t.n_seq().strip(),
                                                  file=complete)


def _debug(filen):

    # load_merged_transcript(filen[0])
    return


if __name__ == "__main__":
    import sys
    _debug(sys.argv[1:])

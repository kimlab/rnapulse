from __future__ import print_function
from __future__ import division

import logging

logger = logging.getLogger(__name__)

class Exon(object):

    def __init__(self, splited_line):

        # extract info from file
        chromosome = splited_line[0].strip()
        start = splited_line[3].strip()
        end = splited_line[4].strip()
        strand = splited_line[6].strip()

        extended_id_info = parse_information_column(splited_line[8])


        self.chromosome = chromosome
        self.start = int(start)
        self.end = int(end)
        self.strand = strand

        # Optional
        self.transcript_id = extended_id_info.get('transcript_id', '')
        self.gene_id = extended_id_info.get('gene_id', '')
        self.nseq =  extended_id_info.get('sequence', '')
        self.exon_number = int(extended_id_info.get('exon_number', '0'))
        self.class_code = extended_id_info.get('class_code', '')
        self.tss_id = extended_id_info.get('tss_id', '')
        self.ref_gene_name = extended_id_info.get('ref_gene_name', '')
        self.cov = float( extended_id_info.get('cov', '0.0'))

        # # internal
        self.id = self.chromosome + '_' + str(self.start) + '_' + str(self.end)
        self.length = self.end - self.start + 1

    def __str__(self):

        return 'ID: {}\nT: {}\nG: {}\n#: {}\nC: {}\nSS: {}'.format(self.id,
                                                                    self.transcript_id,
                                                                    self.gene_id,
                                                                    self.exon_number,
                                                                    self.class_code,
                                                                    self.tss_id)


class Transcript(object):

    def __init__(self,  splited_line):

        # extract info from file
        chromosome = splited_line[0].strip()
        start = splited_line[3].strip()
        end = splited_line[4].strip()
        strand = splited_line[6].strip()

        extended_id_info = parse_information_column(splited_line[8])

        self.chromosome = chromosome
        self.start = int(start)
        self.end = int(end)
        self.strand = strand

        self.transcript_id = extended_id_info.get('transcript_id', '')
        self.id = extended_id_info.get('transcript_id', '')
        self.gene_id = extended_id_info.get('gene_id', '')


        self.class_code = extended_id_info.get('class_code', '')
        self.tss_id = extended_id_info.get('tss_id', '')
        self.ref_gene_name = extended_id_info.get('ref_gene_name', '')
        self.cov = float( extended_id_info.get('cov', '0.0'))
        self.fpkm = float( extended_id_info.get('FPKM', '0.0'))
        self.tpm = float( extended_id_info.get('TPM', '0.0'))

        if  extended_id_info.get('tss_id', False):
            self.tss_id = extended_id_info['tss_id']
        else:
            self.tss_id = '{}{}'.format(chromosome, start)

        # Auto
        self.exons = []

    def add_exon(self, exon):
        #exon = Exon(self, chr_map, sequence)
        self.exons.append(exon)
        return

    def fill_seq(self, genome_reference):

        try:
            for e in self.exons:

                e.nseq =  genome_reference[self.chromosome][e.start - 1:e.end]
        except:
            raise

    def n_seq(self):
        nucleotide_seq = ''
        for exon in self.exons:
            nucleotide_seq += exon.nseq
        return nucleotide_seq

    # def extract_exon_ids(self):
    #     for e in self.exons:
    #         self.list_exon_ids.append(e.id)
    #     return

    def iterpairs(self):
        for x in range(len(self.exons) - 1):
            pair = [self.exons[x], self.exons[x + 1]]
            if len(pair) == 2:
                yield pair
            else:
                return

    def get_exon(self,exon_number):
        for e in self.exons:
            if e.exon_number == int(exon_number):
                return e
        return None

    def __str__(self):


        return 'ID: {}\n'.format(self.transcript_id)



class Transcriptome(object):

    def __init__(self):

        # Transcript_id: Transcript_object
        self.transcripts = dict()
        # Gene_id: list(Transcript_ids)
        self.genes = dict()
        self.chromosomes = dict()


    def add_gtf_exon(self,exon):
        ''' Load gtf exon information , group by transcript & Gene'''

        if exon.transcript_id in self.transcripts:
            self.transcripts[exon.transcript_id].add_exon(exon)

    def add_gtf_transcript(self, transcript):
        ''' Load gtf exon information , group by transcript & Gene'''

        if transcript.transcript_id not in self.transcripts:
            self.transcripts[transcript.transcript_id] = transcript

        # Add Gene-Transcript map
        if transcript.gene_id in self.genes:
            self.genes[transcript.gene_id].append(transcript.transcript_id)
        else:
            self.genes[transcript.gene_id] = [transcript.transcript_id]

        # Add Chromosome and count
        self.chromosomes[transcript.chromosome] = 1 + self.chromosomes.get(transcript.chromosome,0)



    def __str__(self):

        return 'Genes: {}\nTranscripts: {} '.format(len(self.genes),len(self.transcripts))


def parse_information_column(information):
    """ Extract ID info, gene_id, transcript_id, etc..

    read key value pairs and save it in to a dict

    Parameters
    ----------

    Returns
    -------
    dict


    """

    information = information.split(';')
    id_info = dict()
    for item in information:
        item = item.split('"')
        if len(item) > 1:
            # Use gtf labels
            # gene_id, transcript_id, exon_number, class_mode, tss_id
            id_info[item[0].strip()] =  item[1].strip()
        else:
            # end of line
            pass

    return id_info


def read_gtf(filename):
    """Load gtf file  in to a Transcriptome Object.

    Parameters
    ----------
    filename : str
        gtf file

    Returns
    -------




    """
    # init object
    sample_transcriptome = Transcriptome()
    # Parse file
    with open(filename,'r') as input_file:
        for line in input_file:
            if line.strip().endswith(';'):
                column = line.split('\t')
                if len(column) == 9:
                    if column[2] == 'exon':
                        # exon coordinates
                        # create Exon with the information
                        e = Exon(column)
                        # Add Exon to genome or transcriptome
                        sample_transcriptome.add_gtf_exon(e)

                    if  column[2] == 'transcript':
                        # create Exon with the information
                        t = Transcript(column)

                        # Add Exon to genome or transcriptome
                        sample_transcriptome.add_gtf_transcript(t)


    return sample_transcriptome

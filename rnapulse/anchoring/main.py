from __future__ import print_function
from __future__ import division

import sys
import os
import linecache
import logging

#
from . import blast
from . import filtermap
from . import generate_index_and_libs


logger = logging.getLogger(__name__)



def anchor_searching(config):

    # ##########################################################################
    # BLAST

    logger.info("PIPELINE: Now blasting the events.fa file from: %s ", config['SAMPLE_ID'])
    blast_exit_code = blast.blast_events(config["UNIPROT_FASTA_LOCATION"],
                                         config['EVENTS_FASTA'], 
                                         config['BLAST_OUTPUT'],
                                         config['CPUS'])

    ##########################################################################


    if blast_exit_code == 0:
        logger.info("Blast success!")
    else:
        logger.error("Blast Error: %s ", blast_exit_code)
        sys.exit()

def index_generation(config):

    try:

        # temp files
        config['FILTERMAP_NOT_LEN_COLLAPSED_OUTPUT'] = '{}/temp/rnaseq_huniprot_corrected_len.txt'.format(config['PREPROCESS_OUTPUT'])
        config['FILTERMAP_LEN_COLLAPSED_OUTPUT'] = '{}/temp/rnaseq_huniprot_corrected_len_collapsed.txt'.format(config['PREPROCESS_OUTPUT'])

        # Generate mapping between uniprot to splicing events with filter map
        # select by lenght and exon mapping
        logger.info("PIPELINE: Running filtermap 1")

        filtermap.filter_map1(config['BLAST_OUTPUT'], config['FILTERMAP_NOT_LEN_COLLAPSED_OUTPUT'])

        # remove redundancy
        logger.info("PIPELINE: Running filtermap 2")
        filtermap.filter_map2(config['FILTERMAP_NOT_LEN_COLLAPSED_OUTPUT'], config['FILTERMAP_LEN_COLLAPSED_OUTPUT'])


        ##########################################################################
        # generate index

        logger.info("PIPELINE: Running generate cDNA & Protein")
        generate_index_and_libs.generate1(config['FILTERMAP_LEN_COLLAPSED_OUTPUT'], config['COMPLETE_TRANSCRIPTS'],
                                          config['CDNA'], config['PSEQ'])

        ##########################################################################

        # Extract Anchor info (len) return dict()
        logger.info("PIPELINE: Running filtermap 3")
        uniprot_ddbb = filtermap.filter_map3(config['UNIPROT_FASTA_LOCATION'])

        # Extract Putative Isoform info (len) return dict()
        logger.info("PIPELINE: Running filtermap 4")
        isoforms_ddbb = filtermap.filter_map4(config['PSEQ'])



        # previously called Feline Parser , Generate map index file
        logger.info("PIPELINE: Running filtermap 5")
        filtermap.filter_map6(config['FILTERMAP_LEN_COLLAPSED_OUTPUT'], config['ANCHOR_EVENT_INIDEX_MAP'],
                              uniprot_ddbb, isoforms_ddbb)


    except:
        logger.error("ERROR. MAPPING GENERATION")
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        logger.error("EXCEPTION IN (%s, LINE %s %s): %s", filename, lineno, line.strip(), exc_obj)

        # TODO: Write better error statement for debugging.

from __future__ import print_function
from __future__ import division

from Bio.Seq import Seq
import linecache
import sys
import logging

from . import generate_index_and_libs_functions as gfuncs

logger = logging.getLogger(__name__)


def generate1(blastx_file, complete_transcripts_location,
              cdna_output_location, pseq_output_location):
    """

    :param blastx_file Refined blastx output:
    :param complete_transcripts_location:
    :param filtermap_uniprot_exon_indices_map:
    :param cdna_output_location:
    :param pseq_output_location:
    :return:
    """
    try:
        blastx_output = open(blastx_file, 'r')


        cdna_output = open(cdna_output_location, 'w')
        pseq_output = open(pseq_output_location, 'w')

        for blastx_hit in blastx_output:

            # try:
            (query_id, subject_id, perc_identity, aln_length, mismatch_count,
             gap_open_count, query_start, query_end, subject_start,
             subject_end, e_val, bit_score) = blastx_hit.split('\t')

            # get_n_seq is dealing with the fact that  query_id = (transcript+event_id)

            n_seq = Seq(gfuncs.get_n_seq(query_id, complete_transcripts_location))

            if int(query_start) > int(query_end):

                pseq, cdna_seq, s, e = gfuncs.translate_backwards(n_seq, int(query_start), int(query_end))

            else:
                tn_seq = str(n_seq)
                pseq, cdna_seq = gfuncs.translate(tn_seq, int(query_start))

            # Save AA seq
            print('>' + str(query_id), file = pseq_output)
            print(pseq, file = pseq_output)

            # Save CDNA seq
            print('>' + str(query_id), file = cdna_output)
            print(cdna_seq, file = cdna_output)


    except:
        logger.error("ERROR!! Index generation")
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        logger.error('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


    pseq_output.close()
    cdna_output.close()

from __future__ import print_function
from __future__ import division
import sys
import logging

from rnapulse.support import parsing_header

logger = logging.getLogger(__name__)

def p_beg_end(p_start, pend, p_length):
    phend = (pend + THRESHOLD // 3 > p_length)
    phbeg = (p_start - THRESHOLD // 3 < 1)

    return [phbeg, phend]


def m_beg_end(n_start, n_end, n_length, backwards):
    if backwards:
        temp = n_start
        n_start = n_end
        n_end = temp
    mhend = (n_end + THRESHOLD > n_length)
    mhbeg = (n_start - THRESHOLD < 1)

    return [mhbeg, mhend]


# CONSTANTS
# TODO: Move these to preprocess_settings.json
THRESHOLD = 9  # nucleotides
PINDENT_THRESHOLD = 95.0
IGNORE_EXCLUSION = False
DEAL_WITH_NUCLEOTIDE = False


def filter_map1(blast_file, rna_seq_output):
    read_from = open(blast_file, 'r')
    write_to = open(rna_seq_output, 'w')

    for line in read_from:
        (query_id, subject_id, perc_identity, aln_length, mismatch_count, gap_open_count,
         query_start, query_end, subject_start, subject_end, e_val, bit_score) = line.split('\t')

        try:
            # PARSING ID
            nucleotide_lengths = parsing_header.get_nucleotide_lengths(query_id)
            #snucleotide_lengths = query_id.split("-")[-1].split("=")
            #nucleotide_lengths = [int(snucleotide_lengths[0]),
            #                      int(snucleotide_lengths[1]),
            #                      int(snucleotide_lengths[2])]
        except:

            logger.debug("WARNING WITH  %s", query_id)

        p_start = int(subject_start)  # /3.0
        p_end = int(subject_end)  # /3.0
        p_length = int(aln_length)  # /3.0

        if DEAL_WITH_NUCLEOTIDE:
            p_start /= 3.0
            p_end /= 3.0
            p_length /= 3.0

        n_start = int(query_start)
        n_end = int(query_end)
        n_length = sum(nucleotide_lengths)

        pident = float(perc_identity)

        if query_id[0] == "E":
            if IGNORE_EXCLUSION:
                continue
            n_length -= nucleotide_lengths[1]

        to_print = True

        backwards = (n_start > n_end)

        [hit_p_beg, hit_p_end] = p_beg_end(p_start, p_end, p_length)
        [hit_n_beg, hit_n_end] = m_beg_end(n_start, n_end, n_length, backwards)

        exist_beg = True
        exist_end = True

        if not hit_n_beg and not hit_p_beg:
            to_print = False
            exist_beg = False

        if not hit_n_end and not hit_p_end:
            to_print = False
            exist_end = False

        # if not (exist_beg or exist_end):
        #     print line.strip() + " no_both"
        # elif not exist_beg:
        #     if backwards:
        #         print line.strip() + " no_c2"
        #     else:
        #         print line.strip() + " no_c1"
        # elif not exist_end:
        #     if backwards:
        #         print line.strip() + " no_c1"
        #     else:
        #         print line.strip() + " no_c2"
        else:
            hit_length = n_end - n_start + 1
            if hit_length < 0:
                hit_length *= -1
                hit_length += 2

            if ((hit_length + THRESHOLD) / 3 < p_end - p_start + 1) or (
                    (hit_length - THRESHOLD) / 3 > p_end - p_start + 1):
                to_print = False
                # print line.strip() + " missing_chunks"

        if pident < PINDENT_THRESHOLD:
            to_print = False
            # print line.strip() + " pident_failure"

        if query_id[0] == "E":
            # make sure A site is within
            if not backwards:
                if not (n_start <= nucleotide_lengths[0] <= n_end):
                    to_print = False
            else:
                if not (n_end <= nucleotide_lengths[0] <= n_start):
                    to_print = False

        if to_print:
            print(line.strip()[0:len(line)],file=write_to)


##########################################################################


def filter_map2(read_from, write_to):
    # filters multiple uniprot hits by taking the best one in terms of evalue
    read_from = open(read_from, 'r')
    write_to = open(write_to, 'w')

    old_id = ""

    for line in read_from:
        tokens = line.split('\t')
        # PARSING ID
        asid = tokens[0]
        _id = asid

        if old_id != _id:
            print(line.strip(), file=write_to)

        old_id = _id

##########################################################################


def filter_map3(uniprot_fasta, sep = '|'):
    """
    Load uniprot Fasta seq
    Returns uniprot_ddbb
    ID:sequence length

    Parameters
    ==========
        :uniprot_fasta (str) file path

    Return
    ------
        :uniprot_ddbb (dict) ID:length
    """

    uniprot_ddbb = {}
    seq = ''
    uid = ''

    with open(uniprot_fasta, 'r') as input_file:
        for line in input_file:
            if line[0] == '>':
                uniprot_ddbb[uid] = len(seq)
                # PARSING ID
                # Get ACC ID and remove >
                uid = line.split(sep)[1]
                seq = ''
            # PARSING ID
            else:
                seq += line.strip()

    uniprot_ddbb[uid] = len(seq)
    return uniprot_ddbb

##########################################################################


def filter_map4(isoform_fasta):
    """
    Load isoform fasta Seq to Dict.
    ID:sequence length

    Parameters
    ==========
        :isoform_fasta (str) file path

    Return
    ------
        :isoforms_ddbb (dict) ID:length


    """
    isoforms_ddbb = {}
    seq = ''
    uid = ''


    with open(isoform_fasta, 'r') as input_file:
        for line in input_file:
            if line[0] == '>':
                isoforms_ddbb[uid] = len(seq)
                # PARSING ID
                # remove >
                uid = line[1:].strip()
                seq = ''

            else:
                seq += line.strip()

    isoforms_ddbb[uid] = len(seq)

    return isoforms_ddbb

##########################################################################


def filter_map5(read_from):
    """

    :param read_from:
    :return:
    """
    # tags on spliced exon indices on query hits using p_start pend n_start and n_end along with A exon positions
    # this information is obtained from the splice sequence to uniprot mapping file
    read_from = open(read_from, 'r')

    has_mapping = {}

    for line in read_from:
        tokens = line.split('\t')
        # PARSING ID
        _id = tokens[0]  # asid
        uniprot = tokens[1].split("|")[1]
        # PARSING ID
        if _id in has_mapping:
            has_mapping[_id].append(uniprot)
        else:
            has_mapping[_id] = [uniprot]

    read_from.close()
    return has_mapping

##########################################################################


def filter_map6(read_from, write_to, uniprot_ddbb, isoforms_ddbb):

    has_mapping = filter_map5(read_from)

    read_from = open(read_from, 'r')
    write_to = open(write_to, 'w')

    # has_mapping = filter_map5(read_from)

    for line in read_from:
        tokens = line.split('\t')

        # EVENT ID
        as_id = tokens[0].strip()  # asid
        # Getting Uniprot ID
        uniprot = tokens[1].split("|")[1]


        # PARSING EVENT ID
        # Getting transcript ID & exons lengths
        transcript_isoform = parsing_header.get_transcriptid(as_id)
        nucleotide_lengths = parsing_header.get_nucleotide_lengths(as_id)
        # Getting Protein mapping coordinates
        p_start = int(tokens[8])
        p_end = int(tokens[9])
        n_start = int(tokens[6])
        n_end = int(tokens[7])

        if n_start < n_end:
            c1_hit_length = nucleotide_lengths[0] - n_start + 1
            c2_hit_length = nucleotide_lengths[2] - (sum(nucleotide_lengths) - n_end + 1)
        else:
            c1_hit_length = nucleotide_lengths[0] - n_end + 1
            c2_hit_length = nucleotide_lengths[2] - (sum(nucleotide_lengths) - n_start + 1)

        if c1_hit_length < 0:
            c1_hit_length = 0
        if c2_hit_length < 0:
            c2_hit_length = 0

        a_start = p_start + ( c1_hit_length // 3)
        a_end = p_end - ( c2_hit_length // 3 )


        # this code looks wired; I would say that a big part is not
        # necesary
        try:
            if (a_start and a_end) > 0 and a_end >= a_start and "-" not in uniprot and isoforms_ddbb[as_id] > 1:
                type_id = parsing_header.get_type(as_id)
                if type_id == "I":
                    list_prot = []
                    if ("E" + as_id[1:len(as_id)]) in has_mapping:
                        list_prot = has_mapping["E" + as_id[1:len(as_id)]]
                    event_mapping_info =  as_id + "\t" + uniprot + "\t" + repr(p_start) + "\t" + repr(a_start) + \
                                       "\t" + repr(a_end) + "\t" + repr(p_end) + "\t" + repr(list_prot) + \
                                       "\t" + str(uniprot_ddbb[uniprot]) + "\t" + str(isoforms_ddbb[as_id])
                else:
                    list_prot = []
                    if ("I" + as_id[1:len(as_id)]) in has_mapping:
                        list_prot = has_mapping["I" + as_id[1:len(as_id)]]
                    event_mapping_info =  as_id + "\t" + uniprot + "\t" + repr(p_start) + "\t" + repr(a_start) + \
                                       "\t" + repr(a_start) + "\t" + repr(p_end) + "\t" + repr(list_prot) + \
                                       "\t" + str(uniprot_ddbb[uniprot]) + "\t" + \
                                       str(isoforms_ddbb[as_id])

                print(event_mapping_info, file=write_to)

            else:
                pass

        except KeyError as e:

            logger.warning(" WARNING %s NOT FOUND ", str(e))
            logger.warning(" WARNING Please check your Uniprot file or/and your poll of isoforms")
            # print id


def main(argv):

    '''For Debugging'''

    cell_line = ''
    ref_genome = ''
    pulse_path = ''
    output_path = ''
    uniprot_ddbb = '/home/kimlab2/ccorbi/Webpulse/support/uniprot/uniprot_sprot.fasta'

    uniprot_ddbb = filter_map3(uniprot_ddbb)

    # Extract Putative Isoform info (len) return dict()

    isoforms_ddbb = filter_map4(argv[2])
    #read_from, write_to, uniprot_ddbb, isoforms_ddbb
    # h = filter_map5(argv[0])
    filter_map6(argv[0],argv[1],uniprot_ddbb,isoforms_ddbb)

if __name__ == "__main__":
    main(sys.argv[1:])

from subprocess import Popen, PIPE, STDOUT
import logging

logger = logging.getLogger(__name__)


def blast_events(uniprot_file_location, events_file_location, output_location, cpus='1'):
    """

    :param uniprot_file_location:
    :param events_file_location:
    :param output_location:
    :return:
    """
    command1 = ['blastx',
                '-db',
                uniprot_file_location,
                '-query',
                events_file_location,
                '-out',
                output_location,
                '-max_target_seqs', '1',
                '-outfmt', '6',
                '-num_threads', cpus]

    logger.info("RUN: blast: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_code = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))

    return exit_code


def blastall(uniprot_file_location, events_file_location, output_location):
    """

    :param uniprot_file_location:
    :param events_file_location:
    :param output_location:
    :return:
    """
    command1 = ['blastall',
                '-p',
                'blastx',
                '-d',
                uniprot_file_location,
                '-i',
                events_file_location,
                '-o',
                output_location,
                '-v1',
                '-b1',
                '-m8']

    logger.info("RUN: blast: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_code = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))

    return exit_code

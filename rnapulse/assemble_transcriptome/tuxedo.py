from __future__ import print_function
from __future__ import division

import sys
import getopt
from subprocess import Popen, PIPE, STDOUT
import logging

logger = logging.getLogger(__name__)


def run_stringtie(config):

    """Call StringTie.

    Parameters
    ----------
    config: dict

    Returns
    -------

    """
    # stringtie /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/data/output/assembly/TCGA-14-1825.bam/no-rg/TCGA-14-1825.bam -p 4 -o transcriptome.gtf
    #-G /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/support/genomes/GRCh38.refannotation.gtf -A gene_abund.tab -C cov_refs.gtf -B
    command1 = [config['STRINGTIE_BIN'],
                '-o',
                config['TRANSCRIPT_OUT'],
                '-G',
                config["REF_ANNOTATION"],
                config['FOLDER_RNASEQ_INPUT'] + config["SAMPLE_ID"] + '.bam',
                '-p',
                config['CPUS'] ]

    logger.info("RUN: StringTie: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished StringTie for: %s", config['SAMPLE_ID'])

    return exit_codes

def run_stringtie_merge(config):
    """Call Merge StringTie.

    Parameters
    ----------
    config: dict

    Returns
    -------

    """
    # stringtie /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/data/output/assembly/TCGA-14-1825.bam/no-rg/TCGA-14-1825.bam -p 4 -o transcriptome.gtf
    #-G /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/support/genomes/GRCh38.refannotation.gtf -A gene_abund.tab -C cov_refs.gtf -B
    command1 = [config['STRINGTIE_BIN'],
                '--merge',

                '-G',
                config["REF_ANNOTATION"],

                '-o',
                config['TRANSCRIPT_MERGE'],

                config['TRANSCRIPT_OUT'],
                '-p',
                config['CPUS']]


    logger.info("RUN: StringTie Merging: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished StringTie Merging for: %s", config['SAMPLE_ID'])

    return exit_codes

def run_GFFcompare(config):
    """Call gFFCOMAPRE.

    Parameters
    ----------
    config: dict

    Returns
    -------

    """
    command1 = [config['GFFCOMAPRE_BIN'],

                '-r',
                config["REF_ANNOTATION"],
                '-o',
                config['ASSEMBLY_OUTPUT'] +  'strtcmp',
                config['ASSEMBLY_OUTPUT'] + config['TRANSCRIPT_MERGE']]


    logger.info("RUN: gff COMAPRE: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished Sgff COMAPRE: %s", config['SAMPLE_ID'])

    return exit_codes


def run_stringtie_estimate(config):
    """Call  StringTie, Estimate.

    Parameters
    ----------
    config: dict

    Returns
    -------

    """
    # stringtie /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/data/output/assembly/TCGA-14-1825.bam/no-rg/TCGA-14-1825.bam -p 4 -o transcriptome.gtf
    #-G /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/support/genomes/GRCh38.refannotation.gtf -A gene_abund.tab -C cov_refs.gtf -B
    command1 = [config['STRINGTIE_BIN'],
                '-e',
                '-B',
                '-A',
                config['ASSEMBLY_OUTPUT'] + '/gene_abund.tab',
                '-C',
                config['ASSEMBLY_OUTPUT'] +  '/cov_refs.gtf',
                '-G',
                config['TRANSCRIPT_MERGE'],

                config['FOLDER_RNASEQ_INPUT'] + config["SAMPLE_ID"] + '.bam',

                '-o',
                config['TRANSCRIPT_ASSEMBLY'],
                '-p',
                config['CPUS']]

    logger.info("RUN: StringTie Estimating: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished StringTie Estimating for: %s", config['SAMPLE_ID'])

    return exit_codes


#
# def run_cufflinks(config):
#     """Call cufflinks.
#
#     Parameters
#     ----------
#
#     Returns
#     -------
#
#     """
#     command1 = [config['CUFFLINKS_BIN'],
#                 '-o',
#                 config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']\
#                  + config["SAMPLE_ID"] + config['FOLDER_CUFFLINKS'],
#                 '-g',
#                 config['PULSE_PATH'] + config["REF_ANNOTATION"],
#                 config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']\
#                                      + config["SAMPLE_ID"]\
#                                      + config['FOLDER_SAMTOOLS_TEMP']\
#                                      + config["SAMPLE_ID"],
#                 '-q']
#
#     logger.info("RUN: cufflinks: %s", command1)
#     p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
#     exit_codes = p1.wait()
#     # pass std output to log file
#     output = p1.stdout.read()
#     logger.info(output.decode("utf-8"))
#     logger.info("Finished cufflinks for: %s", config['SAMPLE_ID'])
#
#     return exit_codes
#
#
#
# def run_cuffcompare(config):
#     """Compare De novo transfag to a reference Genome.
#
#     """
#
#     command1 = [config['CUFFCOMPARE_BIN'],
#                 config['PULSE_PATH'] + config["REF_ANNOTATION"],
#                 config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
#                 config['FOLDER_CUFFLINKS'] + config['FILENAME_TRANSCRIPTOME_OUTPUT'],
#                 '-s', config['PULSE_PATH'] + config["FOLDER_GENOME_SEQ"],
#                  '-C']
#
#     logger.info("RUN: cuffcompare: %s", command1)
#     p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
#     exit_codes = p1.wait()
#     # pass std output to log file
#     output = p1.stdout.read()
#     logger.info(output.decode("utf-8"))
#     logger.info("Finished cuffcompare for: %s", config['SAMPLE_ID'])
#
#     return exit_codes
#
#
# def run_cuffmerge(config):
#     """Merge Transfags  to a reference genome.
#
#     Help to eliminate likely artifacts and assemblies not associated with Ensembl genes
#
#     """
#     # create manifest file
#     create_input_file(config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
#                 config['FOLDER_CUFFLINKS'], config['FILENAME_TRANSCRIPTOME_OUTPUT'])
#
#     command1 = [config['CUFFMERGE_BIN'],
#                 '--ref-gtf',
#                 config['PULSE_PATH'] + config["REF_ANNOTATION"],
#                 '-o',
#                 config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
#                 config['FOLDER_CUFFLINKS']+config['FOLDER_CUFFMERGE_OUTPUT'],
#                 config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
#                 config['FOLDER_CUFFLINKS']+'assembly_list.txt']
#
#     logger.info("RUN: cuffmerge: %s ", command1)
#     p1 = Popen(command1,  stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
#     exit_codes = p1.wait()
#     output = p1.stdout.read()
#     logger.info(output.decode("utf-8"))
#
#     return exit_codes
#
# def create_input_file(path, *args):
#     """Create a file with all the gtf files to merge.
#
#
#     """
#     manifest = open(path+'assembly_list.txt','w')
#
#     for fname in args:
#         print(path+fname, file=manifest)
#
#     manifest.close()
#
#     return
#
#
# def cuffcompare_clean(config):
#     """Retrain_high_quality_transcripts From the cuffcompare output, extract hight quality transcriptomes.
#
#
#     Cuff compare classifies assembled transcripts into multiple categories
#     in relation to reference transcripts. We retrain only transcripts that were deemed
#     'equal' '=', 'containe' 'c' or 'new splice isoforms' 'j'
#
#
#     """
#     filename = config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
#                 config['FOLDER_CUFFLINKS'] + config['CUFFCOMPARE_OUTPUT']
#     output = open(config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
#         config['FOLDER_CUFFLINKS'] + config['CUFFCOMPARE_OUTPUT_CLEAN'],'w')
#
#     with open(filename,'r') as input_file:
#         for line in input_file:
#             tab_data =line.split('\t')
#             if tab_data[2] in ['=','j','c','class_code']:
#
#                 print(line.strip(),file=output)
#
#     return
#
#
#
#
# def run_samtools(config):
#     """Runs samtools pipe to grep and back to samtools.
#
#
#     . : Notes
#
#     ```bash
#         samtools view -h $file | grep -v ^@RG | samtools view -Sbh - > no-rg/$output
#     ```
#
#     :param pulse_path:
#     :param cell_line_for_samtools:
#     :return:
#     """
#     INPUT_FILE = config['PULSE_PATH'] + config['FOLDER_RNASEQ_INPUT'] + config['SAMPLE_ID']
#     OUTPUT_FILE = config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] +\
#                         config['SAMPLE_ID'] + config['FOLDER_SAMTOOLS_TEMP'] +\
#                         config['SAMPLE_ID']
#
#     logger.info("Running samtools for: %s", config['SAMPLE_ID'])
#     logger.info("BAM FILE:  %s", INPUT_FILE)
#     logger.info("CLEAN BAM FILE:  %s", OUTPUT_FILE)
#
#     command = '{} view -h {} | grep -v ^@RG | {} view -Sbh - > {}'.format(config['SAMTOOLS_BIN'],
#                                                                           INPUT_FILE,
#                                                                           config['SAMTOOLS_BIN'],
#                                                                           OUTPUT_FILE)
#     p = Popen( command, shell=True)
#     exit_codes = p.wait()
#
#     logger.info("Finished samtools for: %s", config['SAMPLE_ID'])
#
#     return exit_codes
#
#
# def main(argv):
#
#
#     try:
#         opts, args = getopt.getopt(argv, "i:o:")
#         print(opts)
#     except getopt.GetoptError:
#         print('samtools.py -i <bam file> -o <output>')
#         sys.exit(2)
#
#     for opt, arg in opts:
#         if opt == '-h':
#             print('samtools.py -i <bam file> -o <output>')
#             sys.exit()
#         elif opt in ("-i"):
#             input_bam = arg
#         elif opt in ("-o",):
#             output = arg
#
#     # create a moak configuration
#     config = dict()
#     # config['PULSE_PATH'] = os.getcwd() + '/'
#     config['SAMTOOLS_BIN'] = 'samtools'
#     config['SAMPLE_ID'] = input_bam
#     config['FOLDER_RNASEQ_INPUT'] = ''
#     config['ASSEMBLY_OUTPUT'] = ''
#     config['FOLDER_SAMTOOLS_TEMP'] = output
#
#
#
#     print(config)
#     run_samtools(config)
#
# if __name__ == "__main__":
#     main(sys.argv[1:])

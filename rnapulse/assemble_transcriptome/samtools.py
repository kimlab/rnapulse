import sys
import os
import getopt
from subprocess import Popen
import logging

logger = logging.getLogger(__name__)

def run_samtools(config):
    """Runs samtools pipe to grep and back to samtools.


    . : Notes

    ```bash
        samtools view -h $file | grep -v ^@RG | samtools view -Sbh - > no-rg/$output
    ```

    :param pulse_path:
    :param cell_line_for_samtools:
    :return:
    """
    INPUT_FILE = config['PULSE_PATH'] + config['FOLDER_RNASEQ_INPUT'] + config['SAMPLE_ID']
    OUTPUT_FILE = config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] +\
                        config['SAMPLE_ID'] + config['FOLDER_SAMTOOLS_TEMP'] +\
                        config['SAMPLE_ID']

    logger.info("Running samtools for: %s", config['SAMPLE_ID'])
    logger.info("BAM FILE:  %s", INPUT_FILE)
    logger.info("CLEAN BAM FILE:  %s", OUTPUT_FILE)

    command = '{} view -h {} | grep -v ^@RG | {} view -Sbh - > {}'.format(config['SAMTOOLS_BIN'],
                                                                          INPUT_FILE,
                                                                          config['SAMTOOLS_BIN'],
                                                                          OUTPUT_FILE)
    p = Popen( command, shell=True)
    exit_codes = p.wait()

    logger.info("Finished samtools for: %s", config['SAMPLE_ID'])
    
    return exit_codes


def main(argv):


    try:
        opts, args = getopt.getopt(argv, "i:o:")
        print(opts)
    except getopt.GetoptError:
        print('samtools.py -i <bam file> -o <output>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('samtools.py -i <bam file> -o <output>')
            sys.exit()
        elif opt in ("-i"):
            input_bam = arg
        elif opt in ("-o",):
            output = arg

    # create a moak configuration
    config = dict()
    config['PULSE_PATH'] = os.getcwd() + '/'
    config['SAMTOOLS_BIN'] = 'samtools'
    config['SAMPLE_ID'] = input_bam
    config['FOLDER_RNASEQ_INPUT'] = ''
    config['ASSEMBLY_OUTPUT'] = ''
    config['FOLDER_SAMTOOLS_TEMP'] = output



    print(config)
    run_samtools(config)

if __name__ == "__main__":
    main(sys.argv[1:])

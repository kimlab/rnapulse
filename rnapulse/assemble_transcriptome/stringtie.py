from __future__ import print_function
from __future__ import division

import sys
import getopt
from subprocess import Popen, PIPE, STDOUT
import logging

logger = logging.getLogger(__name__)


def run_stringtie(config):

    """Call StringTie.

    Parameters
    ----------
    config: dict

    Returns
    -------

    """
    # stringtie /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/data/output/assembly/TCGA-14-1825.bam/no-rg/TCGA-14-1825.bam -p 4 -o transcriptome.gtf
    #-G /home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/support/genomes/GRCh38.refannotation.gtf -A gene_abund.tab -C cov_refs.gtf -B
    command1 = [config['STRINGTIE_BIN'],
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']\
                                     + config["SAMPLE_ID"]\
                                     + config['FOLDER_SAMTOOLS_TEMP']\
                                     + config["SAMPLE_ID"],

                '-o',
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']\
                                     + config["SAMPLE_ID"]\
                                     + config['FILENAME_TRANSCRIPTOME_OUTPUT'],
                '-G',
                config['PULSE_PATH'] + config["REF_ANNOTATION"],
                '-A', 'gene_abund.tab',
                '-C', 'cov_refs.gtf',
                '-B']

    logger.info("RUN: StringTie: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished StringTie for: %s", config['SAMPLE_ID'])

    return exit_codes

def run_cufflinks(config):
    """Call cufflinks.

    Parameters
    ----------

    Returns
    -------

    """
    command1 = [config['CUFFLINKS_BIN'],
                '-o',
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']\
                 + config["SAMPLE_ID"] + config['FOLDER_CUFFLINKS'],
                '-g',
                config['PULSE_PATH'] + config["REF_ANNOTATION"],
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']\
                                     + config["SAMPLE_ID"]\
                                     + config['FOLDER_SAMTOOLS_TEMP']\
                                     + config["SAMPLE_ID"],
                '-q']

    logger.info("RUN: cufflinks: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished cufflinks for: %s", config['SAMPLE_ID'])

    return exit_codes



def run_cuffcompare(config):
    """Compare De novo transfag to a reference Genome.

    """

    command1 = [config['CUFFCOMPARE_BIN'],
                config['PULSE_PATH'] + config["REF_ANNOTATION"],
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
                config['FOLDER_CUFFLINKS'] + config['FILENAME_TRANSCRIPTOME_OUTPUT'],
                '-s', config['PULSE_PATH'] + config["FOLDER_GENOME_SEQ"],
                 '-C']

    logger.info("RUN: cuffcompare: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    logger.info("Finished cuffcompare for: %s", config['SAMPLE_ID'])

    return exit_codes


def run_cuffmerge(config):
    """Merge Transfags  to a reference genome.

    Help to eliminate likely artifacts and assemblies not associated with Ensembl genes

    """
    # create manifest file
    create_input_file(config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
                config['FOLDER_CUFFLINKS'], config['FILENAME_TRANSCRIPTOME_OUTPUT'])

    command1 = [config['CUFFMERGE_BIN'],
                '--ref-gtf',
                config['PULSE_PATH'] + config["REF_ANNOTATION"],
                '-o',
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
                config['FOLDER_CUFFLINKS']+config['FOLDER_CUFFMERGE_OUTPUT'],
                config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
                config['FOLDER_CUFFLINKS']+'assembly_list.txt']

    logger.info("RUN: cuffmerge: %s ", command1)
    p1 = Popen(command1,  stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_codes = p1.wait()
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))

    return exit_codes

def create_input_file(path, *args):
    """Create a file with all the gtf files to merge.


    """
    manifest = open(path+'assembly_list.txt','w')

    for fname in args:
        print(path+fname, file=manifest)

    manifest.close()

    return


def cuffcompare_clean(config):
    """Retrain_high_quality_transcripts From the cuffcompare output, extract hight quality transcriptomes.


    Cuff compare classifies assembled transcripts into multiple categories
    in relation to reference transcripts. We retrain only transcripts that were deemed
    'equal' '=', 'containe' 'c' or 'new splice isoforms' 'j'


    """
    filename = config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
                config['FOLDER_CUFFLINKS'] + config['CUFFCOMPARE_OUTPUT']
    output = open(config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT'] + config['SAMPLE_ID'] + \
        config['FOLDER_CUFFLINKS'] + config['CUFFCOMPARE_OUTPUT_CLEAN'],'w')

    with open(filename,'r') as input_file:
        for line in input_file:
            tab_data =line.split('\t')
            if tab_data[2] in ['=','j','c','class_code']:

                print(line.strip(),file=output)

    return



def main(argv):
    pulse_path = ''
    cell_line_for_cufflinks = ''

    try:
        opts, args = getopt.getopt(argv, "hp:c:", ["path=", "cell_line="])
    except getopt.GetoptError:
        print('cufflinks.py -p <pulse_path> -c <cell_line_for_cufflinks>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('cufflinks.py -p <pulse_path> -c <cell_line_for_cufflinks>')
            sys.exit()
        elif opt in ("-p", "--path"):
            pulse_path = arg
        elif opt in ("-c", "--cell-line"):
            cell_line_for_cufflinks = arg
    # run_cufflinks(pulse_path, cell_line_for_cufflinks)



if __name__ == "__main__":
    main(sys.argv[1:])

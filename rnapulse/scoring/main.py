from __future__ import print_function
from __future__ import division
import glob
import pandas as pd
import numpy as np
import dill as pkl
import warnings
warnings.filterwarnings("ignore")
from sklearn.ensemble import RandomForestClassifier

#from subprocess import Popen, PIPE, STDOUT
import logging

logger = logging.getLogger(__name__)

def retrain_score(cfg):

    # load Training data
    train_features = pd.read_csv(cfg['TRAINING_DATASET'], sep=',')
    # init labels
    TP = int(cfg['TRAINING_DATASET_TP'])  # Curated Positives
    N = train_features.shape[0]  # TP + Unlabelled data

    labels_train = np.zeros(N)
    for i in range(N):
        if i < TP:
            labels_train[i] = 1
        else:
            labels_train[i] = 0

    
    models = list()
    num_interations = int(cfg['NUM_ITER'])
    for i in range(num_interations):
        logger.info("RUNNING TRAINING ITERATION: %i", i)

        # train random classifier
        Y = labels_train.copy()
        clf = RandomForestClassifier(n_estimators=2000, n_jobs=int(cfg['CPUS']), oob_score=True)
        clf.fit(train_features, labels_train)
        # get error out of the bag and use it to redefine 1/0 boundery
        # compute C value
        vec_oob = clf.oob_decision_function_
        err = vec_oob[0:,1:]
        cvalue = err[:TP].sum()/err.sum()
        # reassign labels
        for j in range(N):
            label = 0
            if j < TP:
                label =1
            else:
                # reformed probability 
                prob = err[j]
                mprob = ((1-cvalue)/cvalue)*(prob/(1-prob))
                cutoff = np.random.uniform(0,1)
                # relabel unlabelled data based on probability
                if cutoff <= mprob:
                    label =1
                else:
                    label = 0

            Y[j] = label
        # train new model with 
        model_random = RandomForestClassifier(n_estimators=2000, n_jobs=int(cfg['CPUS']), oob_score=True)
        model_random.fit(train_features, Y)
        models.append(model_random)

    # use ensemble to score
    data = pd.read_csv(cfg['FEATURES_ML'])
    scores = score_ensemble(models, data)
    save_score(scores, cfg)

    return

def fast_score(cfg):
    
    # load precalculated models
    dir_models  = glob.glob(cfg['MODELS'])
    models = list()
    for m in dir_models:
        logger.info("LOADING MODEL: %s", m)

        models.append(pkl.load(open(m,'rb')))

    # use them to  score
    data = pd.read_csv(cfg['FEATURES_ML'])
    scores = score_ensemble(models, data)
    save_score(scores, cfg)

    return



def score_ensemble(models, data):


    logger.info("SCORING")
    results = list()
    for m in models:
        r = m.predict_proba(data)[0:,1:]
        results.append(r)

    ensembl = np.asarray(results)
    scores = ensembl.sum(axis=0) / len(models)

    return scores


def save_score(scores, cfg):

    o = open(cfg['PULSE_OUTPUT'], 'w')
    scores.tofile(o, format="%.3f", sep='\n')
    o.close()

    return


def run_PULSE(config):

    if config['FAST_MODELS']:
        logger.info("FAST MODELS")
        fast_score(config)
    else:
        logger.info("FRESH MODELS ... slower method")
        retrain_score(config)   


from __future__ import print_function
from __future__ import division
import glob
import pandas as pd

from rnapulse.support.utils import is_non_zero_file, hashseqs
#from subprocess import Popen, PIPE, STDOUT
import logging
import warnings
warnings.filterwarnings("ignore")
logger = logging.getLogger(__name__)


def readfasta2dict(fastafile):
    """read fasta file and return dict fastaid:seq.


    Only works with fasta files generated in the PULSE pipeline (sequence is
        in one line)

    """
    fastadict = dict()
    with open(fastafile, 'r') as input_file:

        for line in input_file:
            if line.startswith('>'):
                _id = line[1:].strip()
            else:
                # check if already exist [just in case]
                if _id in fastadict:
                    raise('Error: Duplicated items in {}'.format(fastafile))
                else:
                    fastadict[_id] = line.strip()

    return fastadict


def fasta2df(fastafile, rename_map):

    if is_non_zero_file(fastafile):
        cdnaSeqIso = readfasta2dict(fastafile)
    else:
        logger.error('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.error('Fasta cdna transcripts file do not exist %s', fastafile)
        return pd.DataFrame()

    # Transform
    cdnaSeqIso_df = pd.DataFrame.from_dict(cdnaSeqIso, orient='index')
    cdnaSeqIso_df.reset_index(inplace=True)
    cdnaSeqIso_df.rename(columns=rename_map, inplace=True)

    return cdnaSeqIso_df


def read_anchor_map(config):
    if is_non_zero_file(config['ANCHOR_EVENT_INIDEX_MAP']):
        anchormap = pd.read_csv(config['ANCHOR_EVENT_INIDEX_MAP'],
                                names=['id_pulse', 'uniprot_id', 'map_e1', 'map_s2',
                                       'map_e2', 'map_s3', 'temp', 'len_anchor', 'len_transcript'],
                                sep='\t')
        return anchormap
    else:
        logger.error('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.error('Events index do not exist %s',
                     config['ANCHOR_EVENT_INIDEX_MAP'])
        return pd.DataFrame()


def csv2df(csvfile, **kargs):
    #
    if is_non_zero_file(csvfile):
        events = pd.read_csv(csvfile, **kargs)
        return events
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('names files do not exist %s', csvfile)
        return pd.DataFrame()


def wrap_pulse_transcripts(config):
    """wrap Pulse output in a single Pandas.DataFrame.

    Parameters
    ----------
        cell_source: str
            Name of the folder of the cell line
        pulseCfg_file: str
            Path to the PULSE json configuration file

    Returns
    -------
        transcripts: pandas.DataFrame
        id_pulse, uniprot_id, s1, e1, s2, e2, temp, len_anchor, len_transcript, aa_transcript, hash_transcript



    """
    # LOAD PULSE DATA
    # load Aminoacid transcripts seq and transrform to dataFrame to merge

    protSeqIso_df = fasta2df(
        config['PSEQ'], {0: 'aa_transcript', 'index': 'id_pulse'})
    cdnaSeqIso_df = fasta2df(
        config['CDNA'], {0: 'cdna_transcript', 'index': 'id_pulse'})
    # laod anchors
    anchormap = read_anchor_map(config)
    if not protSeqIso_df.empty and not cdnaSeqIso_df.empty and not anchormap.empty:
        transcripts = pd.merge(protSeqIso_df, cdnaSeqIso_df, on='id_pulse')

        # generate MD5 hash seq for each transcript SEQ
        transcripts['hash_transcript'] = transcripts['aa_transcript'].apply(
            hashseqs)

        logger.debug(anchormap.head())

        transcripts = pd.merge(transcripts, anchormap,
                               how='left', on='id_pulse')

        # drop transcripts without mapping
        transcripts.dropna(subset=['uniprot_id'], inplace=True, axis=0)
        logger.debug(transcripts.head())

    else:
        logger.error('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')

        logger.error(
            'An important file is missing, see below for more details, probably the pipeline fail')

        return pd.DataFrame()

    transcripts.drop_duplicates(subset=['hash_transcript'], inplace=True)
    transcripts = transcripts[['id_pulse', 'uniprot_id', 'len_transcript', 'aa_transcript',
                               'cdna_transcript', 'hash_transcript']]

    return transcripts


def wrap_pulse_events(config, transcripts_PULSE):
    """wrap Pulse output in a single Pandas.DataFrame.

    Parameters
    ----------
        transcripts_PULSE: pandas.DataFrame

        cell_source: str
            Name of the folder of the cell line
        pulseCfg_file: str
            Path to the PULSE json configuration file

    Returns
    -------
        transcripts: pandas.DataFrame
        id_pulse, uniprot_anchor, s1, e1, s2, e2, temp, len_anchor, len_transcript,  pulse_score, seq, checksum



    """


    # load transcript names
    events = csv2df(config['NAMES_ML'], names=['id_pulse', 'uniprot_id'], sep='//')
    # scores
    score = csv2df(config['PULSE_OUTPUT'], names=['pulse_score'])

    # Anchors maps
    # load transcript maps
    anchormap = read_anchor_map(config)
    # cdna event
    cdna_events_df = fasta2df(config['EVENTS_FASTA'], {0: 'cdna_event', 'index': 'id_pulse'})


    
    # MERGE and transform
    # add score
    if not events.empty and not score.empty:
        events = pd.concat([events, score], axis=1)
    else:
        return pd.DataFrame()
    if not anchormap.empty:
        # add uniprot id and mapp information
        events = pd.merge(anchormap, events, on=['id_pulse', 'uniprot_id'])
        # add cdna of the event
        events = pd.merge(cdna_events_df, events, on='id_pulse')
        # event  hash
        events['event_hash'] = events['id_pulse'] + events['cdna_event']
        events['event_hash'] = events['event_hash'].apply(hashseqs)

        # add isofomr hash
        events = pd.merge(events, transcripts_PULSE[['id_pulse', 'hash_transcript']], how='left', on='id_pulse')

        return events

    else:
        return pd.DataFrame()




def summary_pulse_output(config):

    # -------------
    # I S O F O R M
    # Get Transcripts sequences and events related
    transcripts_PULSE = wrap_pulse_transcripts(config)
    # transcripts_PULSE.to_csv(config['SUMMARY_OUTPUT']+'transcripts.csv', index=False)

    # Get events and scores
    events_PULSE = wrap_pulse_events(config, transcripts_PULSE)
    events_PULSE.to_csv(config['SUMMARY_OUTPUT']+'events.csv', index=False)

    events_PULSE = events_PULSE[['pulse_score','hash_transcript']]
    events_PULSE = events_PULSE.groupby(['hash_transcript'], as_index=False).mean()
    assert events_PULSE.shape[0] == transcripts_PULSE.shape[0]
    transcripts_PULSE = pd.merge(transcripts_PULSE,events_PULSE, on='hash_transcript')
    transcripts_PULSE.to_csv(config['SUMMARY_OUTPUT']+'transcripts_scores.csv', index=False)


import xmltodict
import pandas as pd



def read_phatology(pathology_path):
    with open(pathology_path) as fd:
        doc = xmltodict.parse(fd.read())

    return doc


def get_survival(raw_doc, source_id, disease_code):

     code = disease_code.lower()

     if raw_doc['{}:tcga_bcr'.format(code)]['{}:patient'.format(code)]['clin_shared:vital_status']['#text'] != 'Alive':
         return [source_id,'Survival', int(raw_doc['{}:tcga_bcr'.format(code)]['{}:patient'.format(code)]['clin_shared:days_to_death']['#text']), None]
     else:
         # Zero means alive or no data  available
        return [source_id, 'Survival', 0, None]

def get_source_features(source_pathology_path, source, disease_code):

    # /home/ccorbi/Work/Beagle/PULSE/Pulse_RNAseq/data/Clinical/nationwidechildrens.org_clinical.TCGA-02-2485.xml'
    # Parse XML file
    raw_doc = read_phatology(source_pathology_path)
    # get features
    features = list()
    features.append(get_survival(raw_doc, source, disease_code))

    return pd.DataFrame(features,columns=['name','fname', 'fvalue', 'description'])

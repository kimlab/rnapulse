# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 17:38:37 2015

@author: ccorbi
"""


#from sqlalchemy import create_engine
import sqlalchemy as sa
import pymysql
import sys
import pandas as pd

#local_ddbb = 'mysql+pymysql://root@localhost'
#remote_ddbb = 'mysql+pymysql://root:kim630@192.168.6.19:3306/elaspic'
#engine =create_engine(local_ddbb)
#cnx = engine.raw_connection()

# lab database engine
engine = sa.create_engine('mysql+pymysql://root:kim630@192.168.6.19:3306')


def get_UniprotInfo_str(uniprotid,e = engine, field='gene_name'):

    '''Using uniparc schema map uniprot ids to Gene symbol or
    'protein_name','sequence','sequence_length'

        Parameters
    ----------
    uniprotid: `str`
        Uniprot id access code
    e:  `sqlalchemy.engine` (default: kimlab DDBB)
        engine to connect to the database
    field: str (default: 'gene_name')
        list of fields to retrive, valid options
        ['gene_name', 'protein_name','sequence','sequence_length']


    return
    ------
        str with the gene name '''

    try:
        symbol_names = []
        query1 = """SELECT uniparc_id FROM uniprot_kb.uniparc_xref where xref_id = '{}' and active = 'Y' """.format(uniprotid)
        uniparcs_info = pd.read_sql_query(query1,e)

        for i,j in uniparcs_info.iterrows():
            query2 = """SELECT * FROM uniprot_kb.uniparc where uniparc_id = '{}' """.format(j['uniparc_id'])
            uniparcref = pd.read_sql_query(query2,e)
            symbol_names.extend(uniparcref[field].unique())

        str_res =  ' '.join(symbol_names)
        return str_res

    except TypeError:
        return ''
    except KeyError:
        return 'There is no field {} in the ddbb'.format(field)

def get_UniprotInfo_df(uniprotid,e = engine):

    '''Using Uniprot local database: uniparc schema
    map uniprot ids to information fields, like gene_name, sequence, etc.
    in a pandas DataFame

    Parameters
    ----------
    uniprotid: `str`
        Uniprot id access code
    e:  `sqlalchemy.engine` (default: kimlab DDBB)
        engine to connect to the database

    return
    ------
        Pandas DataFrame with all the info '''

    try:
        # get best uniparc_id maps to uniprot
        query1 = """SELECT uniparc_id FROM uniprot_kb.uniparc_xref where xref_id = '{}' and active = 'Y' """.format(uniprotid)
        uniparcs_info = pd.read_sql_query(query1,e)

        #for i,j in uniparcs_info.iterrows():
        query2 = """SELECT * FROM uniprot_kb.uniparc where uniparc_id = '{}' """.format(uniparcs_info['uniparc_id'][0])
        uniparcref = pd.read_sql_query(query2,e)
        uniparcref['uniprot_id'] = uniprotid


    except TypeError:
        return ''



    return uniparcref
#%%
#Base = sa_ext_declarative.declarative_base()
#Base.metadata.naming_conventions = naming_convention
#
#
#class UniprotSequence(Base):
#    """ Table containing the entire Swissprot + Trembl database as well as any
#    additional sequences that were added to the database.
#    """
#    __tablename__ = 'uniprot_sequence'
#    __table_args__ = get_table_args(__tablename__, [], ['uniprot_kb_schema_tuple'])
#
#    db = sa.Column(sa.String(SHORT), nullable=False)
#    uniprot_id = sa.Column(sa.String(SHORT), primary_key=True)
#    uniprot_name = sa.Column(sa.String(SHORT), nullable=False)
#    protein_name = sa.Column(sa.String(MEDIUM))
#    organism_name = sa.Column(sa.String(MEDIUM), index=True)
#    gene_name = sa.Column(sa.String(MEDIUM), index=True)
#    protein_existence = sa.Column(sa.Integer)
#    sequence_version = sa.Column(sa.Integer)
#    uniprot_sequence = sa.Column(sa.Text, nullable=False)

# def search(uniprot_id,to):

#     conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='umapping')
#     c = conn.cursor()

#     c.execute('SELECT count(*) FROM uniprot_mapping WHERE uniprot_acc = %s and identifier_type = %s', (uniprot_id,to,))
#     table_data = c.fetchone()[0]
#     c.close()
#     return table_data




# def _create_ddbb():

#     # Create table for transmem_index
#     c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_mapping
#         (uniprot_acc VARCHAR(8),
#         identifier_type VARCHAR(25),
#         value VARCHAR(150)
#         )""")

#     c.execute("""CREATE INDEX uniprot_mapping_index on uniprot_mapping (uniprot_acc)""")


# def _update_uniprot_mapping_table(filename):

#     conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='umapping')
#     c = conn.cursor()

#     i = 0
#     with open(filename, "r") as input_file:

#         for line in input_file:
#             row = line.split('\t')
#             new_uniprot_id = row[0]
#             new_databse = row[1]
#             new_value = row[2].strip()

#             c.execute('SELECT count(*) FROM uniprot_mapping WHERE uniprot_acc = %s and identifier_type = %s and value = %s ', (new_uniprot_id,new_databse,new_value,))
#             table_data = c.fetchone()[0]
#             if table_data == 0:
#                 i += 1
#                 if i > 30000:
#                     conn.commit()
#                     i = 0
#                 else:
#                     pass
#                 c.execute("INSERT INTO uniprot_mapping VALUES (%s, %s, %s)", \
#                 (new_uniprot_id,new_databse,new_value))
#             else:
#                 continue

#     conn.commit()
#     conn.close()

# def load_uniprot_mapping_table(filename):

#     conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='umapping')
#     c = conn.cursor()


#     i = 0
#     with open(filename, "r") as input_file:

#         for line in input_file:
#             row = line.split('\t')
#             new_uniprot_id = row[0]
#             new_databse = row[1]
#             new_value = row[2].strip()

#             c.execute('SELECT count(*) FROM uniprot_mapping WHERE uniprot_acc = %s and identifier_type = %s and value = %s ', (new_uniprot_id,new_databse,new_value,))
#             table_data = c.fetchone()[0]
#             if table_data == 0:
#             #i += 1
#             #if i > 30000:
#             #    conn.commit()
#             #    i = 0
#             #else:
#             #    pass
#             	c.execute("INSERT INTO uniprot_mapping VALUES (%s, %s, %s)", \
#             	(new_uniprot_id,new_databse,new_value))
# 	    else:
# 		pass


#     conn.commit()
#     conn.close()


def main():
    #conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='umapping')

    get_UniprotInfo_str(sys.argv[1])

if __name__ == '__main__':
    main()

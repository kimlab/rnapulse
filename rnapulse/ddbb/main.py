import os
import hashlib
import logging
import time
import yaml
# import subprocess
import warnings

import pandas as pd
# import numpy as np
from sqlalchemy import create_engine
# from memory_profiler import profile
from tqdm import tqdm

from rnapulse.ddbb import phatology
from rnapluse.ddbb import local_mapping
from rnapulse.support import elms
from rnapulse.preprocess_transcriptome import transcriptome, read_gtf
from rnapulse.support.utils import timelog_ddbb_debug, is_non_zero_file, load_default_config, update_config

# Global
warnings.filterwarnings('ignore')
logger = logging.getLogger(__name__)


@timelog_ddbb_debug
def parse_expression(config):
    # read gtf file
    a = read_gtf(config['TRANSCRIPT_FILE'])
    # transform data to dataframe
    data = list()
    for k, v in a.transcripts.items():
        data.append([k, v.fpkm, v.cov, v.tpm])

    trans = pd.DataFrame(
        data, columns=['transcript_name', 'fpkm', 'cov', 'tpm'])

    return trans


@timelog_ddbb_debug
def insert_expression(source, config, engine):

    q = 'select * from rnaseq_event'
    transcripts = pd.read_sql_query(q, engine)
    transcripts['transcript_name'] = transcripts['id_pulse'].str.split(
        ':').str[1]
    # get transcript id for each transcript
    # map
    tmap = transcripts[['transcript_name', 'transcript_id']]
    #
    xdata = parse_expression(config)
    xdata_labeled = pd.merge(xdata, tmap, on='transcript_name')

    for element in ['fpkm', 'cov', 'tpm']:

        tempdf = xdata_labeled[['transcript_id', element]]
        tempdf.rename(columns={element: 'fvalue'}, inplace=True)
        tempdf['fidentifier'] = element
        tempdf['fname'] = element
        tempdf['ftype'] = 'expression'
        tempdf['fsample'] = source['SOURCE_ID']
        # mock id to avoid duplicates
        tempdf['temphash'] = tempdf['fsample'] + \
            tempdf['transcript_id'].astype(str) + element

        # I need to improve this hack
        q = 'select * from rnaseq_transcripts_features'
        already = pd.read_sql_query(q, engine)
        # remove anchors already annotated
        # add fake id
        already['temphash'] = already['fsample'] + \
            already['transcript_id'].astype(str) + already['fname']

        data_to_insert = tempdf[~tempdf['temphash'].isin(already['temphash'])]
        # insert new fields
        del data_to_insert['temphash']
        data_to_insert.to_sql('rnaseq_transcripts_features',
                              engine, if_exists='append', index=False, chunksize=100)

    return data_to_insert


def is_non_zero_file(fpath):
    if os.path.isfile(fpath) and os.path.getsize(fpath) > 0:
        return True
    else:
        logger.info("FILE EMPTY or DO NOT EXISTS %s", fpath)
        return False


def hashseqs(row):
    r = row.encode('utf-8')
    return hashlib.md5(r).hexdigest()


@timelog_ddbb_debug
def find_elms(seq_df, seq, checksum):

    elm_info = pd.DataFrame()
    for i, row in tqdm(seq_df.iterrows(), desc="ELMS", leave=False):

        elm = elms.motif_search(row[seq])
        elm[checksum] = row[checksum]
        elm_info = pd.concat([elm_info, elm])

    elm_info['ftype'] = 'elm'
    return elm_info


def readfasta2dict(fastafile):
    """read fasta file and return dict fastaid:seq.


    Only works with fasta files generated in the PULSE pipeline (sequence is
        in one line)

    """
    fastadict = dict()
    with open(fastafile, 'r') as input_file:

        for line in input_file:
            if line.startswith('>'):
                _id = line[1:].strip()
            else:
                # check if already exist [just in case]
                if _id in fastadict:
                    raise('Error: Duplicated items in {}'.format(fastafile))
                else:
                    fastadict[_id] = line.strip()

    return fastadict


@timelog_ddbb_debug
def wrap_pulse_events(transcripts_PULSE, source, config):
    """wrap Pulse output in a single Pandas.DataFrame.

    Parameters
    ----------
        transcripts_PULSE: pandas.DataFrame

        cell_source: str
            Name of the folder of the cell line
        pulseCfg_file: str
            Path to the PULSE json configuration file

    Returns
    -------
        transcripts: pandas.DataFrame
        id_pulse, uniprot_anchor, s1, e1, s2, e2, temp, len_anchor, len_transcript,  pulse_score, seq, checksum



    """

    # define folders and headers
    # Load SCORES
    # MERGE OUTPUT FILES
    # Names

    # load transcript names
    if is_non_zero_file(config['NAMES_ML']):
        events = pd.read_csv(config['NAMES_ML'], names=[
            'id_pulse', 'uniprot_id'], sep='//')
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('names files do not exist %s', source)
        return pd.DataFrame()

    # scores
    # load transcript scores
    if is_non_zero_file(config['PULSE_OUTPUT']):
        score = pd.read_csv(config['PULSE_OUTPUT'], names=['pulse_score'])
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('Scores files do not exist %s', source)
        return pd.DataFrame()

    # Anchors maps
    # load transcript maps

    if is_non_zero_file(config['ANCHOR_EVENT_INIDEX_MAP']):
        anchormap = pd.read_csv(config['ANCHOR_EVENT_INIDEX_MAP'],
                                names=['id_pulse', 'uniprot_id', 'map_e1', 'map_s2',
                                       'map_e2', 'map_s3', 'temp', 'len_anchor', 'len_transcript'],
                                sep='\t')
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('Events index do not exist %s', source)
        return pd.DataFrame()

    # cdna event
    if is_non_zero_file(config['EVENTS_FASTA']):
        cdna_events = readfasta2dict(config['EVENTS_FASTA'])
        # Transform
        cdna_events_df = pd.DataFrame.from_dict(cdna_events, orient='index')
        cdna_events_df.reset_index(inplace=True)
        cdna_events_df.rename(
            columns={0: 'cdna_event', 'index': 'id_pulse'}, inplace=True)
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('Fasta cdna events file do not exist %s', source)
        return pd.DataFrame()

    # MERGE and transform
    # add score
    events = pd.concat([events, score], axis=1)
    # add uniprot id and mapp information
    events = pd.merge(anchormap, events, on=['id_pulse', 'uniprot_id'])
    # add cdna of the event
    events = pd.merge(cdna_events_df, events, on='id_pulse')
    # event  hash
    events['event_hash'] = events['id_pulse'] + events['cdna_event']
    events['event_hash'] = events['event_hash'].apply(hashseqs)

    # add isofomr hash
    events = pd.merge(events, transcripts_PULSE[[
                      'id_pulse', 'hash_transcript']], how='left', on='id_pulse')

    return events


@timelog_ddbb_debug
def wrap_pulse_transcripts(source, config):
    """wrap Pulse output in a single Pandas.DataFrame.

    Parameters
    ----------
        cell_source: str
            Name of the folder of the cell line
        pulseCfg_file: str
            Path to the PULSE json configuration file

    Returns
    -------
        transcripts: pandas.DataFrame
        id_pulse, uniprot_id, s1, e1, s2, e2, temp, len_anchor, len_transcript, aa_transcript, hash_transcript



    """
    # LOAD PULSE DATA
    # load Aminoacid transcripts seq and transrform to dataFrame to merge
    if is_non_zero_file(config['PSEQ']):
        protSeqIso = readfasta2dict(config['PSEQ'])
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('Fasta transcripts file do not exist %s', source)
        return pd.DataFrame()

    # Transform
    protSeqIso_df = pd.DataFrame.from_dict(protSeqIso, orient='index')
    protSeqIso_df.reset_index(inplace=True)
    protSeqIso_df.rename(
        columns={0: 'aa_transcript', 'index': 'id_pulse'}, inplace=True)

    # load cdna transcripts
    if is_non_zero_file(config['CDNA']):
        cdnaSeqIso = readfasta2dict(config['CDNA'])
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('Fasta cdna transcripts file do not exist %s', source)
        return pd.DataFrame()

    # Transform
    cdnaSeqIso_df = pd.DataFrame.from_dict(cdnaSeqIso, orient='index')
    cdnaSeqIso_df.reset_index(inplace=True)
    cdnaSeqIso_df.rename(
        columns={0: 'cdna_transcript', 'index': 'id_pulse'}, inplace=True)

    # merge information
    transcripts = pd.merge(protSeqIso_df, cdnaSeqIso_df, on='id_pulse')

    # generate MD5 hash seq for each transcript SEQ
    transcripts['hash_transcript'] = transcripts['aa_transcript'].apply(
        hashseqs)

    # may be need to clean the cell line name [folders looks like:

    # Add uniprot_id

    # Anchors maps
    # load transcript maps

    if is_non_zero_file(config['ANCHOR_EVENT_INIDEX_MAP']):
        anchormap = pd.read_csv(config['ANCHOR_EVENT_INIDEX_MAP'],
                                names=['id_pulse', 'uniprot_id', 'map_e1', 'map_s2',
                                       'map_e2', 'map_s3', 'temp', 'len_anchor', 'len_transcript'],
                                sep='\t')

        logger.debug(anchormap.head())
        transcripts = pd.merge(transcripts, anchormap,
                               how='left', on='id_pulse')

        # drop transcripts without mapping
        transcripts.dropna(subset=['uniprot_id'], inplace=True, axis=0)
        logger.debug(transcripts.head())

    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('Events index do not exist %s', source)
        return pd.DataFrame()

    # transcripts['exist_uniprot'] = np.nan
    # transcripts['disable_transcript'] = ''
    # iso
    transcripts.drop_duplicates(subset=['hash_transcript'], inplace=True)
    transcripts = transcripts[['id_pulse', 'uniprot_id', 'len_transcript', 'aa_transcript',
                               'cdna_transcript', 'hash_transcript']]

    return transcripts


@timelog_ddbb_debug
def get_uniprot_info(anchors):
    """Fetch General information from the anchors, from kimlab database.
    ToDo: if it fails, check uniprot online, check if is a uniprot valid name

    Paramentes
    ----------
        anchors: list
            list with all the anchors in uniprot format

    Return
    ------
        anchor_info: pandas.DataFrame
            Dataframe columns = [uniparc_id, uniprot_id, gene_name,
                                protein_name, NCBI_GI, NCBI_taxonomy_id,
                                chain, component, proteome_id, sequence,
                                sequence_length, sequence_checksum]


    """
    adf = pd.DataFrame()

    for anchor in anchors:
        adf = pd.concat([adf, local_mapping.get_UniprotInfo_df(anchor)])

    adf.rename(columns={'protein_name': 'description', 'sequence': 'aa_anchor',
                        'sequence_checksum': 'hash_anchor', 'sequence_length': 'len_anchor'}, inplace=True)
    # just keep what I need
    adf = adf[['uniprot_id', 'description',
               'aa_anchor', 'hash_anchor', 'len_anchor']]

    logger.debug('>> get_uniprot_info')
    logger.debug(adf.head())

    return adf


@timelog_ddbb_debug
def wrap_uniprot_pfam(anchors_to_insert, config):
    """Get from preCalc uniprot ddbb pfam information.

    Parameters
    ----------
    anchors_DF : pd.DataFrame
       Dataframe with uniprot id of the anchors

    config : dict
       configuration values

    Returns
    -------
    pd.Dataframe


    """
    # Open database
    engine = create_engine('sqlite:////' + config['UNIPROT_FEATURES_DDBB'])
    # Get all information
    query = 'select * from uniprot_pfams'
    iteranchors_pfam = pd.read_sql(query, engine, chunksize=200000)
    target_uniprots = anchors_to_insert['uniprot_id'].unique()
    interested = list()

    for i in iteranchors_pfam:

        temp = i[i['id'].isin(target_uniprots)]
        if temp.shape[0]:
            interested.append(temp)

    if len(interested):
        anchors_pfam = pd.concat(interested, ignore_index=True)
        anchors_pfam.rename(columns={'id': 'uniprot_id'}, inplace=True)
        # Filter
        anchors_pfam = pd.merge(
            anchors_to_insert, anchors_pfam, on='uniprot_id')

        # Select info and rename
        # anchors_pfam = anchors_pfam[['uniprot_id','START', 'END', 'HMM ACC' ]]
        # load Pfam definitions
        pfam_def = pd.read_csv(config['PFAM_DEFINITION'], sep='\t', names=['pfamA_acc',
                                                                           'pfamA_id', 'previous_id', 'description',  'author', 'deposited_by', 'seed_source', 'type', 'comment',
                                                                           'sequence_GA', 'domain_GA', 'sequence_TC', 'domain_TC', 'sequence_NC', 'domain_NC', 'buildMethod',
                                                                           'model_length', 'searchMethod', 'msv_lambda', 'msv_mu', 'viterbi_lambda', 'viterbi_mu', 'forward_lambda',
                                                                           'forward_tau', 'num_seed', 'num_full', 'updated', 'created', 'version', 'number_archs', 'number_species',
                                                                           'number_structures', 'number_ncbi', 'number_meta', 'average_length', 'percentage_id', 'average_coverage',
                                                                           'change_status', 'seed_consensus', 'full_consensus', 'number_shuffled_hits', '1', '2', '3', '4', '5', '6', '7'])
        # Add names and definition to transcript
        anchors_pfam = pd.merge(
            anchors_pfam, pfam_def, left_on='pfam_id', right_on='pfamA_acc', how='left')

        anchors_pfam['ftype'] = 'pfam'
    else:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning('None pfam domain definitions found it for ')
        logger.warning(anchors_to_insert)
        anchors_pfam = pd.DataFrame()

    return anchors_pfam


@timelog_ddbb_debug
def wrap_pulse_pfam(data_to_map, config):

    # DOMAINS, Pfam information
    # Recollect from precalculated file

    pfam_path = config['PFAM_REFERENCE']

    transcripts_pulse_pfam = pd.read_csv(pfam_path, skiprows=28, names=['id_pulse', 'start', 'end',
                                                                        'envelope start', 'envelope end',
                                                                        'HMM ACC', 'HMM Name', 'type',
                                                                        'hmm start', 'hmm end', 'hmm length',
                                                                        'bit score', 'E-value', 'significance',
                                                                        'clan'],
                                         delim_whitespace=True)

    pfam_def = pd.read_csv(config['PFAM_DEFINITION'], sep='\t', names=['pfamA_acc',
                                                                       'pfamA_id', 'previous_id', 'description',  'author', 'deposited_by', 'seed_source', 'type', 'comment',
                                                                       'sequence_GA', 'domain_GA', 'sequence_TC', 'domain_TC', 'sequence_NC', 'domain_NC', 'buildMethod',
                                                                       'model_length', 'searchMethod', 'msv_lambda', 'msv_mu', 'viterbi_lambda', 'viterbi_mu', 'forward_lambda',
                                                                       'forward_tau', 'num_seed', 'num_full', 'updated', 'created', 'version', 'number_archs', 'number_species',
                                                                       'number_structures', 'number_ncbi', 'number_meta', 'average_length', 'percentage_id', 'average_coverage',
                                                                       'change_status', 'seed_consensus', 'full_consensus', 'number_shuffled_hits', '1', '2', '3', '4', '5', '6', '7'])
    # Add names and definition to transcript
    transcripts_pulse_pfam['pfam_id'] = transcripts_pulse_pfam['HMM ACC'].str.split(
        '.').str[0]
    anchors_pfam = pd.merge(
        data_to_map, transcripts_pulse_pfam, on='id_pulse')

    anchors_pfam = pd.merge(
        anchors_pfam, pfam_def, left_on='pfam_id', right_on='pfamA_acc', how='left')

    anchors_pfam['ftype'] = 'pfam'

    return anchors_pfam


@timelog_ddbb_debug
def insert_sequences(df_total_data, seq_type, engine, config):
    """Insert new anchors, if any and their features in the ddbb.

    Parameters
    ----------

    Returns
    -------

    """
    type_flags = {'anchors': {'main_table': 'rnaseq_protein',
                              'hash': 'hash_anchor',
                              'seq_id': 'aa_anchor'},
                  'transcripts': {'main_table': 'rnaseq_transcript',
                                  'hash': 'hash_transcript',
                                  'seq_id': 'aa_transcript'}}
    # remove if exist
    # add
    q = 'select * from {}'.format(type_flags[seq_type]['main_table'])
    already = pd.read_sql_query(q, engine)
    # remove anchors already annotated

    data_to_insert = df_total_data[~df_total_data[type_flags[seq_type]['hash']].isin(
        already[type_flags[seq_type]['hash']])]

    # add any thing new
    if data_to_insert.shape[0]:
        # insert new anchors in the ddbb
        logger.debug("insert %i ", data_to_insert.shape[0])

        # Get features  add features
        if seq_type == 'anchors':
            data_to_insert.to_sql(
                type_flags[seq_type]['main_table'], engine, if_exists='append', index=False, chunksize=100)
            # to_mysql(data_to_insert, type_flags[seq_type]['main_table'])
            # if there is no new anchor to add, skip this step
            if data_to_insert.shape[0]:
                data_pfam = wrap_uniprot_pfam(data_to_insert, config)
            else:
                logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
                logger.warning('No data anchors to add')
                data_pfam = pd.DataFrame()
        else:
            # file exists

            data_to_insert = add_pk_value(
                data_to_insert, engine, 'uniprot_id', 'rnaseq_protein', 'protein_id')
            data_to_map = data_to_insert.copy()
            del data_to_insert['uniprot_id']
            del data_to_insert['id_pulse']

            data_to_insert.to_sql(
                type_flags[seq_type]['main_table'], engine, if_exists='append', index=False, chunksize=100)
            # to_mysql(data_to_insert, type_flags[seq_type]['main_table'])

            if is_non_zero_file(config['PFAM_REFERENCE']):
                data_to_map = data_to_map[['id_pulse', 'hash_transcript']]
                data_to_map.drop_duplicates(
                    subset=['hash_transcript'], inplace=True)
                data_pfam = wrap_pulse_pfam(data_to_map, config)
                logger.debug('transcripts_pulse_pfam')
                logger.debug(data_pfam.head(2))
            else:
                logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
                logger.warning("PFAM INFO DO NOT EXIST  %s",
                               config['PFAM_REFERENCE'])
                logger.warning("SKIPPING")
                data_pfam = pd.DataFrame()
        # Transformations. After check there is something to add.

        # Add names and definition to transcript
        insert_features(data_pfam, engine, selkey=seq_type, feature='pfam')

        data_ELM = find_elms(
            data_to_insert, seq=type_flags[seq_type]['seq_id'], checksum=type_flags[seq_type]['hash'])

        insert_features(data_ELM, engine, selkey=seq_type, feature='elm')

    return data_to_insert


@timelog_ddbb_debug
def insert_events(df_total_data, engine):

    q = 'select * from  rnaseq_event'
    already = pd.read_sql_query(q, engine)

    data_to_insert = df_total_data[~df_total_data['event_hash'].isin(
        already['event_hash'])]

    if data_to_insert.shape[0]:
        # get what i need
        # get primary key anchors
        data_to_insert = add_pk_value(
            data_to_insert, engine, 'uniprot_id', 'rnaseq_protein', 'protein_id')

        # get primary key transcript
        data_to_insert = add_pk_value(
            data_to_insert, engine, 'hash_transcript', 'rnaseq_transcript', 'transcript_id')
        # extract event type from pulse id
        data_to_insert['type_event'] = data_to_insert['id_pulse'].str.split(
            ':').str[0]
        # exract Genomic coordinates
        data_to_insert['chromosome'] = data_to_insert['id_pulse'].str.split(
            ':').str[2].str.split('_').str[0]
        data_to_insert['genomic_exon_start'] = data_to_insert['id_pulse'].str.split(
            ':').str[2].str.split('_').str[1]
        data_to_insert['genomic_exon_ends'] = data_to_insert['id_pulse'].str.split(
            ':').str[2].str.split('_').str[2]
        # transform
        data_to_insert = data_to_insert[['id_pulse',
                                         'protein_id',
                                         'transcript_id',
                                         'cdna_event',
                                         'event_hash',
                                         'map_e1',
                                         'map_s2',
                                         'map_e2',
                                         'map_s3',
                                         'pulse_score',
                                         'type_event',
                                         'chromosome',
                                         'genomic_exon_start',
                                         'genomic_exon_ends']]
        # insert
        data_to_insert.to_sql('rnaseq_event', engine,
                              if_exists='append', index=False, chunksize=100)

    return data_to_insert


@timelog_ddbb_debug
def insert_sample(source, config, engine):

    # ignore if is already in
    q = 'select * from  rnaseq_sample'
    already = pd.read_sql_query(q, engine)
    # get what i nee
    inside_ddbb = already[already['name'].isin([source['SOURCE_ID']])]

    if not inside_ddbb.shape[0]:

        new_source = source.rename({'SOURCE_ID': 'name',
                                    'SAMPLE_TYPE': 'source',
                                    'DISEASE': 'disease',
                                    'SYSTEM': 'system',
                                    'DESCRIPTION': 'description'})
        # I could get ride off of the series index, event after convert to dataframe
        # this si wired but it works
        # get and array with the info
        array_to_insert = new_source[['name',
                                      'source',
                                      'disease',
                                      'system',
                                      'description']].get_values()

        data_to_insert = pd.DataFrame([array_to_insert], columns=['name',
                                                                  'source',
                                                                  'disease',
                                                                  'system',
                                                                  'description'])
        logger.debug('### insert_sample  ###')
        logger.debug(data_to_insert)
        data_to_insert.to_sql('rnaseq_sample', engine,
                              if_exists='append', index=False, chunksize=100)

        # add samples features
        # Not all the samples have this kind of information.
        try:
            pathology_data = phatology.get_source_features(
                config['CLINICAL'], new_source['name'], new_source['disease'])

            # add pk
            pathology_data = add_pk_value(
                pathology_data, engine, 'name', 'rnaseq_sample', 'sample_id')
            # insert
            data_to_insert = pathology_data[[
                'fname', 'fvalue', 'sample_id', 'description']]
            data_to_insert.to_sql('rnaseq_samples_features',
                                  engine, if_exists='append', index=False)

        except IOError as e:
            logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
            logger.warning("Clinal features not found for %s",
                           new_source['name'])

        except KeyError:
            logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
            logger.warning("Error Parsing Clinical data for %s",
                           new_source['name'])

    return inside_ddbb


@timelog_ddbb_debug
def insert_mapping(transcripts_PULSE, config, engine):

    # add primary Keys
    transcripts_PULSE = add_pk_value(
        transcripts_PULSE, engine, 'name', 'rnaseq_sample', 'sample_id')

    transcripts_PULSE = add_pk_value(
        transcripts_PULSE, engine, 'hash_transcript', 'rnaseq_transcript', 'transcript_id')

    logger.debug('********* MODIF *******')
    logger.debug(transcripts_PULSE.head())
    data_to_insert = transcripts_PULSE[['sample_id', 'transcript_id']]
    data_to_insert['id_map_ts'] = data_to_insert['sample_id'].astype(
        str) + '-' + data_to_insert['transcript_id'].astype(str)

    # remove duplicates
    q = 'select * from  rnaseq_mapping_transcripts_samples'
    already = pd.read_sql_query(q, engine)
    data_to_insert = data_to_insert[~data_to_insert['id_map_ts'].isin(
        already['id_map_ts'])]
    # insert in the database
    data_to_insert.to_sql('rnaseq_mapping_transcripts_samples',
                          engine, if_exists='append', index=False, chunksize=100)

    return data_to_insert


def update_mapping(engine):
    # GEt info and preprocess
    q = 'select  rnaseq_mapping_transcripts_samples.sample_id,rnaseq_event.id  from  rnaseq_event, rnaseq_transcript, rnaseq_mapping_transcripts_samples where rnaseq_mapping_transcripts_samples.transcript_id = rnaseq_transcript.id and rnaseq_transcript.id = rnaseq_event.transcript_id '
    events = pd.read_sql_query(q, engine)
    events.drop_duplicates(subset=['sample_id', 'id'], inplace=True)
    events.rename(columns={'id': 'event_id'}, inplace=True)
    events['id_map_es'] = events['sample_id'].astype(
        str) + '-' + events['event_id'].astype(str)
    events.drop_duplicates(subset=['id_map_es'], inplace=True)
    logger.debug('********* UPDATING EVENTS *******')
    logger.debug(events.head())

    q = 'select * from  rnaseq_mapping_events_samples'
    already = pd.read_sql_query(q, engine)
    # get only new data
    data_to_insert = events[~events['id_map_es'].isin(already['id_map_es'])]
    logger.debug(data_to_insert.head())
    # insert data
    data_to_insert.to_sql('rnaseq_mapping_events_samples',
                          engine, if_exists='append', index=False, chunksize=100)

    q = 'select rnaseq_transcript.protein_id, rnaseq_mapping_transcripts_samples.sample_id  from  rnaseq_transcript, rnaseq_mapping_transcripts_samples where rnaseq_mapping_transcripts_samples.transcript_id = rnaseq_transcript.id '
    proteins = pd.read_sql_query(q, engine)
    proteins['id_map_ps'] = proteins['sample_id'].astype(
        str) + '-' + proteins['protein_id'].astype(str)
    proteins.drop_duplicates(subset=['id_map_ps'], inplace=True)
    logger.debug('********* UPDATING PROTEINS *******')
    logger.debug(proteins.head())
    # remove duplicates
    q = 'select * from  rnaseq_mapping_proteins_samples'
    already = pd.read_sql_query(q, engine)
    data_to_insert = proteins[~proteins['id_map_ps'].isin(
        already['id_map_ps'])]
    logger.debug(data_to_insert.head())
    # insert in the database
    data_to_insert.to_sql('rnaseq_mapping_proteins_samples',
                          engine, if_exists='append', index=False, chunksize=100)

    return data_to_insert


@timelog_ddbb_debug
def insert_features(dfpfam, engine, selkey='transcripts', feature='pfam'):

    selector = {'transcripts': {'table': 'rnaseq_transcripts_features',
                                'pk_table': 'rnaseq_transcript',
                                'pk_name': 'transcript_id',
                                'join_field': 'hash_transcript'},
                'anchors': {'table': 'rnaseq_proteins_features',
                            'pk_table': 'rnaseq_protein',
                            'pk_name': 'protein_id',
                            'join_field': 'hash_anchor'}}

    mapping_columns = {'pfam': {'pfam_id': 'fidentifier',
                                'pfamA_id': 'fname'},
                       'elm': {'Accession': 'fidentifier',
                               'FunctionalSiteName': 'fname'}}

    # add primary key
    dfpfam = add_pk_value(dfpfam, engine, **selector[selkey])
    # rename and select data
    dfpfam.rename(columns=mapping_columns[feature], inplace=True)

    data_to_insert = dfpfam[['ftype', 'fidentifier',
                             'start', 'end', selector[selkey]['pk_name'], 'fname']]
    # insert to ddbb
    data_to_insert.to_csv('{}{}.csv'.format(selkey, feature))
    data_to_insert.to_sql(
        selector[selkey]['table'], engine, if_exists='append', index=False, chunksize=2000)

    return data_to_insert


@timelog_ddbb_debug
def add_pk_value(df_to_label, engine, join_field, pk_table, pk_name, **args):
    """Retun Primary Key ID for a row.

    Parameters
    ----------
    row : pandas.DataFrame, pandas.Series

    pk : pandas.DataFrame, pandas.Series

    Returns
    -------


    """
    q = 'select * from {}'.format(pk_table)
    primary_table = pd.read_sql_query(q, engine)
    # Get primary keys value

    labeled = pd.merge(df_to_label, primary_table[[
                       'id', join_field]], on=join_field, how='left')
    # Check why <<<<<<<<<<<<
    labeled.dropna(subset=['id'], inplace=True)
    labeled.id = labeled.id.astype(int)
    labeled.rename(columns={'id': pk_name}, inplace=True)

    return labeled


def ddbb_loader(samples, eng):

    #transcripts_name = sys.argv[1]
    #transcripts_score =  sys.argv[2]

    #########
    # START #
    #########
    # init
    # Gather General information
    # read pulse cfg, paths    # database

    PULSE_PATH = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    # eng = create_engine('mysql://pulse:XXXXXX@192.168.6.19:3306/pulsernaseq')

    # parsing
    for idx, source in tqdm(samples, desc='SAMPLES'):

        # Gather information for each source (sample)
        logger.info("*** %s %s ***", idx, source['SOURCE_ID'])
        # load paths

        # if already exist, skip
        q = 'select * from  rnaseq_sample'
        already = pd.read_sql_query(q, eng)
        inside_ddbb = already[already['name'].isin([source['SOURCE_ID']])]

        config = load_default_config(
            PULSE_PATH + '/config.yml', sample_id=source['SOURCE_ID'], work_folder=os.getcwd())
        # argument
        if not inside_ddbb.shape[0]:
            # if not False:

            # ------------- Get & Parse Pulse Output
            anchors, transcripts_PULSE, events_PULSE = gather_pulse_output(
                config, source)

            # ------------- Save Pulse Info on DDBB
            add_info_ddbb(anchors, transcripts_PULSE,
                          events_PULSE, source, config)

        else:
            logger.info("*** SKIP %s %s ***", idx, source['SOURCE_ID'])


def gather_pulse_output(config, source):

    # -------------
    # I S O F O R M
    # Get Transcripts sequences and events related
    transcripts_PULSE = wrap_pulse_transcripts(
        source['SOURCE_ID'] + '.bam', config)
    if not transcripts_PULSE.shape[0]:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning("SKIPPING %s", source['SOURCE_ID'])

    # Get events and scores
    events_PULSE = wrap_pulse_events(
        transcripts_PULSE, source['SOURCE_ID'] + '.bam', config)
    if not events_PULSE.shape[0]:
        logger.warning('*#**#**#**#**#**#**#**#**#**#**#**#**#**#**#*')
        logger.warning("SKIPPING %s", source['SOURCE_ID'])

    # -------------
    # A N C H O R S
    # Load uniprot id
    logger.info("*** LOAD ANCHORS ***")
    anchors_IDs = transcripts_PULSE['uniprot_id'].unique().tolist()
    logger.info("# %i Anchors ", len(anchors_IDs))

    # Get Seq, Gene name, etc...
    anchors = get_uniprot_info(anchors_IDs)
    logger.info("# %s Anchors Definitions", anchors.shape[0])
    logger.debug(anchors.head(2))

    return anchors, transcripts_PULSE, events_PULSE


def add_info_ddbb(anchors, transcripts_PULSE, events_PULSE, source, config):

        # connect to Target ddbb
        # conn = sqlite3.connect(config['TARGET_DDBB'])
    engine = create_engine(
        'mysql://pulse:kim630@192.168.6.19:3306/pulsernaseq')
    # INSERT ANCHORS or CANONICALS
    insert_sequences(anchors, 'anchors', engine, config)
    # INSERT transcriptS
    # Save DDBB
    # del transcripts_PULSE['id_pulse']
    insert_sequences(transcripts_PULSE, 'transcripts', engine, config)
    # now, it is events time
    insert_events(events_PULSE, engine)
    # Finally
    # Mapping, index & sample
    insert_sample(source, config, engine)
    # MAPPING CELL-transcriptS
    # Save DDBB
    transcripts_PULSE['name'] = source['SOURCE_ID']
    insert_mapping(transcripts_PULSE, config, engine)
    insert_expression(source, config, engine)
    # Satellit MAPPING
    update_mapping(engine)


# if __name__ == "__main__":
#     """Main.
#
#     Parameters
#     ----------
#
#
#     Returns
#     -------
#
#     """
#
#     # init Logging
#     time_stamp = time.ctime()
#     logging.basicConfig(
#         level=logging.DEBUG,
#         format='%(asctime)s - %(name)s - %(levelname)s :::     %(message)s',
#         datefmt='%m-%d %H:%M',
#         filename='./logs/FEEDER_{4}_{1}_{2}_{0}_{3}.log'.format(
#             *time_stamp.split()),
#         filemode='w')
#
#     logger = logging.getLogger(__name__)
#
#     logger.info('JOB START {4} {1} {2} {0} {3}'.format(*time_stamp.split()))
#
#     run_writer()
#
#     logger.info('JOB ENDS {4} {1} {2} {0} {3}'.format(*time_stamp.split()))

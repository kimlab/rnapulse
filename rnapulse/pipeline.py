from __future__ import print_function
from __future__ import division

# STD lib
import logging

# PULSE modules
# utils & accesories
from rnapulse.support.utils import timelog, check_ouputs, check_binaries,  create_folders, run_TCGA_downloader, is_non_zero_file, check_suppfiles
# from ddbb.main import run_writer
# Transcriptome Reassembly
from rnapulse.assemble_transcriptome.tuxedo import run_stringtie, run_stringtie_merge, run_stringtie_estimate, run_GFFcompare
# Filtering transcriptome
from rnapulse.preprocess_transcriptome.reference_genome import load_pickled_reference_genome
from rnapulse.preprocess_transcriptome.transcriptome import read_gtf
from rnapulse.preprocess_transcriptome.main import group_transcripts, fetch_splicing_events
# Anchoring
from rnapulse.anchoring.main import anchor_searching, index_generation
# Features
from rnapulse.features.main import feature_extraction, generate_features_matrix, naive_filter
# Score
from rnapulse.scoring.main import run_PULSE
from rnapulse.scoring.parsers import summary_pulse_output

logger = logging.getLogger(__name__)

####################
# P I P E L I N E  #
####################


class Director(object):
    """Main class to Flow control"""

    def __init__(self, options, config):

        # dictionary that will be used to determine which static method is
        # to be executed but that will be also used to store possible param
        # value
        self._method_choices = {'assembling': self._assembly,
                                'preprocessing': self._preprocess,
                                'anchoring': self._anchoring,
                                'mapping': self._mapping,
                                'featuring': self._features,
                                'scoring': self._scores,
                                'download': self._download}

        self.config = config
        self.rst = options.rst
        self.only = options.only
        self.options = options

        return

    def __call__(self):

        # check supplementary files
        check_suppfiles(self.config)

        # METHODS
        # check arguments and call setup
        if self.options.action == 'setup':
                        #'ggenome']
            return

        # check arguments and call ddbb dump method
        if self.options.action == 'ddbb_dump':
            self._ddbb_write()
            return

        # check arguments and  call download method
        if self.options.action == 'download':
            # check if files exists
            self._download(manifestfile=self.options.manifestfile)

            return

        # MAIN Pipeline
        # check arguments and run pipeline
        execution = ['assembling',
                     'preprocessing',
                     'anchoring',
                     'mapping',
                     'featuring',
                     'scoring']

        if self.options.action == 'run':
            # evalutate the arguments
            if self.only:
                if not set(self.only).issubset(execution):
                    raise ValueError(
                        'Invalid step defined to run {}'.format(self.only))
            if self.rst:
                if not set(self.rst).issubset(execution):
                    raise ValueError(
                        'Invalid step defined to skip {}'.format(self.rst))

            # run
            rst_flag = False
            for step in execution:
                if self.only and step in self.only:
                    self._method_choices[step]()
                    break

                elif self.rst:
                    if not step in self.rst:
                        if rst_flag:
                            self._method_choices[step]()
                        else:
                            logger.info("skipping %s", step)
                            pass
                    else:
                        rst_flag = True
                        self._method_choices[step]()

                else:
                    self._method_choices[step]()

            return


##########################
# SAMTOOLS AND CUFFLINKS # t
##########################

    @timelog
    def _assembly(self):
        """ From RNAseq  aligned file (bam) generate a transcriptome.

            Using String tie in semi-guided mode


        """
        print('Assembly....', end="", flush=True)
        # check binaries
        must_binaries = [self.config['STRINGTIE_BIN'],
                         self.config['GFFCOMAPRE_BIN']]
        check_binaries(must_binaries)

        # Create outpout folders
        create_folders(self.config, self.config['ASSEMBLY_FOLDER'])
        logger.info("Assembly paths created for: %s", self.config['SAMPLE_ID'])

        # assembly
        run_stringtie(self.config)
        # merge with reference
        run_stringtie_merge(self.config)
        # Control
        run_GFFcompare(self.config)
        # Estimate abundace
        run_stringtie_estimate(self.config)

        print('Ok')

        return

    #################
    # PREPROCESSING # p
    #################
    @timelog
    def _preprocess(self):
        """ Call main functions of the transcript preprocessing step.

        Extract Splicing events and add sequence to genomic coordinates


        """
        print('Preprocessing....', end="", flush=True)
        # Check output previous step
        check_ouputs(self.config, 'Assembly')

        # Create folder
        create_folders(self.config, self.config['PREPROCESS_OUTPUT'])

        logger.info("LOADING PICKLED REFERENCE GENOME  %s ",
                    self.config['GENOME_PICKLE'])
        # Load Seq Reference Genome
        ref_genome = load_pickled_reference_genome(
            self.config['GENOME_PICKLE'])

        logger.info("PICKLED REFERENCE GENOME LOADED")
        logger.info("LOADING CHROMOSOMES: ")
        # Load chromoseomes
        for x in sorted(ref_genome.keys()):
            logger.debug("Available \t\t%s\t##", x)

        logger.info("READING TRANSCRIPTOME FROM GTF FILE")

        sample_transcriptome = read_gtf(self.config['TRANSCRIPT_ASSEMBLY'])

        logger.info("PARSING TRANSCRIPTOME")
        # Group by TSS and Gene and add sequence to genomic coordinates
        # TODO: transform this to a method of the Transcriptome class
        grouped_transcripts = group_transcripts(
            sample_transcriptome, ref_genome)
        # delete the genome variable to free memory
        del ref_genome
        logger.info("FINDING ALTERNATIVE SPLICING EVENTS")
        # find and extract  Splicing events
        # Extract AS events [skipping Exon]
        fetch_splicing_events(self.config, grouped_transcripts)
        print('Ok')

        return

    ####################
    # ANCHOR (blast)   # b
    ####################

    @timelog
    def _anchoring(self):
        """ Call anchoring rutines.

        Try to find a anchor for each splicing event.


        """
        print('Anchoring....', end="", flush=True)

        # check binaries
        must_binaries = [self.config['BLAST_BIN']]
        check_binaries(must_binaries)
        # check files
        check_ouputs(self.config, 'Preprocess')
        # Blast Events to get Anchors
        anchor_searching(self.config)
        print('Ok')

        return

    ####################
    #  Mapping         # m
    ####################

    @timelog
    def _mapping(self):
        """ Generation of a index o map between splicing events and anchors.


        """
        print('Mapping....', end="", flush=True)

        check_ouputs(self.config, 'Anchoring')
        # Map Anchor-isoform Generation
        index_generation(self.config)
        print('Ok')

        return

    ######################
    # FEATURE EXTRACTION # f
    ######################
    @timelog
    def _features(self):
        """ Generation of features for each splicing event using anchor comparative.


        """
        print('Featuring....', end="", flush=True)

        # check binaries
        must_binaries = [self.config['IUPRED_BIN']]
        check_binaries(must_binaries)

        check_ouputs(self.config, 'Mapping')

        logger.info("Housekeeping %s", self.config['SAMPLE_ID'])
        naive_filter(self.config)

        # create folders
        create_folders(self.config, self.config['FEATURES_OUTPUT'])
        logger.info("MSG: Features paths created for: %s",
                    self.config['SAMPLE_ID'])

        feature_extraction(self.config)
        print('Ok')

        return

    ###########
    # ML STEP # s
    ###########
    @timelog
    def _scores(self):
        """ Call PULSE machine learning routine to score the splicing events.


        """
        print('Scoring....', end="", flush=True)

        check_ouputs(self.config, 'Features')

        create_folders(self.config, self.config['SCORE_OUTPUT'])

        generate_features_matrix(self.config)

        run_PULSE(self.config)

        check_ouputs(self.config, 'Score')

        summary_pulse_output(self.config)

        print('Ok')

        return

    # ACCESSORY

    @timelog
    def _download(self, manifestfile):

        with open(manifestfile, 'r') as input_file:
            for line in input_file:
                logging.info('Downloading %s', line)
                run_TCGA_downloader(line.strip(), self.config)

    def _ddbb_write(self):
        # run_writer(self.options.samples, self.options.ddbb_config)
        pass


##############################################################
##############################################################
##############################################################
##############################################################
##############################################################

#! /usr/bin/env python
from __future__ import print_function
from __future__ import division

# STD lib
import logging
import time
import argparse
import sys
import os

# PULSE modules
from rnapulse.pipeline import Director
from rnapulse.support.utils import *


def setup_options():
    """Setup the pipeline.

    Parameters
    ----------

    Returns
    -------

    """
    parser = argparse.ArgumentParser(description="""RNApulse

    RNAseq pipeline for PULSE Launcher Usage: 
     $ rnapusle [update, run] -i path/sample

    or Run
     $ `rnapulse -h` for help information
    
    """)

    parser.add_argument(
        'action',
        help='main pipeline action to be perform by the pipeline',
        choices=['run', 'update'])

    parser.add_argument(
        '-i',
        '--input',
        dest='sample_id',
        help='BAM filename or/and Sample id after preprocess',
        default=False)

    parser.add_argument(
        '-o',
        '--output',
        dest='work_folder',
        help='Folder contains all the output, by default executing/folder/output/',
        default=os.getcwd())


# Optinal arguments
    parser.add_argument(
        '--rst',
        dest='rst',
        action='store',
        nargs='+',
        help="Restart specified steps to restart pipeline. "
        "assembly, preprocess, mapping, anchoring, features, scoring. "
        "A step will fail if any required output of a previous step is missing")

    parser.add_argument(
        '--only',
        dest='only',
        action='store',
        nargs='+',
        help="Run only specified steps for this sample "
        "assembly, preprocess, mapping, anchoring, features, scoring.  "
        "A step will fail if any required output of a previous step is missing")
    
    parser.add_argument(

        '--cpus',
        dest='cpus',
        action='store',
        help='Max number of Cores available for the pipeline (default 2)')
    
    parser.add_argument(
        '--config_file',
        dest='configfile',
        default=os.path.split(os.path.realpath(__file__))[0] + '/config.yml',
        help='Path and configuration variables in json format, by default config.yml in the installation folder')

    parser.add_argument(
        '--logging',
        dest='logging',
        action='store',
        default='info',
        help="""Setup the verbose intensitive of the log file, by default this parameter is set to "info", """
        """change it to  "debug" if it needs it. """)


    parser.add_argument(
        '--retrain_models',
        dest='retrain',
        action='store_true',
        default=False,
        help="RNApulse already comes with 5 train models ready to go. However, some user"
        " may what to force to train the models during the run. This is not recommended "
        "without a good reason; the pipeline will be sensible slower and the scoring values "
        "may change for the same event in different runs. ")

    opts = parser.parse_args()

    return opts, parser


def main():
    # Init setup
    options, parser = setup_options()
    # .bam extension have been added, remove it

    if options.logging == 'info':
        loglevel = logging.INFO
    else:
        loglevel = logging.DEBUG

    # init Logging
    # time stamp
    time_stamp = time.ctime()
    tsec = time.time()
    tsec = ''.join(str(tsec).split('.'))

    logging.basicConfig(
        level=loglevel,
        format='%(asctime)s - %(name)s - %(levelname)s :::  %(message)s',
        datefmt='%m-%d %H:%M',
        filename='{FLOG}/pulse_{SECONDS}.log'.format(FLOG=options.work_folder,
                                                     SECONDS=tsec),
        filemode='w')

    logger = logging.getLogger(__name__)
    logger.info('JOB START {4} {1} {2} {0} {3}'.format(*time_stamp.split()))
    logger.info("MSG: Cfg File load from:\t %s", options.configfile)

    # START
    # Pipeline
    if options.action == 'run':

        if options.sample_id:
            if options.sample_id[-4:] == '.bam':
                options.sample_id = options.sample_id[:-4]
            config = load_default_config(
                options.configfile, options.sample_id, options.work_folder)
            if options.retrain:
                config['FAST_MODELS'] = False
            if options.cpus:
                config['CPUS'] = options.cpus
            logger.info("Running sample:\t %s", options.sample_id)
            run_pipeline = Director(options, config)
            run_pipeline()
        else:
            parser.print_help()
            raise ValueError('Input folder is required')

    if options.action == 'download':
        config = load_default_config(
            options.configfile, 'DOWNLOADING', options.work_folder)

        logger.info("Downloading samples")

        if options.manifestfile and is_non_zero_file(options.manifestfile):
            downloader = Director(options, config)
            downloader()

        else:
            raise SystemError('file {} not found'.format(options.manifestfile))

    if options.action == 'update':

        config = load_default_config(
            options.configfile, 'UPDATING', options.work_folder)

        logger.info("Updating Files")

        tocheck = {'Models': config['SUPPORT'] + '/models/',
                   'Genome': config['SUPPORT'] + '/genomes/',
                   'Uniprot': config['SUPPORT'] + '/uniprot/',
                   'Pfam': config['SUPPORT'] + '/pfam/'}

        for tag, folder in tocheck.items():
            print('Checking {}.....'.format(tag), end="", flush=True)
            try:
                ifdont_create(folder)

                print(
                    'Downloading  {} supporting files, this can take a few minutes...'.format(tag))
                get_files(tag, folder)

                print('OK')
            except Exception as error:
                print('Caught this error: ' + repr(error))

    return

    # END

    time_stamp = time.ctime()
    logger.info("\n\n\n")
    logger.info('JOB ENDS {4} {1} {2} {0} {3}'.format(*time_stamp.split()))


if __name__ == "__main__":
    main()

#should use this to collapse cat_huniprot_as_len_explicit.txt instead of collapsing just uniprot exon indices

readFrom = open('C:/Users/kimguest/Desktop/playAround/mutations/for.yanqi.mutations.txt', 'r')
readFrom1 = open('C:/Users/kimguest/Desktop/playAround/filtered_uniprot_exon_indices.txt', 'r')

writeTo = open('C:/Users/kimguest/Desktop/playAround/mutation_read.txt', 'w')


uniprot_to_index_to_disease = {}


def countStuff(mapping, uniprot, start, end):
	count = 0
	if mapping.has_key(uniprot):
		if start <= end:
			for i in range(start,end+1):
				if not mapping[uniprot].has_key(i):
					pass
				elif mapping[uniprot][i] == "*":
					count = count + 1
			return (1.0*count)/(end - start + 1)
			
	return count


for line in readFrom:

	try:
		tokens = line.split()
		uniprot = tokens[0]
		index = int(tokens[1])
		type = tokens[3]
		
		if uniprot_to_index_to_disease.has_key(uniprot):
			uniprot_to_index_to_disease[uniprot][index] = "*"
		else:
			uniprot_to_index_to_disease[uniprot] = {index:"*"}
	except ValueError:
		print "Cannot parse: " + line.strip()
		
			
			
for line in readFrom1:
	tokens = line.split()
	
	asid = tokens[0].split("_")[0]
	prot = tokens[1]
	sstart = int(tokens[2])
	start = int(tokens[3])
	end = int(tokens[4])
	eend = int(tokens[5])
	
	#if (end - start + 1) <= 0:
	#	continue
	
	c1Count = 0
	aCount = 0
	c2Count = 0
	canonicalAbsolute = 0
	#otherAbsolute = 0
	
	if not uniprot_to_index_to_disease.has_key(prot):
		c1Count = 0
		aCount = 0
		c2Count = 0
		canonicalAbsolute = 0
		otherAbsolute = 0
	else:
		c1Count = countStuff(uniprot_to_index_to_disease, prot, sstart, start)
		aCount = countStuff(uniprot_to_index_to_disease, prot, start, end)
		c2Count = countStuff(uniprot_to_index_to_disease, prot, end, eend)
		
		#use teh new prot length provided
		protLen = int(line.split("\t")[7].strip())
		#otherProt = asid + "//" + prot + "-ST"
		#otherProtLen = int(line.split("\t")[8].strip())
		canonicalAbsolute = countStuff(uniprot_to_index_to_disease, prot, 1, protLen)
		#otherAbsolute = countStuff(uniprot_to_index_to_disease, otherProt, 1, otherProtLen)
		
		
	print >> writeTo, asid + "\t" + prot + "\t" + repr(c1Count) + "\t" + repr(aCount) + "\t" + repr(c2Count) + "\t" + repr(canonicalAbsolute)# + "\t" + repr(otherAbsolute)




from __future__ import print_function

import sqlite3
import sys

def uniprot_tm_table(filename, database):

    conn = sqlite3.connect(database)
    c = conn.cursor()

    # Create table for transmem_index
    c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_tm
        (id TEXT, position INT, result INT)""")

    c.execute("""CREATE INDEX IF NOT EXISTS uniprot_tm_index on uniprot_tm (id)""")

    with open(filename, "r") as input_file:

        # insert a row of data
        for line in input_file:
            row = line.split('\t')
            new_uniprot_id = row[0]

            for i in range(int(row[1]), int(row[2])):
                c.execute("INSERT INTO uniprot_tm VALUES (?, ?, ?)", \
                    (new_uniprot_id, i, "*"))

    conn.commit()
    conn.close()



def uniprot_elm_table(filename, database):

    conn = sqlite3.connect(database)
    c = conn.cursor()

    # Create table for transmem_index
    c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_elm
        (id TEXT, position INT, result INT)""")

    c.execute("""CREATE INDEX IF NOT EXISTS uniprot_rlm_index on uniprot_elm (id)""")

    with open(filename, "r") as input_file:

        # insert a row of data
        for line in input_file:
            if line[0] != '#':
                row = line.split('\t')
                new_uniprot_id = row[0]
                new_tm_start = row[2]
                new_tm_end = row[3].strip()

                for i in range(int(new_tm_start), int(new_tm_end)):
                    c.execute("INSERT INTO uniprot_elm VALUES (?, ?, ?)", \
                        (new_uniprot_id, i, "*"))

    conn.commit()
    conn.close()

def uniprot_PTM_table(filename, database):
    conn = sqlite3.connect(database)
    c = conn.cursor()

    # Create table for transmem_index
    c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_ptms
        (id TEXT, position INT, PTM TEXT, description TEXT)""")

    c.execute("""CREATE INDEX IF NOT EXISTS uniprot_ptm_index on uniprot_ptms (id)""")

    with open(filename, "r") as input_file:

        for line in input_file:
            row = line.split('\t')
            new_uniprot_id = row[0]
            new_PTM = row[1]
            new_position = row[2]

            c.execute('SELECT count(*) FROM uniprot_ptms WHERE id=? and position=?', (uniprot_id,new_position,))
            table_data = c.fetchone()[0]
            if table_data == 0:
                c.execute("INSERT INTO uniprot_ptms VALUES (?, ?, ?, ?)", \
                (new_uniprot_id, new_position, '*', new_PTM))
            else:
                continue

    conn.commit()
    conn.close()


def uniprot_Mut_table(filename, database):
    conn = sqlite3.connect(database)
    c = conn.cursor()

    # Create table for transmem_index
    c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_mutations
        (id TEXT, position INT, mutation TEXT, description TEXT)""")

    c.execute("""CREATE INDEX IF NOT EXISTS uniprot_mutations_index on uniprot_mutations (id)""")

    with open(filename, "r") as input_file:

        for line in input_file:
            row = line.split('\t')
            uniprot_id = row[0]
            org_mut = row[3]
            new_position = row[1]

            c.execute('SELECT count(*) FROM uniprot_mutations WHERE id=? and position=?', (uniprot_id,new_position,))
            table_data = c.fetchone()[0]
            if table_data == 0:

                c.execute("INSERT INTO uniprot_mutations VALUES (?, ?, ?, ?)", \
                    (uniprot_id, new_position, '*', org_mut))
            else:
                continue
    conn.commit()
    conn.close()


def uniprot_iupred_table(filename, database):

    """
    Note: Need to change the input file's directory
    """

    conn = sqlite3.connect(database)
    c = conn.cursor()

    # Create table for transmem_index
    c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_iupred
        (id TEXT, residue_position INT,\
            result TEXT)""")
    #create index to improve performance of the queries
    #slow down the input
    c.execute("""CREATE INDEX IF NOT EXISTS uniprot_iupred_index on uniprot_iupred (id)""")

    with open(filename, "r") as input_file:
        # insert a row of data

        for line in input_file:
            row = line.split('\t')
            new_residue_position = row[0]
            # new_residue_name = row[1]
            new_uniprot_id = row[2]
            # new_value = row[3]
            new_result = row[4].strip()

            c.execute('SELECT count(*) FROM uniprot_iupred WHERE id=? and residue_position=?', (new_uniprot_id,new_residue_position,))
            table_data = c.fetchone()[0]
            if table_data == 0:
                c.execute("INSERT INTO uniprot_iupred VALUES (?, ?, ?)", \
                (new_uniprot_id, new_residue_position, new_result))
            else:
                continue



    conn.commit()
    conn.close()

def uniprot_core_table(filename, database):

    """
    Note: Need to change the input file's directory
    """

    conn = sqlite3.connect(database)
    c = conn.cursor()

    # Create table for transmem_index
    c.execute(""" CREATE TABLE IF NOT EXISTS uniprot_core
        (id TEXT, residue_position INT,\
            result TEXT)""")
    #create index to improve performance of the queries
    #slow down the input
    c.execute("""CREATE INDEX IF NOT EXISTS uniprot_core_index on uniprot_core (id)""")

    with open(filename, "r") as input_file:
        # insert a row of data

        for line in input_file:
            row = line.split('\t')
            new_residue_position = row[1]
            # new_residue_name = row[1]
            new_uniprot_id = row[0]
            # new_value = row[3]
            new_result = row[2].strip()

            c.execute('SELECT count(*) FROM uniprot_core WHERE id=? and residue_position=?', (new_uniprot_id,new_residue_position))
            table_data = c.fetchone()[0]
            if table_data == 0:
                c.execute("INSERT INTO uniprot_core VALUES (?, ?, ?)", \
                (new_uniprot_id, new_residue_position, new_result))
            else:
                continue



    conn.commit()
    conn.close()

'''
##################################
############# MAIN ###############
##################################
'''

def main():

    # Example command:
    # python uniprot_db_program.py -transmem Transmem_index.inp uniprot_features_test.db
    #                                                             [1]                           [2]                     [3]

    # File will output to uniprot_features.db
    try:
        filename = str(sys.argv[2])
        database = str(sys.argv[3])

    except IndexError, e:
        print( 'files %s not found' % str(e))
        print( '''Usage:
                    load_canonical_database.py -opt input_file ddbb_name

                    OPTIONS
                        -transmem
                        -ptm
                        -iupred
                        -elm
                        -mut
                        -core''')
        sys.exit()

    if sys.argv[1] == "-transmem":
        print('Loading Transmembraning ...')
        print('Input\t',filename)
        print('DataBase\t',database)
        uniprot_tm_table(filename, database)
        print('Done')
    elif sys.argv[1] == "-ptm":
        print('Loading Post Transcriptional Modifications ...')
        print('Input\t',filename)
        print('DataBase\t',database)
        uniprot_PTM_table(filename, database)
        print('Done')
    elif sys.argv[1] == "-iupred":
        print('Loading Disorder ...')
        print('Input\t',filename)
        print('DataBase\t',database)
        uniprot_iupred_table(filename, database)
        print('Done')
    elif sys.argv[1] == "-elm":
        print('Loading Disorder ...')
        print('Input\t',filename)
        print('DataBase\t',database)
        uniprot_elm_table(filename, database)
        print('Done')
    elif sys.argv[1] == "-mut":
        print('Loading Mutations ...')
        print('Input\t',filename)
        print('DataBase\t',database)
        uniprot_Mut_table(filename, database)
        print('Done')
    elif sys.argv[1] == "-core":
        print('Loading Core (Sable output) ...')
        print('Input\t',filename)
        print('DataBase\t',database)
        uniprot_core_table(filename, database)
        print('Done')

    # add to database (automatically skips duplicates)
    #   if sys.argv[2] == "-transmem":
    #   elif sys.argv[2] == "-ptm":
    #   elif sys.argv[2] == "-iupred":


if __name__ == "__main__":


    main()
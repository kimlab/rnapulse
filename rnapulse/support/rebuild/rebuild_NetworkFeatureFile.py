'''
should use this to collapse cat_huniprot_as_len_explicit.txt instead of collapsing just uniprot exon indices

'''
from __future__ import print_function


# INPUT
readFrom = open('betweenness.csv', 'r')
readFrom1 = open('degree.csv', 'r')
readFrom2 = open('unigene_to_name.txt', 'r')
# OUTOUT
writeTo1 = open('unigene_betweenness.txt', 'w')
writeTo2 = open('unigene_degree.txt', 'w')


name_to_unigene = {}
wtf = 0

for line in readFrom2:

	tokens = line.split()
	
	if len(tokens) >= 2:	
		unigene = tokens[1]
		name = tokens[0]
		
		name_to_unigene[name] = unigene
	else:
		wtf += 1
		print(line.strip())

print(repr(wtf))
	
for line in readFrom:
	tokens = line.split(",")
	name = tokens[1].strip()
	score = tokens[2].strip()
	
	if name_to_unigene.has_key(name):
		unigene = name_to_unigene[name]
		print(unigene + "\t" + score, writeTo1)
	
for line in readFrom1:
	tokens = line.split(",")
	name = tokens[1].strip()
	score = tokens[2].strip()
	
	if name_to_unigene.has_key(name):
		unigene = name_to_unigene[name]
		print(unigene + "\t" + score, file=writeTo2)
		
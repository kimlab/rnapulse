from __future__ import print_function
import glob
import sys

uniprots_done={}
filename = sys.argv[1]
with open(filename,'r') as input_file:
    for line in input_file:
        data = line.strip()
        uniprots_done[data] =1

sable_files = glob.glob('*.out')

for sable_output in sable_files:
    temporal_done = []
    with open(sable_output,'r') as input_file:
        for line in input_file:
            data = line.split('\t')
            if uniprots_done.has_key(data[0]):
                pass
            else:
                temporal_done.append(data[0])
                print(line.strip())
        for a in temporal_done:
            uniprots_done[a] =1

output = open('insable.list.new','w')
for i in uniprots_done.iterkeys():
    print(output,i)

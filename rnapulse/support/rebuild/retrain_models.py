from __future__ import print_function
from __future__ import division
import glob
import pandas as pd
import numpy as np
import dill as pkl

from sklearn.ensemble import RandomForestClassifier

def score_ensemble(models, data):



    results = list()
    for m in models:
        r = m.predict_proba(data)[0:,1:]
        results.append(r)

    ensembl = np.asarray(results)
    scores = ensembl.sum(axis=0) / len(models)

    return scores


def save_score(scores, cfg):

    o = open(, 'w')
    scores.tofile(o, format="%.3f", sep='\n')
    o.close()

    return



train_features = pd.read_csv('../rnapulse/support/params/ML_Training_data.inp', sep=',')


# Parameters
TP = 145  # Number of training examples, Curated Positives
N = train_features.shape[0]  # Positives + Unlabelled data
# generate labels
labels_train = np.zeros(N)
for i in range(N):
    if i < TP:
        labels_train[i] =1
    else:
        labels_train[i] = 0


models = list()
num_interations = 5
for i in range(num_interations):

    # train random classifier
    Y = labels_train.copy()
    clf = RandomForestClassifier(n_estimators=2000, n_jobs=8, oob_score=True)
    clf.fit(train_features[ff[:-1]], labels_train)
    # get error out of the bag and use it to redefine 1/0 boundery
    # compute C value
    vec_oob = clf.oob_decision_function_
    err = vec_oob[0:,1:]
    cvalue = err[:TP].sum()/err.sum()
    # reassign labels
    for j in range(N):
        label = 0
        if j < TP:
            label =1
        else:
            # reformed probability 
            prob = err[j]
            mprob = ((1-cvalue)/cvalue)*(prob/(1-prob))
            cutoff = np.random.uniform(0,1)
            # relabel unlabelled data based on probability
            if cutoff <= mprob:
                label =1
            else:
                label = 0

        Y[j] = label
    # train new model with 
    model_random = RandomForestClassifier(n_estimators=2000, n_jobs=8, oob_score=True)
    model_random.fit(train_features[ff[:-1]], Y)
    models.append(model_random)


for idx, m in enumerate(models):
    pkl.dump(m, open('MODEL_{}.pkl'.format(idx), 'wb'))
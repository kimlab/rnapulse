import pickle
import glob
import os
import sys


def generate_pickled_ref_genome(genome_fasta_files_path):
    """ get full path of all files end with .fa .fas .fasta
        The chromosome name must be enclosed between genome_fasta_files_path and the extension
        between .

    Parameters
    ----------

    Returns
    -------

    """

    fasta_files = glob.glob(genome_fasta_files_path+'*.fa*')
    ref_genome = dict()

    for fasta_file in fasta_files:

        filename = os.path.basename(fasta_file)
        chrom = filename.split('.')[0]
        seq = load_chromosome(fasta_file)

        print("LOADING chr: %s" %  (chrom))
        ref_genome[chrom] = seq
        print("%i bp LOADED" % (len(seq)))

    with open(genome_fasta_files_path+'reference.pkl', 'wb') as output:
        pickle.dump(ref_genome, output, pickle.HIGHEST_PROTOCOL)


    print("DONE:\n %s" %  (genome_fasta_files_path+'.pkl'))
    return


def load_chromosome(filename):

    chr_sequence = ''

    with open(filename,'r') as input_file:
        for line in input_file:
            if line[0] == '>':
                pass
            else:
                chr_sequence += line.strip()

    return chr_sequence.upper()



if __name__ == "__main__":
    # pass folder containing fasta files.
    generate_pickled_ref_genome(sys.argv[1])
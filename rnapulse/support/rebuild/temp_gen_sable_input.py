from __future__ import print_function
import sys
'''
for i in {1..3356};do mkdir part.${i};mv poll_isoforms_small_parts.${i}.fas  part.${i}; cp part.${i}/poll_isoforms_small_parts.${i}.fas part.${i}/data.seq;done
for i in {1..3356};do cp ../../Lungcancer/isoform/part.1/run.1.sable  part.${i}/run.${i}.sable;done
for i in {1..3356};do submitjob 48 -c 1 -m 45 ./run.${i}.sable;cd ..;done
for i in {1..3356};do cd part.${i}; submitjob 48 -c 1 -m 45 ./run.${i}.sable;cd ..;done

'''
def fasta2dict(filename):
    fastadict = dict()
    entry_id = ''
    seq = ''
    with open(filename,'r') as input_file:
        for line in input_file:
            if line.startswith('>'):
                fastadict[entry_id] = seq
                entry_id = line[1:].strip()
                seq = ''
            else:
                seq += line.strip()

        fastadict[entry_id] = seq
        del fastadict['']

    return fastadict

def dict2fasta(dictionary,filename):

    out = open(filename,'w')

    for idx,seq in dictionary.items():
        print('>{}'.format(idx),file=out)
        print(''.format(seq),file=out)

    out.close()
    return

filename = sys.argv[1]
uniprots = dict()
with open(filename,'r') as input_file:
    for line in input_file:
        tab_data =line.split()

        _id_ = tab_data[-1:]

        uniprots[_id_[0].strip()] =  ''

uniseq = fasta2dict('./support/uniprot/uniprot_sprot.fasta')
for idx,uid in enumerate(uniprots.keys()):
    try:
        seq = uniseq[uid]
        dict2fasta({uid:seq},'to_sable.{}.fa'.format(idx))
    except KeyError:
        print(uid)
        pass



'''
used to call iupred and score all the seq in a fasta file.
the ouput is suitable for the features script for canonical or putative isoforms


python generate_iupred_file.py  proteome.fasta

'''
from __future__ import print_function
import sys, os


#################
#### FUNCTIONS

def delete_tmp_file():

	os.system("rm temp.seq")

	return

def write_tmp_file(content):

	writeTo = open('temp.seq', 'w')
	print('temp seq', file= writeTo)
        print(content, file= writeTo)

        writeTo.close()
	return


def exec_uipred():


	return os.popen("iupred temp.seq long").readlines()


def save_iupred_out(output, prot_id):

	for line in output:

		if line[0] == "#":
			pass
		else:
			pos = line[0:5].strip()
			res = line[6:8].strip()
			score = line[13:19].strip()
			binary_score = transform_score(score)
			print("%s\t%s\t%s\t%s" % (str(pos),res,prot_id,binary_score))

	return

def transform_score(score):


	if float(score) > .5:
		return "."
	else:
		return "*"



def generate_disorder_for(filename):

	prot_seq = ''
	prot_id = ''
	with open(filename,'r') as input_file:

		for line in input_file:

			if line[0] == ">":

				if len(prot_seq) > 0:

					write_tmp_file(prot_seq)
					output_uipred = exec_uipred()

					save_iupred_out(output_uipred, prot_id)


				prot_id = line[1:].strip()
				prot_seq = ''
			else:
				prot_seq += line

		write_tmp_file(prot_seq)
		output_uipred = exec_uipred()
		save_iupred_out(output_uipred, prot_id)


###############
#### MAIN

def main():
	
	canonical_proteins = sys.argv[1]
	generate_disorder_for(transcripts_proteins)

if __name__ == "__main__":
	main()

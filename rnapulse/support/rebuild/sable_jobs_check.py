from __future__ import print_function
import os

def get_done_seq(folder_name):
    input_seq_file = open(folder_name+'/OUT_SABLE_1/OUT_SABLE_1_RES')
    list_seq_name = []
    for line in input_seq_file.readlines():

        if line[0] == 'Q':
            tab_data = line.split(':')
            seq_name = tab_data[1].strip()

        if line.strip() == 'END_SECTION':

            #some time the job do not complete but the header of the seq is there, that
            list_seq_name.append(seq_name)
            seq_name = ''


    return list_seq_name

def get_input_seq(folder_name):
    input_seq_file = open(folder_name+'/data.seq')
    dict_seq_name = {}
    seq = ''
    id_seq = ''
    for line in input_seq_file.readlines():
        if line[0] == '>':
            dict_seq_name[id_seq] = seq
            id_seq = line[1:].strip()
        else:
            seq = line.strip()

    dict_seq_name[id_seq] = seq
    return dict_seq_name


#print os.listdir(os.getcwd())
for folder_name in os.listdir(os.getcwd()):
    if '.py' in folder_name or '.out' in folder_name or '\\' in folder_name:
	    pass
    else:
    	input_seqs = get_input_seq(folder_name)
    	done_seqs = get_done_seq(folder_name)
    #print '#'+folder_name
    	for seq_id,seq in input_seqs.items():
        	if not seq_id in done_seqs:
            		print('>'+seq_id)
            		print(seq)


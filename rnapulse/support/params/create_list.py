#get uniprot id list in pfam file
from __future__ import print_function
import sys

filename = sys.argv[1]

dict_uniprots = {}


with open(filename,'r') as input_file:
    for line in input_file:
        data = line.split('\t')
        if data[0] in dict_uniprots:
            pass
        else:
            dict_uniprots[data[0]] =1


for i in dict_uniprots.keys():
    print(i)

'''
Quick&Dirty script to
Download and keep track of datasets downloaded from cghub

'''
from __future__ import print_function
import subprocess
import pandas as pd
import sys
import shutil
import os


def run_downloader(file_id):
    """Run Downloader data.

    Paramaters
    ----------
    file_id : str
        File id

    """
    # download
    # cghub_bin =  '/home/ccorbi/bin/cghub/bin/gtdownload'
    cghub_bin =  '/home/kimlab2/ccorbi/bin/gdc-client'
    # path_key  = 'https://cghub.ucsc.edu/software/downloads/cghub_public.key'
    path_key = '/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/support/TCGA_mapping/gdc-user-token.txt'

    command1 = [cghub_bin,
                'download',
                file_id,
                '-t',
                path_key]



    print("Downloading: {}".format(file_id))
    p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)

    exit_codes = p1.wait()
    return exit_codes

def get_samples_to_download(data,limit=2):
    """Read  control file and map file."""

    # Extract ids from already preprocessed files
    n=0
    results =  list()
    # get N new files
    for i,r in data.iterrows():

        if n >=limit:
            continue
        else:

            #results.append(r.to_frame())

            df2 = pd.DataFrame([r.values], columns=data.keys())
            results.append(df2)
            n+=1
    # return
    return pd.concat(results)



def main():

    # Working directory
    GL_PATH = '/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/'
    # download path
    dst = GL_PATH + 'data/input/rnaseq'

    # Control File, what it is done. ToDo: query to the database
    # ctrl_file = GL_PATH + 'ctrl_file.xls'
    # read done
    # done = pd.read_excel(ctrl_file)
    inventary = pd.read_csv('/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/download_inventary.csv')
    # Map file from arg
    todo_file = GL_PATH + sys.argv[1]

    # read todo
    todo = inventary[inventary['DOWNLOADED'].isnull()]

    # Get id to download restrict by limit
    n = 120
    tasks = get_samples_to_download(todo,limit=n)
    disease = sys.argv[2]
    # reformat, add info
    #tasks = change_format(tasks, sys.argv[2])
    tasks['DISEASE'] = disease
    tasks['DOWNLOADED'] = ''
    # for file
    print(tasks)
    for i,row  in tasks.iterrows():
        # create a folder
        if not os.path.isfile(dst+'/'+row['SOURCE_ID']):
            # Download NGS info from repo
            print('Downloading ... {}'.format(row['SOURCE_ID']))
            run_downloader(row['UUID_SEQ'])
            # Clean data
            shutil.move('./'+row['UUID_SEQ']+'/'+row['SEQ_FILE_NAME'], dst+'/'+row['SOURCE_ID']+'.bam')
            shutil.rmtree('./'+row['UUID_SEQ']+'/')

            idx = inventary.loc[inventary.SOURCE_ID==row['SOURCE_ID']].index.get_values()[0]
            inventary.loc[idx, 'DOWNLOADED'] = 'LOCAL'
            inventary.to_csv('/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/download_inventary.csv', index=False)
            # download clin
            run_downloader(row['UUID_CL'])
            # Temporal solution
            # a few clinal data is already on that folder, move fails if already exist
            try:
                shutil.move('./'+row['UUID_CL']+'/'+row['CL_FILE_NAME'], './data/Clinical/')
            except shutil.Error:
                shutil.rmtree('./'+row['UUID_CL']+'/')

        # Run beagle job
        # command1 = ['submitjob',
        #            '-m','34',
        #            'python','app.py',
        #            '-i',row['SOURCE_ID']+'.bam',
        #            '-a','a']

        # print  command1
        # p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)
        # p1.wait()
        print('{},{}'.format(row['SOURCE_ID'],row['UUID_SEQ']))
        #print 'submitjob 50 -m 12 python app.py -i {} -a a'.format('FOLDER_NAME')
        print('{},{}'.format(row['SOURCE_ID'],row['UUID_SEQ']))

    # write to submit id
    # run


    #update donefile
    #done_file = pd.concat([done,tasks])
    tasks.to_excel(GL_PATH+'todo_tasks.xls',index=False)









if __name__ == '__main__':

    main()

"""
rnapulse utils
"""
from __future__ import print_function
from collections import OrderedDict
import sys
import os
import time
import hashlib
import subprocess
import logging
import yaml
import unicodedata
import requests
import tarfile
import glob
import zipfile
from tqdm import tqdm
from rnapulse.support.folder_structure import INVENTORY


logger = logging.getLogger(__name__)


###
# LOGGING DECO
###


def timelog(method):

    def timed(*args, **kw):
        ts = time.time()
        logger.info("###################################################")
        logger.info("START: %s ", method.__name__)
        logger.info("###################################################")
        result = method(*args, **kw)
        te = time.time()

        logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        logger.info("END: %s  TIME: %is ", method.__name__, te - ts)
        logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

        return result

    return timed


def timelog_ddbb_debug(method):

    def timed(*args, **kw):
        ts = time.time()
        logger.debug("###################################################")
        logger.debug("START: %s ", method.__name__)
        logger.debug("###################################################")

        logger.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        logger.debug('INPUT: %s ',  method.__name__)

        try:
            logger.debug('ROWS: %i ',  args[0].shape[0])
            logger.debug('HEAD')
            logger.debug(args[0].head())
        except:
            logger.debug(args[0])

        logger.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

        result = method(*args, **kw)
        te = time.time()

        logger.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        logger.debug('OUTPUT: %s ',  method.__name__)
        logger.debug('ROWS: %i ',  result.shape[0])
        logger.debug('HEAD')
        logger.debug(result.head())
        logger.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

        logger.debug("###################################################")
        logger.debug("END: %s  TIME: %is ", method.__name__, te - ts)
        logger.debug("###################################################")

        return result

    return timed


###
### CONFIG & INTEGRETY
###

def load_default_config(configfile, sample_id, work_folder):
    """Load json file with all the paths and Variables to run PULSE.

    Parameters
    ----------
    configfile : str
        path to the configuration file in json format

    Returns
    -------
    cfg : dict
        Paths and variables to Run PULSE pipeline


    """

    logger.info("Loading configuration")
    with open(configfile, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    cfg['SAMPLE_ID'] = sample_id
    cfg['WORK_PATH'] = work_folder
    file_path, _ = os.path.split(os.path.realpath(__file__))
    cfg['RNAPULSE_PATH'] = file_path.replace('support', '')
    # fill out this first
    cfg['FOLDER_RNASEQ_INPUT'] = cfg['FOLDER_RNASEQ_INPUT'].format(**cfg)
    cfg['FOLDER_OUTPUT'] = cfg['FOLDER_OUTPUT'].format(**cfg)
    # then outputs
    for key in ['ASSEMBLY_OUTPUT', 'PREPROCESS_OUTPUT', 'FEATURES_OUTPUT', 'SCORE_OUTPUT', 'FOLDER_LOGS']:
        cfg[key] = cfg[key].format(**cfg)
        if not os.path.exists(cfg[key]):
            os.makedirs(cfg[key])
    # now the rest
    logger.debug('CONFIG')
    for key, value in cfg.items():
        if type(value) == str:
            cfg[key] = value.format(**cfg)

        logger.debug("%s : %s", key, cfg[key])

    return cfg


def update_config(configfile, config):
    """Load json file with all the paths and Variables to run PULSE.

    Parameters
    ----------
    configfile : str
        path to the configuration file in json format

    Returns
    -------
    cfg : dict
        Paths and variables to Run PULSE pipeline


    """
    logger.info("Updating configuration")

    with open(configfile, 'r') as ymlfile:
        cfg = yaml.load(ymlfile)

    for key, value in configfile.items():
        if type(value) == str:
            config[key] = value.format(**cfg)

        logger.debug("%s : %s", key, config[key])

    return config


def create_folders(config, step_output):
    """Create sample folders.
        FOLDER_OUTPUT / STEP / SAMPLE_ID

    Parameters
    ----------
    config : dict

    step : str

    Returns
    -------

    """

    paths_to_create = [step_output,
                       step_output + '/temp']
    for path in paths_to_create:
        logger.info("MSG: Creating Folders:  %s", path)
        if not os.path.exists(path):
            os.makedirs(path)
        else:
            logger.warning("WARNING: Folder already Exists:  %s  ", path)

    return


def check_ouputs(config, step):

    # datapath = config['PULSE_PATH'] + config['FOLDER_OUTPUT']

    Files2check = OrderedDict()
    Files2check['Assembly'] = {'FOLDER': ['ASSEMBLY_OUTPUT'],
                               'FILES': ['TRANSCRIPT_ASSEMBLY']}

    Files2check['Preprocess'] = {'FOLDER': ['PREPROCESS_OUTPUT'],
                                 'FILES':  ['AS_LOC', 'EXTENDED', 'COMPLETE_TRANSCRIPTS', 'EVENTS_FASTA']}

    Files2check['Anchoring'] = {'FOLDER': ['PREPROCESS_OUTPUT'],
                                'FILES':  ['BLAST_OUTPUT']}

    Files2check['Mapping'] = {'FOLDER': ['PREPROCESS_OUTPUT'],
                              'FILES':  ['CDNA', 'PSEQ', 'ANCHOR_EVENT_INIDEX_MAP']}

    Files2check['Features'] = {'FOLDER': ['FEATURES_OUTPUT'],
                               'FILES': ['TM_FEATURES',
                                         'PTM_FEATURES',
                                         'ELM_FEATURES',
                                         'DISORDER_REFERENCE',
                                         'DISORDER_FEATURES',
                                         'PFAM_REFERENCE',
                                         'PFAM_FEATURES',
                                         'CORE_FEATURES',
                                         'MUT_FEATURES',
                                         'NETWORK_FEATURES']}

    Files2check['Score'] = {'FOLDER': ['SCORE_OUTPUT'],
                            'FILES': ['NAMES_ML', 'FEATURES_ML', 'PULSE_OUTPUT']}

    missing = list()
    file_info = Files2check[step]

    for f in file_info['FILES']:

        if is_non_zero_file(config[f]):
            pass
        else:
            missing.append('{} {}'.format(f, config[f]))

    if len(missing) > 0:
        logger.error("Missing: %s", missing)
        sys.exit(
            "Missing files from {} step, check log file for further details".format(step))

    return


def check_binaries(must_have):

    for bina in must_have:
        try:
            output = subprocess.Popen(
                [bina], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                print(bina)
                print(
                    '{0} is missing, please  install {0} before to use rnapulse '.format(bina))
                sys.exit(1)

    return


def check_suppfiles(cfg):

    tocheck = {'Models': cfg['SUPPORT'] + '/models/',
               'Genome': cfg['SUPPORT'] + '/genomes/',
               'Uniprot': cfg['SUPPORT'] + '/uniprot/',
               'Pfam': cfg['SUPPORT'] + '/pfam/'}

    for tag, folder in tocheck.items():
        print('Checking {}.....'.format(tag), end="", flush=True)
        try:
            if ifdont_create(folder) or missing_files(INVENTORY[tag], folder):

                print('Downloading {} files, this may take a few minutes...'.format(tag))
                get_files(tag, folder)

            print('OK')
        except Exception as error:
            print('Caught this error: ' + repr(error))

    return


def missing_files(repertuar, path):

    for fn in repertuar:
        if is_non_zero_file(path + fn):
            pass
        else:
            return True
    return False


################################################
#########################################


###
# FILE UTILS
###

def hashseqs(row):
    r = row.encode('utf-8')
    return hashlib.md5(r).hexdigest()


def get_filehash(inputFile):
    openedFile = open(inputFile)
    readFile = openedFile.read()

    sha1Hash = hashlib.sha1(readFile.encode('utf-8'))
    return sha1Hash.hexdigest()


def getsize(filename):
    """Return the size of a file, reported by os.stat()."""
    return os.stat(filename).st_size


def is_non_zero_file(fpath):
    if os.path.isfile(fpath) and getsize(fpath) > 0:
        return True
    else:
        return False


def ifdont_create(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        return True
    else:
        return False


def remove_files(list_files):

    for i in list_files:
        if os.path.isfile(i):
            os.remove(i)
    return


def normalize_unicode_data(data):
    """
    Returns a Python-normalized String object from unicode.

    :param data:
    :return:
    """
    try:
        normalized_data = unicodedata.normalize(
            'NFKD', data).encode('ascii', 'ignore')
        return normalized_data
    # if it is
    except TypeError:

        return data


###
# HOUSEKEEPING
###

def zipping(list_files, path='', name='transcriptome'):
    """Zip and delete file.

    Parameters
    ----------
    list_files : array_like
        list of file to zip and delete

    path : str
        files path

    name : str
        Zip file name

    Returns
    -------

    """
    zip_out = '{}{}.zip'.format(path, name)
    # I should move this to logs
    print('creating archive: {}'.format(zip_out))

    if not os.path.isfile(zip_out):
        zf = zipfile.ZipFile(zip_out, 'w', zipfile.ZIP_DEFLATED)
        # try:
        for i in list_files:
            # do not add folders
            print(i)
            if os.path.isfile(i):
                if not zipfile.is_zipfile(i):
                    print('adding {}'.format(i))
                    zf.write(i)
    # finally:
        print('closing')
        zf.close()
        # remove files
        remove_files(list_files)
    else:
        print('!Zip File already exists!: {}'.format(zip_out))

    return


def clean_temp_bam_files(config, ignore):

    temp_bam_files_path = config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']

    # no-rg

    folders = glob.glob(temp_bam_files_path + '/*')

    for fol in folders:
        if fol.find(ignore) == -1:
            print('Removing temporal BAM files: {} '.format(
                fol + config['FOLDER_SAMTOOLS_TEMP']))

            bams = glob.glob(fol + config['FOLDER_SAMTOOLS_TEMP'] + '*.bam')
            remove_files(bams)

    return


def zip_transcriptomes(config, ignore):

    # get folders
    # transcript
    folder_trans_path = config['PULSE_PATH'] + config['ASSEMBLY_OUTPUT']

    folders = glob.glob(folder_trans_path + '/*')

    for fol in folders:
        if fol.find(ignore) == -1:
            # get transcript files
            transcript_files = glob.glob(
                fol + config['FOLDER_CUFFLINKS'] + '/*')
            if len(transcript_files) > 0:
                path_target = fol + config['FOLDER_CUFFLINKS']
                # print(transcript_files)
                # print(path_target)
                # print('*'*39)g
                zipping(transcript_files, path=path_target,
                        name='transcriptome')
            else:
                print('!Folder emprty!: {}'.format(
                    fol + config['FOLDER_CUFFLINKS']))

            merged_transcript_files = glob.glob(
                fol + config['FOLDER_CUFFLINKS'] + '/*')
            if len(merged_transcript_files) > 0:
                path_target = path_target
                # print(merged_transcript_files)
                # print(path_target)
                # print('*'*39)
                zipping(merged_transcript_files,
                        path=path_target, name='merged')
            else:
                print('!Folder emprty!: {}'.format(
                    fol + config['FOLDER_CUFFLINKS'] + config['FOLDER_CUFFMERGE_OUTPUT']))

    return


def zip_preprocess(config):

    # get folders
    # transcript
    folder_trans_path = config['PULSE_PATH'] + config['PREPROCESS_FOLDER']

    folders = glob.glob(folder_trans_path + '/*')

    for fol in folders:
        # get transcript files
        transcript_files = glob.glob(fol + '/*')
        #path_target = fol+config['FOLDER_CUFFLINKS']
        # print(transcript_files)
        # print(path_target)
        # print('*'*39)
        zipping(transcript_files, path=fol + '/', name='preprocess')

    return


def zip_features(config):

    # get folders
    # transcript
    folder_trans_path = config['PULSE_PATH'] + config['FEATURES_FOLDER']

    folders = glob.glob(folder_trans_path + '/*')

    for fol in folders:
        # get transcript files
        transcript_files = glob.glob(fol + '/iupred_isoforms.out')
        #path_target = fol+config['FOLDER_CUFFLINKS']
        # print(transcript_files)
        # print(path_target)
        # print('*'*39)
        zipping(transcript_files, path=fol + '/', name='iupred_isoforms')

    return


def remove_temp_files(config, ignore):

    temp_bam_files_path = config['PULSE_PATH'] + config['PREPROCESS_FOLDER']

    # no-rg

    folders = glob.glob(temp_bam_files_path + '/*')

    for fol in folders:
        if fol.find(ignore) == -1:
            print('Removing temporal files: {} '.format(fol + '/temp/'))

            bams = glob.glob(fol + '/temp/*')
            remove_files(bams)

    return


###
# DOWNLOAD
###

def downloader(destination, link):
    chunk_size = 1024
    print("Downloading {}".format(destination))
    logger.debug("Downloading : %s, %s", destination, link)
    with open(destination, "wb") as f:

        response = requests.get(link, stream=True)
        total_length = response.headers.get('content-length')
        total_length = int(total_length)
        bars = int(total_length / chunk_size) + 1

        if total_length is None:  # no content length header
            f.write(response.content)
        else:

            for data in tqdm(response.iter_content(chunk_size=chunk_size),  unit="KB", leave=True):

                f.write(data)

    f.close()
    return


def get_repo():

    linkers = dict()
    linkers['Models'] = dict()
    for i in range(5):
        l = 'http://kimweb1.ccbr.utoronto.ca/library/rnapulse/support/models/RF_model_{}.pkl'.format(
            i)
        f = 'RF_model_{}.pkl'.format(i)
        linkers['Models'][f] = l

    linkers['Genome'] = dict()
    linkers['Genome']['grch38.tar.gz'] = 'http://kimweb1.ccbr.utoronto.ca/library/rnapulse/support/genomes/GRCh38.tar.gz'
    linkers['Genome']['GRCh38.pkl'] = 'http://kimweb1.ccbr.utoronto.ca/library/rnapulse/support/genomes/GRCh38.pkl'
    linkers['Genome']['GRCh38.refannotation.gtf'] = 'http://kimweb1.ccbr.utoronto.ca/library/rnapulse/support/genomes/GRCh38.refannotation.gtf'

    linkers['Uniprot'] = dict()
    linkers['Uniprot']['uniprot.tar.gz'] = 'http://kimweb1.ccbr.utoronto.ca/library/rnapulse/support/uniprot/uniprot.tar.gz'

    linkers['Pfam'] = dict()
    linkers['Pfam']['pfamhmm.tar.gz'] = 'http://kimweb1.ccbr.utoronto.ca/library/rnapulse/support/pfam/pfamhmm.tar.gz'

    return linkers


def get_files(tag, folder):
    linkers = get_repo()
    for mod_file, link in linkers[tag].items():
        if not is_non_zero_file(folder + mod_file):

            downloader(folder + mod_file, link)
        if 'tar' in mod_file:
            print('...unzipping..', end='', flush=True)
            logger.debug("unzipping : %s @ %s",  mod_file, folder)
            tar = tarfile.open(folder + mod_file, 'r:gz')

            tar.extractall(folder)
            tar.close()
            os.remove(folder + mod_file)
            print('DONE')


def run_TCGA_downloader(file_id, config):
    """Run Downloader data.

    Paramaters
    ----------
    file_id : str
        File id

    """

    command1 = [config['CGHUB_BIN'],
                'download',
                file_id,
                '-t',
                config['KEY']]

    print("Downloading: {}".format(file_id))
    p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)

    exit_codes = p1.wait()
    return exit_codes

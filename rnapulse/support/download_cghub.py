'''
Quick&Dirty script to
Download and keep track of datasets downloaded from cghub

'''
import subprocess
import pandas as pd
import sys
import shutil
import os


def run_downloader(status_id):
    """
    Download data
    """
    # download
    #cghub_bin =  '/home/ccorbi/bin/cghub/bin/gtdownload'
    cghub_bin =  '/home/kimlab2/ccorbi/bin/cghub/bin/gtdownload'
    path_key  = 'https://cghub.ucsc.edu/software/downloads/cghub_public.key'

    command1 = [cghub_bin,
                '-c',
                path_key,
                '-d',
                status_id]



    print("Downloading: {}".format(status_id))
    p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)

    exit_codes = p1.wait()
    return exit_codes

def get_samples_to_download(data,done,limit=2):

    done_uuid = done['UUID'].tolist()
    n=0
    results =  list()
    for i,r in data.iterrows():

        if n >=limit:
            continue

        if r['analysis_id'] in done_uuid:
            pass
        else:

            #results.append(r.to_frame())

            df2 = pd.DataFrame([r.values], columns=data.keys())
            results.append(df2)
            n+=1

    return pd.concat(results)

def change_format(df):

    #rename columns
    df.rename(columns={u'disease_name':u'SAMPLE_SOURCE',
                        u'analysis_id':u'UUID',
                        u'filename':u'FOLDER_NAME'},
                        inplace=True)
    # get cell line name
    df['CELL_LINE_NAME'] = df['barcode'].str.replace('CCLE-','').str.replace('-RNA-08','')
    # drop no longer need it fields
    df.drop([u'study', u'barcode', u'disease',  u'sample_type',
             u'sample_type_name', u'analyte_type', u'library_type',
             u'center', u'center_name', u'platform', u'platform_name',
             u'assembly', u'checksum',u'aliquot_id','files_size',
             u'participant_id', u'sample_id', u'tss_id', u'sample_accession',
             u'published', u'uploaded', u'modified', u'state', u'reason'],
             axis=1, inplace=True)
    # Add extra fields to fit done file
    df['SAMPLE_TYPE'] = 'CELL LINES'
    df['GROUP_ID'] = ''
    df['DISEASE'] = 'Cancer'
    df['JOBID'] = ''
    df['DESCRIPTION'] =''

    return df


# backup file just in case
GL_PATH = '/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/'

done_file = GL_PATH + 'Tasks.xls'
# backup = done_file + '.bak'
# shutil.copy(done_file, backup)

todo_file = GL_PATH + 'lungdataset_CCLE.tsv'
#backup = todo_file + '.bak'
#shutil.copy(todo_file, backup)

# read done
done = pd.read_excel(done_file)

# read todo
todo = pd.read_csv(todo_file,sep='\t')

# Get id to download limit
n = 10
tasks = get_samples_to_download(todo,done,limit=n)
tasks = change_format(tasks)
# download
dst = GL_PATH + 'data/input/rnaseq'

for i,row  in tasks.iterrows():

    if not os.path.isfile(dst+'/'+row['FOLDER_NAME']):

        run_downloader(row['UUID'])
        shutil.move('./'+row['UUID']+'/'+row['FOLDER_NAME'], dst)
        shutil.rmtree('./'+row['UUID']+'/')

    command1 = ['submitjob',
                '-m','12',
                'python','app.py',
                '-i',row['FOLDER_NAME'],
                '-a','a']
    print  command1
    p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)
    exit_codes = p1.wait()
    print row['FOLDER_NAME'],row['UUID']
    #print 'submitjob 50 -m 12 python app.py -i {} -a a'.format('FOLDER_NAME')
    print row['FOLDER_NAME'],row['UUID']

# write to submit id
# run


#update donefile
#done_file = pd.concat([done,tasks])
tasks.to_excel(GL_PATH+'todo_tasks.xls',index=False)



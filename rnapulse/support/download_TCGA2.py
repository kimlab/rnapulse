'''
Quick&Dirty script to
Download and keep track of datasets downloaded from cghub

'''
from __future__ import print_function
import subprocess
import pandas as pd
import sys
import shutil
import os


def run_downloader(file_id):
    """Run Downloader data.

    Paramaters
    ----------
    file_id : str
        File id

    """
    # download
    # cghub_bin =  '/home/ccorbi/bin/cghub/bin/gtdownload'
    cghub_bin =  '/home/kimlab2/ccorbi/bin/gdc-client'
    # path_key  = 'https://cghub.ucsc.edu/software/downloads/cghub_public.key'
    path_key = '/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/support/TCGA_mapping/gdc-user-token.txt'

    command1 = [cghub_bin,
                'download',
                file_id,
                '-t',
                path_key]



    print("Downloading: {}".format(file_id))
    p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)

    exit_codes = p1.wait()
    return exit_codes

def get_samples_to_download(data, done,limit=2):
    """Read  control file and map file."""

    # Extract ids from already preprocessed files
    done_uuid = done
    n=0
    results =  list()
    # get N new files
    for i,r in data.iterrows():

        if n >=limit:
            continue

        if r['SOURCE_ID'] in done_uuid:
            pass
        else:

            #results.append(r.to_frame())

            df2 = pd.DataFrame([r.values], columns=data.keys())
            results.append(df2)
            n+=1
    # return
    return pd.concat(results)

def change_format(df, disease):


    df['SAMPLE_TYPE'] = 'PRIMARY TUMOR'
    df['GROUP_ID'] = ''
    df['DISEASE'] = disease
    df['STATUS'] = ''
    df['JOBID'] = ''
    df['DESCRIPTION'] =''

    return df


def main():

    # Working directory
    GL_PATH = '/home/kimlab2/ccorbi/PULSE/Pulse_RNAseq/'
    # download path
    dst = GL_PATH + 'data/input/rnaseq'

    # Control File, what it is done. ToDo: query to the database
    # ctrl_file = GL_PATH + 'ctrl_file.xls'
    # read done
    # done = pd.read_excel(ctrl_file)
    done = ["TCGA-32-5222","TCGA-06-0878","TCGA-28-5204","TCGA-06-1804","TCGA-27-1832",
    "TCGA-28-2513","TCGA-27-2521","TCGA-06-0130","TCGA-06-2557","TCGA-28-2514","TCGA-06-2559",
    "TCGA-06-5414","TCGA-26-5139","TCGA-14-0781","TCGA-19-0957","TCGA-06-0646","TCGA-02-0047",
    "TCGA-06-0744","TCGA-27-1837","TCGA-19-1787","TCGA-76-4928","TCGA-27-2523","TCGA-06-0219",
    "TCGA-06-5417","TCGA-06-5413","TCGA-26-5133","TCGA-15-0742","TCGA-19-4065","TCGA-14-0871",
    "TCGA-76-4932","TCGA-06-5418","TCGA-26-5135","TCGA-28-5207","TCGA-19-2629","TCGA-32-1980",
    "TCGA-06-0649","TCGA-28-5220","TCGA-06-5411","TCGA-14-1034","TCGA-14-1823","TCGA-19-1389",
    "TCGA-32-2615","TCGA-28-5218","TCGA-06-2564","TCGA-32-2634","TCGA-06-2569","TCGA-32-2616",
    "TCGA-14-0790","TCGA-19-1390","TCGA-06-0211","TCGA-06-0152","TCGA-41-2572","TCGA-08-0386",
    "TCGA-06-0125","TCGA-28-1747","TCGA-06-2567","TCGA-27-1831","TCGA-27-1830","TCGA-02-0055",
    "TCGA-06-0644","TCGA-41-3915","TCGA-76-4927","TCGA-28-5213","TCGA-06-2563","TCGA-32-2638",
    "TCGA-06-0238","TCGA-06-2561","TCGA-76-4925","TCGA-27-2519","TCGA-02-2483","TCGA-06-0686",
    "TCGA-28-1753","TCGA-12-3653","TCGA-12-0618","TCGA-06-0178","TCGA-06-0174","TCGA-06-0158",
    "TCGA-14-1402","TCGA-26-1442","TCGA-27-2526","TCGA-27-2528","TCGA-41-4097","TCGA-02-2485",
    "TCGA-14-2554","TCGA-06-0184","TCGA-06-0210","TCGA-06-0221","TCGA-06-0743","TCGA-06-0745",
    "TCGA-06-5410","TCGA-06-5412","TCGA-32-1970","TCGA-2J-AABT","TCGA-IB-7645","TCGA-HZ-A9TJ",
    "TCGA-HZ-A77O","TCGA-US-A77G","TCGA-HZ-A49G","TCGA-IB-A5SO","TCGA-RL-AAAS","TCGA-FB-A4P5",
    "TCGA-F2-A8YN","TCGA-HZ-A77P","TCGA-IB-AAUP","TCGA-F2-6879","TCGA-3A-A9IJ","TCGA-LB-A9Q5",
    "TCGA-HZ-A8P1","TCGA-IB-7890","TCGA-IB-7654","TCGA-FB-AAQ1","TCGA-HV-A7OL","TCGA-S4-A8RM",
    "TCGA-3A-A9J0","TCGA-S4-A8RO","TCGA-HZ-8001","TCGA-HZ-8637","TCGA-HZ-8317","TCGA-S4-A8RP",
    "TCGA-Z5-AAPL","TCGA-2J-AABP","TCGA-FB-AAQ0","TCGA-2J-AABA","TCGA-IB-AAUR","TCGA-HV-A5A5",
    "TCGA-US-A776","TCGA-HZ-A8P0","TCGA-3E-AAAZ","TCGA-US-A77J","TCGA-IB-AAUQ","TCGA-2L-AAQI",
    "TCGA-XN-A8T5","TCGA-YY-A8LH","TCGA-FB-AAPP","TCGA-HZ-8315","TCGA-YB-A89D","TCGA-2J-AABI","TCGA-PZ-A5RE","TCGA-YB-A89D"]

    # Map file from arg
    todo_file = GL_PATH + sys.argv[1]

    # read todo
    todo = pd.read_csv(todo_file, delim_whitespace=True, names=['SOURCE_ID',
                                                                  'UUID_SEQ',
                                                                  'SEQ_FILE_NAME',
                                                                  'UUID_CL',
                                                                  'CL_FILE_NAME'])


    # Get id to download restrict by limit
    n = 120
    tasks = get_samples_to_download(todo,done,limit=n)

    # reformat, add info
    tasks = change_format(tasks, sys.argv[2])
    # for file
    for i,row  in tasks.iterrows():
        # create a folder
        if not os.path.isfile(dst+'/'+row['SOURCE_ID']):
            # Download NGS info from repo
            print('Downloading ... {}'.format(row['SOURCE_ID']))
            run_downloader(row['UUID_SEQ'])
            # Clean data
            shutil.move('./'+row['UUID_SEQ']+'/'+row['SEQ_FILE_NAME'], dst+'/'+row['SOURCE_ID']+'.bam')
            shutil.rmtree('./'+row['UUID_SEQ']+'/')
            # download clin
            run_downloader(row['UUID_CL'])
            # Temporal solution
            # a few clinal data is already on that folder, move fails if already exist
            try:
                shutil.move('./'+row['UUID_CL']+'/'+row['CL_FILE_NAME'], './data/Clinical/')
            except shutil.Error:
                shutil.rmtree('./'+row['UUID_CL']+'/')

        # Run beagle job
        command1 = ['submitjob',
                    '-m','34',
                    'python','app.py',
                    '-i',row['SOURCE_ID']+'.bam',
                    '-a','a']

        # print  command1
        # p1 = subprocess.Popen(command1, stdout=subprocess.PIPE)
        # p1.wait()
        print('{},{}'.format(row['SOURCE_ID'],row['UUID_SEQ']))
        #print 'submitjob 50 -m 12 python app.py -i {} -a a'.format('FOLDER_NAME')
        print('{},{}'.format(row['SOURCE_ID'],row['UUID_SEQ']))

    # write to submit id
    # run


    #update donefile
    #done_file = pd.concat([done,tasks])
    tasks.to_excel(GL_PATH+'todo_tasks.xls',index=False)









if __name__ == '__main__':

    main()

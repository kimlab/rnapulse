'''
the header is parsed all spread the code. Unified in one place to improve managment
    The ID header is generated on the extraction evenets. Preprocess_helpers.py.
    This tag helps not only to identify each events, also keeps information of the event.
    # Map file//events.fa//as_location
    # HEADER> I/E : Transcript id : exons involved coords : C1 len = E len = C2 len

'''

def get_nucleotide_lengths(header):
    '''
    Parameters
    ==========
        :header (str) header
    # Map file//events.fa//as_location
    #          0          1                   2                    3
    # HEADER> I/E : Transcript id : exons involved coords : C1 len = E len = C2 len
    Return
    ------
        :param (list) length of the exons involved

    '''

    snucleotide_lengths = header.split(":")[3].split("=")
    nucleotide_lengths = [int(snucleotide_lengths[0]), int(snucleotide_lengths[1]), int(snucleotide_lengths[2])]
    return nucleotide_lengths

def get_transcriptid(header):

    '''
    Parameters
    ==========
        :header (str) header
    # Map file//events.fa//as_location
    #          0          1                   2                    3
    # HEADER> I/E : Transcript id : exons involved coords : C1 len = E len = C2 len
    Return
    ------
        :param (str) Transcript id

    '''

    return header.split(":")[1].strip()


def get_type(header):


    return header.split(":")[0].strip()




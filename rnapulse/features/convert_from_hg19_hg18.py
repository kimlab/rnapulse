import logging
from subprocess import Popen, PIPE, STDOUT

logger = logging.getLogger(__name__)

def use_remap_api(remap_api_path, mode, remap_from, remap_to, remap_input, remap_output):
    """

    :param remap_api_path:
    :param mode:
    :param remap_from:
    :param remap_to:
    :param remap_input:
    :param remap_output:
    :return:
    """
    command1 = ['perl',
                remap_api_path,
                '--mode',
                mode,
                '--from',
                remap_from,
                '--dest',
                remap_to,
                '--annotation',
                remap_input,
                '--report_out',
                remap_output]

    logger.info("RUN: Remap API: %s", command1)
    p1 = Popen(command1, stdout=PIPE, stdin=PIPE,  stderr=STDOUT)
    exit_code = p1.wait()
    # pass std output to log file
    output = p1.stdout.read()
    logger.info(output.decode("utf-8"))
    return exit_code

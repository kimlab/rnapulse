import logging
from subprocess import Popen
logger = logging.getLogger(__name__)

def start_pfam_scan(pfam_scan_script_location, pfam_input_location, pfam_output_location, hmmer3_data_location):
    command1 = [
                pfam_scan_script_location,
                '-fasta',
                pfam_input_location,
                '-outfile',
                pfam_output_location,
                '-dir',
                hmmer3_data_location,
                '-cpu',
                '1']

    logger.info("RUN: Pfam: %s", command1)
    p1 = Popen(command1)
    exit_codes = p1.wait()
    # pass std output to log file
    #output = p1.stdout.read()
    #logger.info(output.decode("utf-8"))
    return exit_codes

"""used to call iupred and score all the seq in a fasta file.
the ouput is suitable for the features script
"""
from __future__ import print_function
from __future__ import division
import os
import logging
import hashlib
logger = logging.getLogger(__name__)


def hashseqs(row):
    r = row.encode('utf-8')
    return hashlib.md5(r).hexdigest()

def delete_tmp_files(feature_extract_output_path):
    os.system("rm " + feature_extract_output_path + "/temp/*.seq")
    return


def write_tmp_file(content, temp_file, feature_extract_output_path):
    #I/O problem IUPred seems to do not work well with fs-cache
    logger.debug("content: %s", content)
    logger.debug("prot: %s", temp_file)
    logger.debug("folder: %s",feature_extract_output_path)
    write_to = open(feature_extract_output_path + '/temp/{}.seq'.format(temp_file), 'w')

    print("{} seq".format(temp_file), file= write_to)
    print(content, file= write_to)

    return


def exec_iupred(iupred_path, temp_file,  feature_extract_output_path):
    command = ' {}  {}/temp/{}.seq long'.format(iupred_path, feature_extract_output_path, temp_file)
    logger.debug('run %s', command)
    return os.popen(command).readlines()


def save_iupred_out(output, prot_id, output_file):
    for line in output:
        if line[0] == "#":
            pass
        else:
            pos = line[0:5].strip()
            res = line[6:8].strip()
            score = line[13:19].strip()
            binary_score = transform_score(score)
            # logger.debug("IUpred  %s %i %s",prot_id, pos, binary_score)
            print("{}\t{}\t{}".format(prot_id, str(pos), binary_score),
                  file= output_file)
    return


def transform_score(score):
    if float(score) > .5:
        return "."
    else:
        return "*"


def generate_iupred_file(filename, feature_extract_output_path,
                          iupred_install_path, output_location):
    prot_seq = ''
    prot_id = ''
    temp_file = ''
    output_file = open(output_location, 'w')
    with open(filename, 'r') as input_file:
        for line in input_file:
            logger.debug(line)
            if line[0] == ">":
                if len(prot_seq) > 1:
                    write_tmp_file(prot_seq, temp_file, feature_extract_output_path)

                    output_iupred = exec_iupred(iupred_install_path, temp_file, feature_extract_output_path)

                    logger.debug("output IUPRED:  %s", output_iupred )
                    save_iupred_out(output_iupred, prot_id, output_file)
                prot_id = line[1:].strip()
                prot_seq = ''
                temp_file = hashseqs(prot_id)
            else:
                prot_seq += line.strip()
        # Skipe No protein seq. Invalid isoform. Avoid an error if some of them arrive so far
        if len(prot_seq) > 0:
            write_tmp_file(prot_seq, temp_file, feature_extract_output_path)
            output_iupred = exec_iupred(iupred_install_path, temp_file, feature_extract_output_path)
            logger.debug("S.. output IUPRED:  %s", output_iupred )
            save_iupred_out(output_iupred, prot_id, output_file)

    delete_tmp_files(feature_extract_output_path)

""" creates query file for converting between hg19 and 18
"""
from __future__ import print_function
from __future__ import division

import logging
logger = logging.getLogger(__name__)

def create_query_file(as_location_file, conservation_query_output_location):
    as_location = open(as_location_file, 'r')
    write_to = open(conservation_query_output_location, 'w')

    for line in as_location:
        tokens = line.split()
        chromosome = tokens[0]
        start = tokens[1]
        end = tokens[2]
        # asid = tokens[3]
        # type = tokens[4]
        # strand = tokens[5]

        output = chromosome + ":" + start + "-" + end
        print(output, file= write_to)

    write_to.close()

"""Combines all our feature tables into one huge feature table ready to be read by R
Also generates some simple features not found in other feature tables
"""
from __future__ import print_function
from __future__ import division
from collections import OrderedDict
import logging

from rnapulse.support import parsing_header

logger = logging.getLogger(__name__)

def read_features( stream, duality):

    features_map = dict()
    for line in stream:
        tokens = line.split()
        if duality:
            key = tokens[0] + "//" + tokens[1]
            rest = tokens[2:len(tokens)]
            features_map[key] = rest
        else:
            key = tokens[0]
            rest = tokens[1:len(tokens)]
            features_map[key] = rest

    return features_map

def obtain_feature(mapp, key, n_rows, feature):
    new_list = ["0.0"] * n_rows

    try:
        if key in mapp:
            new_list = mapp[key]
    except:
        logger.debug("missing %s for %s",feature, key)
        pass

    return new_list


def generate_machine_learning_matrix( config):

    all_features = dict()

    name_features = OrderedDict({'CORE_FEATURES':(4,True),
                                 'DISORDER_FEATURES': (6,True),
                                 'PFAM_FEATURES': (7, True),
                                 'ELM_FEATURES':(4, True),
                                 'MUT_FEATURES':(4, True),
                                 'PTM_FEATURES':(4, True),
                                 'TM_FEATURES':(4, True),
                                 'NETWORK_FEATURES':(1, False),
                                 'SEQ_CONSERVATION_FEATURES':(3, False),
                                 'EVNT_CONSERVATION_FEATURES':(4, False)})
    # ML_AS_EVENT_CLASSIFICATION':(1, False)


    write_all = open(config["FEATURES_ML"], 'w')
    write_all_names = open(config["NAMES_ML"], 'w')

    try:
        map_file_obj = open(config['ANCHOR_EVENT_INIDEX_MAP'], 'r')
    except:
        logger.error('ERROR: Index file do not found')
        logger.error(map_file_obj)

    for f, info in name_features.items():
        try:
            read_f = open(config[f], 'r')
        except:
            if 'CONSERVATION_FEATURES' in f:
                all_features[f] = dict()
            else:
                logger.error('ERROR: Features File Do not Found')   
                logger.error(config[f])
                all_features[f] = dict()

        
        feature_values = read_features(read_f, info[1])
        logger.debug(feature_values)
        all_features[f] = feature_values



    output = merge_features_by_index(map_file_obj,all_features, name_features)


    # Write Matrix header
    header = "frameshift,spliceLength,lenDiff,nlenDiff,lenAfter,coreC1,coreA,coreC2,canCore,disorderC1," \
             "disorderA,disorderC2,canDisorder,otherDisorder,otherADis,domainC1,domainA,domainC2,canDomain," \
             "otherDomain,otherADom,enzymatic,elmC1,elmA,elmC2,canElm,mutationC1,mutationA,mutationC2," \
             "canMutation,ptmC1,ptmA,ptmC2,canPtm,transmemC1,transmemA,transmemC2,canTransmem,degree," \
             "seqConAve,seqConMin,seqConMax,classifier,spSpec,consAS,gspec"

    print("IE, {}".format(header), file=write_all)
    
    # Write Matrix features
    for iso in output:
 
        print(iso[0], file=write_all_names)
        print(iso[1], file=write_all)

    write_all_names.close()
    write_all.close()  



def merge_features_by_index(map_file_obj,all_features, name_features):

    output = list()


    # Matrix generation
    for line in map_file_obj:
        # READ HEADER ID
        tokens = line.split()
        asid = tokens[0]
        prot = tokens[1]
        # MAP COORD
        sstart = int(tokens[2])
        start = int(tokens[3])
        end = int(tokens[4])
        eend = int(tokens[5])
        # LENGTH
        canonical_length = int(line.split("\t")[7].strip())
        other_length = int(line.split("\t")[8].strip())

        temp_line = line.split("\t")[6].strip().replace("[", "").replace("]", "").replace("'", "").replace(" ", "")

        list_other = []
        if temp_line != "":
            list_other = temp_line.split(",")

        # check the list to see if it's uniprot (not empty), or positive and add the
        # line to print which we get eventually.
        # at the end print these in order.

        dual_key = asid + "//" + prot
        single_key = asid
        # print 'getting features'
        # obtain feature line from various feature files.
        features_line = list()


        for f, info in name_features.items():

            logger.debug("PIPELINE: Parsing %s Features for %s", f, asid)
            if info[0]:
                features_line.extend(obtain_feature(all_features[f], dual_key, info[0], f ))
            else:
                features_line.extend(obtain_feature(all_features[f], single_key, info[0], f ))


        len_diff = canonical_length - other_length
        n_len_diff = (1.0 * len_diff) / canonical_length
        len_after = other_length - start

        # This is ready for the default format label splicing plus lenght of the exons -> X.LABEL-###_33=33=33
        length_a_exon = parsing_header.get_nucleotide_lengths(asid)[1]
        # length_a_exon = int(tokens[0].split("-")[-1].split("=")[1])

        # I/E exon could promote a frameshift
        frameshift = 1
        if length_a_exon % 3 == 0:
            frameshift = 0
 
        feature_line = repr(frameshift) + ", " + repr(length_a_exon) + ", " + repr(len_diff) + ", " + \
                       repr(n_len_diff) + ", " + repr(len_after) + ", " + ",".join(features_line) 

        # Split events, inclusion, exclusion. Debugging purposes.
        event_type = parsing_header.get_type(asid)
        if event_type == "I":
            feature_line = '1, {}'.format(feature_line)
            output.append([dual_key, feature_line])

        if event_type == "E":
            feature_line = '0, {}'.format(feature_line)
            output.append([dual_key, feature_line])

    return output
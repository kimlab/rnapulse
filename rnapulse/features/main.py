from __future__ import print_function
from __future__ import division
import time
import sys
import os
import logging


from .uniprot_ptm import get_postranscriptional_modification_features
from .uniprot_transmem import get_transmembrane_region_features
from .uniprot_elm_read import get_uniprot_elm_features
from .generate_iupred_file import generate_iupred_file
from .uniprot_disorder import get_uniprot_disorder_features
from .uniprot_domain_read import get_uniprot_domain_read
from .run_pfam_scan import start_pfam_scan
from .uniprot_core import get_sable_scores
from .mutation_features import get_mutation_features
from .conservation_conversion_query import create_query_file
from .convert_from_hg19_hg18 import use_remap_api
from .seq_conserv import generate_sequence_conservation_features
from .event_conserv import generate_event_conservation_feature_table
from .generate_ml_input import generate_machine_learning_matrix
from .network_features import generate_network_features
from rnapulse.support.utils import normalize_unicode_data

logger = logging.getLogger(__name__)


def naive_filter(config):
    """Remove events that would be likly removed by the spliciosoma.

    Parameters
    ----------

    Returns
    -------

    """
    to_ignore = list()
    # no need to score sequences
    # NMD, too short or without UTR or STOP codon.
    with open(config['CDNA'],'r') as input_file:
        for line in input_file:
            if line.startswith('>'):
                temp_id = line[1:].strip()
            else:

                if len(line.strip()) < 60:
                    to_ignore.append(temp_id)

                    logger.warning("Remove Short  event:\t\t%s", temp_id)
                    logger.warning("Remove Seq:\t\t %s", line.strip())
                if line.strip()[:3] != 'ATG':

                    to_ignore.append(temp_id)
                    logger.warning("Remove event without UTR :\t\t%s", temp_id)
                    logger.warning("Remove Seq:\t\t %s", line.strip())

                if line.strip()[-3:] not in  ['TAG', 'TAA','TGA']:

                    to_ignore.append(temp_id)
                    logger.warning("Remove event without STOP codon:\t\t%s", temp_id)
                    logger.warning("Remove Seq:\t\t %s", line.strip())


    # read index
    f = open(config['ANCHOR_EVENT_INIDEX_MAP'])
    data = f.readlines()
    f.close()
    # overwrite new version, filtered
    overwrite = open(config['ANCHOR_EVENT_INIDEX_MAP'], 'w')
    for l in data:
        fields = l.split('\t')
        if fields[0] in to_ignore:
            pass
        else:
            print(l.strip(), file=overwrite)

    overwrite.close()

    return


def feature_extraction(config):
    """One liner description.

    Parameters
    ----------

    Returns
    -------

    """
    logger.info("OUTOUT  %s ", config['FEATURES_OUTPUT'])

    #########################
    # TRANSMEMBRANE SCORING #
    #########################

    logger.info("STEP: Now getting transmembrane region features...")

    uniprot_tm_indices_db_location = normalize_unicode_data(config["F_UNIPROT_TRANSMEM_INDICES_LOCATION"])

    # Extract features
    get_transmembrane_region_features(config['ANCHOR_EVENT_INIDEX_MAP'],
                                      uniprot_tm_indices_db_location,
                                      config['TM_FEATURES'])

    logger.info("DONE")

    ################
    # PTM FEATURES #
    ################

    logger.info("STEP: Now getting post-transcriptional modifications...")
    # Setup  folders
    uniprot_ptm_db_location = normalize_unicode_data(config["F_PTMS_LOCATION"])

    # Extract features
    get_postranscriptional_modification_features(config['ANCHOR_EVENT_INIDEX_MAP'],
                                                 uniprot_ptm_db_location,
                                                 config['PTM_FEATURES'])

    logger.info("DONE")

    ###################################
    # EUKARYOTIC LINEAR MOTIF SCORING #
    ###################################

    logger.info("STEP: Now getting eukaryotic linear motif scores...")
    # Setup  folders
    # uniprot_elm_db_location = normalize_unicode_data(config["F_ELM2_LOCATION"])

    get_uniprot_elm_features(config['ANCHOR_EVENT_INIDEX_MAP'],
                             config["F_ELM2_LOCATION"],
                             config['ELM_FEATURES'])

    logger.info("DONE")

    #################################
    # DISORDEROME PUTATIVE ISOFORMS #
    #################################

    logger.info("STEP: Now running helper files for disorderome...")
    # Setup  folders

    # iupred_install_path = normalize_unicode_data(config["IUPRED_BIN"])
    # Extract features
    generate_iupred_file(config['PSEQ'],
                         config['FEATURES_OUTPUT'],
                         config["IUPRED_BIN"],
                         config['DISORDER_REFERENCE'])


    logger.info("DONE")

    ###################################
    # DISORDEROME UNIPROT &  FEATURES #
    ###################################

    logger.info("STEP: Now getting disorderome features...")
    # Extract features
    get_uniprot_disorder_features(config['ANCHOR_EVENT_INIDEX_MAP'],
                                  config['DISORDER_REFERENCE'],
                                  config['UNIPROT_FEATURES_DDBB'],
                                  config['DISORDER_FEATURES'])

    # is_non_zero_file(disorder_read_out_location)
    logger.info("DONE")

    ##########################
    # PFAM & DOMAIN FEATURES #
    ##########################
    try:

        logger.info("Step: Now running pfam_scan...")

        # Extract features
        pfam_exit_code = start_pfam_scan(config["PFAM_SCAN_BIN"],
                                         config['PSEQ'],
                                         config['PFAM_REFERENCE'],
                                         config["HMMER_PFAM_DATA_LOCATION"])
        logger.info("PIPELINE: DONE")

    except:
        logger.error("ERROR: pfam_scan failed")
        logger.error(pfam_exit_code)
        logger.error(sys.exc_info())


    logger.info("STEP: Now getting uniprot domain features...")
    # Setup  folders
    # f_pfam_special_db_location = normalize_unicode_data(config["F_PFAM_SPECIAL_LOCATION"])

    # Extract features
    get_uniprot_domain_read(config["F_PFAM_SPECIAL_LOCATION"],
                            config['UNIPROT_FEATURES_DDBB'],
                            config['ANCHOR_EVENT_INIDEX_MAP'],
                            config['PFAM_REFERENCE'],
                            config['PFAM_FEATURES'])

    logger.info(" DONE")

    # is_non_zero_file(config['PFAM_FEATURES'])


    #################
    # SABLE SCORING #
    #################

    logger.info("Step: Now getting features for SABLE...")
    # Setup  folders
    # f_sable_db_location = normalize_unicode_data(config["F_SABLE_LOCATION"])
    # config['CORE_FEATURES'] = config['FEATURES_OUTPUT'] + '/core_read.out'

    get_sable_scores(config['ANCHOR_EVENT_INIDEX_MAP'],
                    config["F_SABLE_LOCATION"],
                    config['CORE_FEATURES'])

    logger.info("DONE")


    ####################
    # MUTATION SCORING #
    ####################


    logger.info("STEP: Now getting mutation scores...")
    # Setup  folders
    #f_mutations_db_location = normalize_unicode_data( config["F_MUTATIONS_LOCATION"])
    #config["MUT_FEATURES"] = config['FEATURES_OUTPUT'] + '/mutation_read.out'
    # Extract features
    get_mutation_features(config['ANCHOR_EVENT_INIDEX_MAP'],
                          config["F_MUTATIONS_LOCATION"],
                          config["MUT_FEATURES"])

    logger.info("DONE")

    #################################
    # CONSERVATION/NETWORK FEATURES #
    #################################

    try:
        logger.info("STEP: Conservation Features")
        logger.info("Creating query file conservation/network features...")
        # Setup  folders
 

        create_query_file(config['AS_LOC'] , config["CONSERVATION_REFERENCE"])
        logger.info("Query Created")
        logger.info("Converting between hg19 to hg18 using the query file generated...")
        # Setup  folders
        # Extract features
        remap_exit_code = use_remap_api(config['REMAP_HG_API_BIN'],
                                        config["REMAP_MODE"],
                                        config["REMAP_FROM"],
                                        config["REMAP_TO"],
                                        config["CONSERVATION_REFERENCE"],
                                        config["CONSERVATION_REPORT"])

        # Extract features
        generate_sequence_conservation_features(config["F_PHASTCONS_HG18_BED_LOCATION"],
                                                config['AS_LOC'] ,
                                                config['CONSERVATION_REPORT'],
                                                config['SEQ_CONSERVATION_FEATURES'])
    #logger.info("DONE")

        logger.info("STEP: Now generating events conservation feature table...")

        # Extract features
        generate_event_conservation_feature_table(config["F_PHASTCONS_HG18_BED_LOCATION"],
                                                  config["F_EVENT_CONSERVATION_LOCATION"],
                                                  config["AS_LOC"],
                                                  config['CONSERVATION_REPORT'],
                                                  config['EVNT_CONSERVATION_FEATURES'])
        logger.info("EVENTS CONSERVATIONS DONE")
    except:
        logger.warning("Warning: Remaping failed, Skipping this feature")
        pass
    
    logger.info("STEP: Now generating network features using gene names...")
    # Setup  folders

    config['NETWORK_FEATURES'] = config['FEATURES_OUTPUT'] + '/degree_read.out'
    # Extract features
    generate_network_features(config["F_UNIPROT_GENEWIKI_LOCATION"],
                               config["F_DEGREE_LOCATION"],
                              config['ANCHOR_EVENT_INIDEX_MAP'],
                              config['NETWORK_FEATURES'])
    logger.info("FEATURES: DONE")

    return


def generate_features_matrix(config):

        ###############################
        # GENERATE MATRIX OF FEATURES #
        ###############################
    logger.info("Now put every feature in a single vector to feed PULSE..")


    generate_machine_learning_matrix(config)

    logger.info("DONE")

from __future__ import print_function
from __future__ import division

import logging
from .features_helpers import score_differences

logger = logging.getLogger(__name__)

def get_transmembrane_region_features(uniprot_exon_indices_location, uniprot_tm_indices_db_location,
                                      output_file_location):
    """
    Reads uniprot TM indices DB and generates transmembrane scores.

    :param uniprot_exon_indices_location:
    :param uniprot_tm_indices_db_location:
    :param output_file_location:
    :return:
    """
    #print(uniprot_exon_indices_location)
    #print(uniprot_tm_indices_db_location)
    # print(output_file_location)

    read_from = open(uniprot_exon_indices_location, 'r')
    #print uniprot_exon_indices_location
    write_to = open(output_file_location, 'w')
    #print output_file_location
    uniprot_to_index_to_whatever = {}

    uniprot_tm_indices_db = open(uniprot_tm_indices_db_location, 'r')
    #print uniprot_tm_indices_db_location

    for line in uniprot_tm_indices_db:
        tokens = line.split("\t")
        logger.debug(tokens)
        try:
            uniprot = tokens[0].strip()
            start = int(tokens[2].strip())
            end = int(tokens[3].strip())
            for i in range(start, end + 1):
                if uniprot in uniprot_to_index_to_whatever:
                    uniprot_to_index_to_whatever[uniprot][i] = "*"
                else:
                    uniprot_to_index_to_whatever[uniprot] = {i: "*"}
        except ValueError:
            logger.warning("Cannot parse: %s", line[0:len(line) - 1])

    for line in read_from:
        tokens = line.split()
        # PARSING ID
        logger.debug(tokens)
        asid = tokens[0].split("_")[0]
        prot = tokens[1]

        # PARSING ID
        sstart = int(tokens[2])
        start = int(tokens[3])
        end = int(tokens[4])
        eend = int(tokens[5])

        c1_count = 0
        a_count = 0
        c2_count = 0
        canonical_absolute = 0

        if prot not in uniprot_to_index_to_whatever:
            c1_count = 0
            a_count = 0
            c2_count = 0
            canonical_absolute = 0
            other_absolute = 0
            prot_len = 0

        else:
            c1_count = score_differences(uniprot_to_index_to_whatever, prot, sstart, start)
            a_count = score_differences(uniprot_to_index_to_whatever, prot, start, end)
            c2_count = score_differences(uniprot_to_index_to_whatever, prot, end, eend)
            prot_len = int(line.split("\t")[7].strip())
            canonical_absolute = score_differences(uniprot_to_index_to_whatever, prot, 1, prot_len)

        # logger.debug(tokens[0],c1_count,a_count,c2_count,prot_len,canonical_absolute)
        print(tokens[0] + "\t" + prot + "\t" + repr(c1_count) + "\t" + repr(a_count) + "\t" + repr(
            c2_count) + "\t" + repr(canonical_absolute), file=write_to)

from __future__ import print_function
from __future__ import division

import logging
import os
import sys
import sqlite3

logger = logging.getLogger(__name__)


def score_differences(mapping, uniprot, start, end):
    count = 0
    try:
        if uniprot in mapping:
            if start <= end:
                for i in range(start, end + 1):
                    if i not in mapping[uniprot]:
                        pass
                    elif mapping[uniprot][i] == "*":
                        count += 1
                return (1.0 * count) / (end - start + 1)

    except KeyError as e:
        logging.warning("WARNING!: Features not found %s ", e)
        logging.warning("Please check ID tags or/and generate the missing features")
    return count


def score_differences_pfam(mapping, uniprot, start, end):
    try:
        count = 0
        enzymatic = 0
        if uniprot in mapping:
            if start <= end:
                for i in range(start, end + 1):
                    if i not in mapping[uniprot]:
                        pass
                    elif mapping[uniprot][i][0] == "*":
                        if mapping[uniprot][i][1]:
                            enzymatic = 1
                        count += 1
                return [(1.0 * count) / (end - start + 1), enzymatic]
    except KeyError as e:
        logging.warning("WARNING!: Features not found %s ", e)
        logging.warning("Please check ID tags or/and generate the missing features")
    return [count, 0]


# Used in features.uniprot_disorder
def link_db(db_path):
    try:
        db = sqlite3.connect(db_path)
    except sqlite3.Error as errmsg:
        logger.error("DB not available at: %s", str(errmsg))
        sys.exit()
    db_cursor = db.cursor()
    return db_cursor, db

from setuptools import setup, find_packages

requirements = [
            'pandas',
            'numpy',
            'biopython',
            'scipy',
            'pyyaml',
            'pytest',
            'sqlalchemy',
            'scikit-learn',
            'tqdm',
            'dill',
            'tqdm',
            'pandas',
            'pyyaml',
            'dill',
            'scipy']


setup(
  name = 'rnapulse',
  packages=  find_packages(),
  package_data={
                'rnapulse.support': ['rebuild/*.py'],
                'rnapulse.support': ['params/*.inp'],
                'rnapulse':['*.yml'],
                },


  version = '0.1.1',
  description = 'Pipeline to score RNAseq data with Classifier trained in a nontradicional dataset',
  author = 'Carles Corbi-Verge',
  author_email = 'carles.corbiverge@utoronto.ca',
  url = 'https://gitlab.com/kimlab/rnapulse',
  download_url = 'https://gitlab.com/kimlab/rnapulse',
  keywords = ['mRNAseq', 'Isoforms', 'pipeline', 'predictor'], # arbitrary keywords
  include_package_data=True,
  install_requires=requirements,
  entry_points={
          'console_scripts': [
              'rnapulse = rnapulse.__main__:main'
          ]
  },
  classifiers=[
       "License :: OSI Approved :: MIT License",
       "Programming Language :: Python :: 3",
       "Topic :: Scientific/Engineering :: Bio-Informatics",
       ],
  license='MIT',



)


# RNApulse

The immense majority of genes are alternatively spliced and there are many isoforms specifically associated with cancer progression and metastasis.
The splicing pattern of specific isoforms of numerous genes is altered as cells move through the oncogenic process towards a more aggressive invasive cancer phenotype. Moreover, splicing factors might be deregulated in cancer, and in some cases, aberrant isoform might appear and drive aspects of the disease progression. Target those specific cancer isoforms might provide a novel and less toxic therapeutic molecules.


However, detect and characterizes those isoforms is a particularly challenging goal. Here we present an RNAseq data preprocess pipeline to identify potential novel isoforms associated with cancer progression from mRNA-seq data from each primary phenotype found on The Cancer Genome Atlas (TCGA).  Therefore, filter and analyze the predicted isoforms with a better score and strongly associated with death and cancer malignant progression. We score all potential isoforms present on the mRNAseq data of TCGA samples. Our methodology takes advantage of an algorithm to predict the likelihood of yielding stably folded protein isoforms from exon skipping AS events (PULSE). PULSE leverages 48 predictive features from the splicing event, achieving an AU-ROC value of 0.85, suggesting that it outputs high probability values for functional isoform [Hao et al., 2015](http://www.cell.com/cell-reports/pdf/S2211-1247(15)00643-9.pdf).

These results might help to identify new genetic biomarkers associated with cancer risk, prognosis and response to standard treatment. Consequently, we are requesting access to the controlled sequencing data on TCGA. Findings will be made public, but as agreed, the data will be secured, confidential, not transferred, properly acknowledged and won’t allow identification.

You can find a paper like documentation at  ```./docs```


*NOTE: All scripts are currently in early development and are not guaranteed to function properly. Some knowledge of shell scripting and, Linux systems will be of use.

![workflow](./docs/rnapulse_workflow.png)


# Table of Contents

  * Installation
  * Dependencies
    * Python packages
    * Third party software
    * Optional
  * Run
    * Example
  * Configuration & Usage
  * Pipeline Description
    * Steps
    * Ouput folder architecture
    * Mining AS events
    * Features
  * Additional Notes
    * Building your custom references
    * Retrain models
    * Other features
  * Contributing
  * Authors
  * License
  * Acknowledgments

# Installation

Anaconda installation is recommended, however, you can clone the package and use it as you want. Also is recommended to use this pipeline on a Linux system with at least 25Gb of free space, but they will work on OSX as well. RNApulse will download all the precalculated features, models and databases needed to score the isoforms. Those have been optimized to work with Human, any other species may work, however the results may be not optimal and complete retrain of models and features is recommended. 


```bash

conda config --add channels bioconda --add channels conda-forge --add channels kimlab

conda install rnapulse -c kimlab 

```

Next, install [IUpred](http://iupred.enzim.hu)  if is not already installed in your system, and [StringTie](https://ccb.jhu.edu/software/stringtie/) if you are running RNApulse on OSX. 


# Dependencies

 * 25 Gb of free space
 * Python 2.7 or Python 3.x
 * Perl 5.14.2 or later


 **Python Requirements**

   - Biopython
   - tqdm
   - sqlalchemy
   - pandas
   - pytest
   - scikit-learn
   - numpy
   - pyymal
   - scipy
   - dill


**Third Party Software**

  All these software, except by IUpred, will be installed from bioconda automatically. Remember to install IUpred and add the binary to the path. stringTie on _OSX_,  must be manually installed either.


* [StringTie](https://ccb.jhu.edu/software/stringtie/). Software for Assembly Transcriptomes.

* [BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download) is required in the preprocessing step after alternative splice site extraction

* [pfamscan](http://www.vcru.wisc.edu/simonlab/bioinformatics/programs/install/pfamscan.htm) is required to domain detection

*  [iupred](http://iupred.enzim.hu) disorderome scoring step of feature extraction step requires.


**Optional Software**

This software is no need for running RNApulse, however, it can be useful if you want to retrain the model. 
  
* [GDC Data Transfer Tool](https://gdc.cancer.gov/access-data/gdc-data-transfer-tool). Software for Downloading Data from TCGA.

* [Sable]http://sable.cchmc.org/().  Software for accurate sequence-based prediction of relative solvent accessibilities. 

# Run

The first run after the installation download all the supporting files to properly assign features to the isoforms, and precalculated data and models.  The running time is proportional to the size of the .bam file and the number of cores. A ~5Gb .bam file in 8 cores run for ~ 3h. 
The name of the file will be used as a sample id, however, there is no need to add the file extension (.bam), RNApulse will ignore the extension if it is provided but I recommend do not provide it. All the output data does dump into a single folder, (by default the folder were RNApulse is executed ```./output```). However this value can be changed using an argument, -o and  if you need it, this and all the other parameters can be modified providing your own configfile (see below for more details)

```bash

rnapulse run -i name_raw_file  
```
## Examples

There are many open data to apply RNApulse, you can download bam files from different repositories, see below two major examples. As a test of the pipeline, you can download as test example this file [ENCFF993VOO](https://www.encodeproject.org/files/ENCFF993VOO/)


 - [TCGA](https://portal.gdc.cancer.gov/repository)

 - [ENCODE](https://www.encodeproject.org/search/?type=Experiment)


```bash

rnapulse run -i ENCFF993VOO

```

# Configuration & Usage

The default configuration should fit the majority of users.  However, you can update any of the default parameters by writing them into a file in yaml format. You must use the `config.yml` as a template and change only those values you want to customize. Parameters as the 



```bash

rnapulse run -i name_raw_file  --config_file my_config file
```


```bash
rnapulse --help

usage: rnapulse [-h] [-i SAMPLE_ID] [-o WORK_FOLDER] [--rst RST [RST ...]]
                [--only ONLY [ONLY ...]] [--config_file CONFIGFILE]
                [--logging LOGGING] [--cpus CPUS] [--retrain_models]
                {run,update}

RNAseq pipeline for PULSE Launcher Usage: 
  rnapusle [update, run] -i path/sample

Run `rnapulse -h` for help information

positional arguments:
  {run,update}          main pipeline action to be performed by the pipeline

optional arguments:
  -h, --help            show this help message and exit

  -i SAMPLE_ID, --input SAMPLE_ID
                        BAM filename or/and Sample id after preprocess
  -o WORK_FOLDER, --output WORK_FOLDER
                        Folder contains all the output data, by
                        default executing/folder/output/
  --rst RST [RST ...]   Restart specified steps to restart pipeline. assembly,
                        preprocess, mapping, anchoring, features, scoring.
                        A step will fail if any required output of a previous
                        step is missing
  --only ONLY [ONLY ...]
                        Run only specified steps for this sample assembly,
                        preprocess, mapping, anchoring, features, scoring. A
                        step will fail if any required output of a previous
                        step is missing
  --cpus CPUS           Max number of Cores available for the pipeline
                        (default 2)

  --config_file CONFIGFILE
                        Path and configuration variables in json format, by
                        default config.yml in the installation folder
  --logging LOGGING     Setup the verbose intensitive of the log file, by
                        default this parameter is set to "info", change it to
                        "debug" if it needs it.

  --retrain_models      RNApulse already comes with 5 train models ready to
                        go. However, some user may what to force to train the
                        models during the run. This is not recommended without
                        a good reason; the pipeline will be sensible slower
                        and the scoring values may change for the same event
                        in different runs.


```


options ```--rst``` and ```--only```  defines which step of the pipeline skip or exclusive run, respectively. (below more details of each step). Restarting flags are assembling, preprocessing, anchoring, mapping, featuring, and scoring. (full description below)



# Pipeline Description

![pipeline](./docs/mRNA-Pulse-diagram.png)


## STEPS 

_assembling_

1. Ensemble the transcriptome allowing de novo assembly


_preprocessing_

1. Find exon-skipping alternative splicing events from transcriptome.
2. Generate an index file to locations of alternative splicing events.


_anchoring & mapping_

1. Identify anchors for alternative splicing events using BLAST.
2. Map to the anchor and get the AminoAcid sequences of the Spicing Form.
3. Filter out alternative splicing events without a good anchor.


_featuring_

1. Transmembrane scoring.
2. Post-transcriptional modification scoring.
3. Eukaryotic linear motif scoring.
4. Disorderome scoring.
5. Domain scoring using PFAM.
6. Sable scoring.
7. Mutation scoring.
8. Conservation/network features generation.


_scoring_

Five iterations of random forest classifier - takes the average of all iterations to generate final votes from ensemble and importance of each feature. To speed up the classification, the random forest classifiers have been precalculated. there is the option to refresh the models if is need it. 

TRAINING_DATASET: '{RNAPULSE_PATH}/support/params/ML_Training_data.inp'
TRAINING_DATASET_TP: '145'
MODELS: '{RNAPULSE_PATH}/support/models/RF_model_*.pkl'
FAST_MODELS: True
NUM_ITER: '5'


## Default Output Folder Architecture

 This schema allows keeping all the data from multiple bam files in the same folder. This is especially handy to process data from the same source or study (i.e same tissue). Data under this schema can be parsed and added to a database. 

```
output/
├── assembly/
    ├── Name_sample_1
    ├── ...
    └── Name_sample_N

├── preprocess/
    ├── Name_sample_1
    ├── ...
    └── Name_sample_N

├── features/
    ├── Name_sample_1
    ├── ...
    └── Name_sample_N

├── score/
    ├── Name_sample_1
    ├── ...
    └── Name_sample_N
```



## Mining of AS events


The alternative splicing (AS) events in the Illuminas' Body Map project analyzed by PULSE came from ad hoc file. This file contained the coordinates of exons involved in AS events in a tab separated files with the coordinates for the human genome 18  (Locus_ID Locus_ID Chromosome strain As Ae C1s C1e C2s C2e).  This file was processed with custom scripts. RNApulse extract all the AS events detected in the RNAseq sample after resemble after consider all the exons skipping events detected for each gene, similar methodologies have been applied before (similar to https://github.com/vastgroup/vast-tools). RNApulse extract all the AS events detected for each assembled gene by compareing all the transcripts exon configurations, filtering away exons with FPKM below a threshold, this threshold can be setup by users in the configuration file. ```FPKM_THRESHOLD```


## Features

1. **Structural  Features:**  This  category  captures  statistics  relating  to  how  much  an  isoform  deviates from its canonical pair in terms of overlap with globular domains, disorder rate, trans-membrane structures,  and  enzymatic  sites.  Ratios  are  calculated  for  the  alternative  exon  A as  well  as  the constitutive  flanking  exons  C1  and  C2  and  over  the  canonical  isoform  as  a  whole.  This  is because,  as  we  showed  in  our  previous  studies,  C1  and  C2  contain  a  lot  of  important  signals relating  to  regulation  and  functional  effects  of  splicing  such  as  rewiring  of  protein  interactions (Çolak et al., 2013; Ellis et al., 2012). We derived domain annotations using Pfam HMM profiles and  the  pfamscan  tool (Finn  et  al.,  2013).  Disorder  predictions  are  performed  with  IUPRED (Dosztányi  et  al.,  2005).  Our  motivation  to  include  disorder  was  that  highly  disordered  regions has  been  shown  to  be  a  hot  spot  for  regulatory  regions  and  tissue  specific  alternative  splicing, which is a common mechanism to overload proteins with tissue specific functions (Buljan et al., 2013;  Ellis  et  al.,  2012).    In  addition,  we  annotated  trans-membrane  region  boundaries  from Uniprot,  since  they  are  critical  for  protein  localization  at  the  cell  membranes.  Their  disruption can  change  the  protein  isoform  location  and  function,  for  which  the  IL-4  isoform  is  a  good example (Blum et al., 1996), or directly become insoluble and dysfunctional. Finally, enzymatic domains were identified and labeled based on a keyword search of `catalytic` or `enzy*` in Pfam descriptions to distinguish them from modular protein-protein interaction domains. Their activity can  be  severely  affected  by  splicing  events;  activities  can  be  affected  by  affinity,  substrate specificity,  and  catalytic  properties.  Nevertheless,  some  inactive  variants  have  been  found producing  a  dominant  negative  effect  over  the  catalytically  active  form (Li  and  Koromilas, 2001), although most of them may lead to non-functional isoforms.

2. **Splicing  Features:**  This  category  mostly  focuses  on  statistics  characterizing  the  direct  effects  of splicing  on  the  alternative  isoform.  It  includes  basic  information  about  frame  shift,  early  stop codons,  and  total  length  change.  Moreover,  we  also  use  the  tissue  specificityinformation; although, it has been reported as an indicator of functionality in several previous works (Bellay et al., 2012), it was never analysed in the context of FID. Indeed recent work suggests that AS is, compared  to  highly  conserved  gene  expression,  weakly  conserved  even  across  close  species (Barbosa-Morais et al., 2012; Merkin et al., 2012).

3. **Evolutionary  Features:** We also included sequence conservation scores obtained from PhastCon 
(Siepel  and  Haussler,  2005).  However,  unlike  previous  approaches,  we  not  only  focused  on sequence conservation but also on the conservation of splicing itself, which became feasible only recently  due  to  large  scale  cross-species  RNA-Seq  experiments (Barbosa-Morais  et  al., 2012; Merkin  et  al.,  2012). Conserved  sequences  tend  to  indicate  important  protein  functional segments,  thus  removing  conserved  sequences  through  exon  skipping  can  be  detrimental  to isoform functionality. We also included signals related to advanced evolutionary and regulatory aspects  of  conservation  of  the  splicing  event  itself:  genome  specificity,  species  specificity,  and classification  ability  of  the  event.  These  advanced  features  were  derived  from  in  depth  analysis of  cross-species  splicing  analysis  from  our  previous work (Barbosa-Morais  et  al.,  2012). Genome-specific  events  include  alternative  exons  that  could  not  be  found  in  other  species,  not even as constitutive events. The classifiers events’ alternative exon, on the other hand, exists in the several species but have different typical inclusion levels in different  species. We also have the  species-specific  AS  exons  that  are  alternative  in  only  one  species  but  not  necessarily  stably across all tissues.

4. **Regulatory  Features:**  This  category  focuses  on  overlap  with  regulatory  hot  spot  regions.  It includes  Eukaryotic  Linear  Motif  (ELM),  which  are  crucial  for  domain-peptide  interactions,  as well  as  Post  Translation  Modification  (PTM)  rates  and  disease  mutation  occurrence  rate.  ELM predictions  were  obtained  from SLiMSearch  2.0(Davey  et  al.,  2011)
and PTMs  were  obtained from  Uniprot. Changes  in  regulatory   element  density  can  be  an  indicator  of  change  in functionality. Mutation hotspots are also good indicators of regions important for functionality.

5. **Proteomic  Features:**  This  categorizes  protein  network  interaction  degree  measures  as  well  as disease mutation occurrence obtained from Cosmic somatic cancer mutation database 
(Flicek et al.,  2011).  Network  degree  was  derived  from  the  BioGrid  human  protein  interaction network (Chatr-Aryamontri  et  al.,  2013).  Our  reasoning  was  that  hub  proteins  might  require  many positive  splicing

# Additional Notes

## Build your custom references

All the references files are optimized to work with human RNAseq data, if you need to apply this pipeline to another organism, then is necessary to change the references files and also is advised to retrain the models. Please use the scripts on the rnapulse/support/rebuild to regenerate the reference and supporting files in the right format. 


* Reference Genome

    1. Download Fasta files from your new reference genome
        ```bash
         rsync -av rsync://ftp.ensembl.org/ensembl/pub/release-70/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.70.dna.chromosome.*.fa.gz .   ```
    2. Generate binary file
        ```bash
        python  rebuild_refgen.py /dna/Homo_sapiens.GRCh37.70.dna.chromosome.
        ```
    3. Do not forget to download a corresponding GTF (General Feature Format) file for your reference genome.
    ```ftp://ftp.ensembl.org/pub/release-XX/gtf/homo_sapiens/Homo_sapiens.GRCh3X.XX.gtf.gz```

    4. Update ```REF_ANNOTATION```, and  ```FOLDER_GENOME_SEQ``` in the config file.

* Reference Proteome

The canonical repository to find proteomes is UniProt, however,  any other file will work if it is in FASTA format. You need to update the  ```UNIPROT_FASTA_LOCATION```  rnapulse/support/uniprot/{your file in fasta} , in the config file. Remember to format the file to be accessible by Blast (formatdb or makeblastdb). After, it is strongly recommended to update the files used to obtain the features. You will need all the scripts on rnapusle/support/scripts. Also you will need (Sable)[http://sable.cchmc.org/], The transmembrane coordindates, PFAM scan output, mutations, Protein-protein network, and SLIMS build for your custom proteome. Remember to update ```UNIPROT_FEATURES_DDBB, F_ELM2_LOCATION, F_SABLE_LOCATION, F_PTMS_LOCATION, F_MUTATIONS_LOCATION```  in the config file. 


## Retrain Models

Files necesary to retrain the model can be found on {path}/rnapulse/support/params/ML_Training_data.inp and  {path}/rnapulse/support/rebuild

## Experimental Features

We are working to expand the number of features used in RNApulse. However, this is still under development and are not fully support for the pipeline and they may not work as expected. Use at your own risk. 


 **Perl Requirements** 

   - Getopt::Long
   - LWP::UserAgent
   - HTTP::Request::Common qw(POST), qw(GET)
   - HTTP::Headers
   - XML::XPath
   - XML::XPath::XMLParser
   - JSON
   - IO::Socket::SSL


Anaconda package will install Perl and Perl-Cpan. Many of the required modules are not installed by default, it is highly recommended to check if the modules are installed. You can install all those modules at once. 

```bash

cpanm Getopt::Long LWP::UserAgent  HTTP::Request::Common  HTTP::Headers  XML::XPath  XML::XPath::XMLParser  JSON  IO::Socket::SSL LWP::Protocol::https

```

sudo apt-get install libexpat1-dev


or a specific module

```bash

cpan -i  JSON

```
Finally, download and add to the path the [NCBI remapping script](https://www.ncbi.nlm.nih.gov/genome/tools/remap/docs/api) and run the pipeline with --retrain parameter. 


# Contributing

Please read [CONTRIBUTING.md] for details on our code of conduct, and the process for submitting pull requests to us.

# Authors

* **Carles Corbi-Verge** 
  - *Other work* - (https://github.com/ccorbi)
  - *Other work* - (https://gitlib.com/ccorbi)

See also the list of [contributors](https://gitlib.com/ccorbi/rnapulse/contributors) who participated in this project.

# License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

# Acknowledgments

* Alexey Strokach
* Philip M. Kim


